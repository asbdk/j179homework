import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../pages/Login'
import Comment from '../pages/Comment'

Vue.use(VueRouter)

const routes = [
    {path:'/login',component:Login},
    {path:'/comment',component:Comment},
]

export default new VueRouter({
    mode:'history',
    routes
})