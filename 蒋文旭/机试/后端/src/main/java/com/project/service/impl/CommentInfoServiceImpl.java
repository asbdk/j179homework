package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.CommentInfoBean;
import com.project.dao.ICommentInfoDAO;
import com.project.service.ICommentInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
@Service
public class CommentInfoServiceImpl extends ServiceImpl<ICommentInfoDAO, CommentInfoBean> implements ICommentInfoService {

    @Resource
    private ICommentInfoDAO commentInfoDAO;

    @Override
    public Object selectAll() {
       return commentInfoDAO.selectAll();
    }

    @Override
    public void updateCommentCount(Long commentId, int count) {
        commentInfoDAO.updateCommentCount(commentId, count);
    }

    @Override
    public int checkedLike(Long userId, Long commentId) {
        int num = commentInfoDAO.checkedLike(userId, commentId);
        return num;
    }


}
