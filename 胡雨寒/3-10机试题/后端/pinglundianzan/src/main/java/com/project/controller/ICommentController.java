package com.project.controller;


import com.project.bean.CommentBean;
import com.project.service.ICommentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/comment")
public class ICommentController {
    @Resource
    private ICommentService commentService;

    @GetMapping("/select")
    public Object selectAll(){
                return commentService.selectAll();
    }
    @GetMapping("/update")
    public Object update(Long id,Long num){
        int a = (int) commentService.selectByIdNum(id, num);
        if(a ==1){
            commentService.updateDel(id, num);
        }else {
            commentService.updateAdd(id, num);
        }
        return"ok";
    }

}
