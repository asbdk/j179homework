package com.lovo.service.impl;

import com.lovo.bean.CommentInfoBean;
import com.lovo.dao.ICommentInfoDAO;
import com.lovo.service.ICommentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class CommentInfoServiceImpl extends ServiceImpl<ICommentInfoDAO, CommentInfoBean> implements ICommentInfoService {
    @Resource
    private ICommentInfoDAO dao;

    @Override
    public List<CommentInfoBean> showAll() {
        return dao.showAll();
    }

    @Override
    public void updateAdd(Long userId,Long comId) {
        if (dao.find(userId, comId) == 0){
            dao.updateAdd(comId);
        }
    }

    @Override
    public void updateCut(Long userId,Long comId) {
        if (dao.find(userId, comId) == 1){
            dao.updateCut(comId);
            dao.del(userId, comId);
        }
    }

    @Override
    public Integer find(Long userId, Long comId) {
        return dao.find(userId, comId);
    }
}
