import axios from 'axios';
import qs from 'qs';
const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
export default {
    async getSubmit (serverURL,paramObj){
    let response = await axios({
        method:"get",
        url:serverURL,
        params:paramObj,
        paramsSerializer: function(params) {
            return qs.stringify(params, {arrayFormat: 'repeat'})
        }
    })
    return response.data;
},
   async postSubmit(serverURL,paramObj)  {
    let response = await axios({
        headers,
        method:"post",
        url:serverURL,
        data:qs.stringify(paramObj, {arrayFormat: 'repeat'})
    })
    return response.data;
},
async postToken(serverURL,paramObj,token)  {
    let response = await axios({
        headers:{
            'token':token
        },
        method:"post",
        url:serverURL,
        data:qs.stringify(paramObj, {arrayFormat: 'repeat'})
    })
    return response.data;
},
async uploadSubmit (submitURL,param) {
    
            let formData = new FormData();

            for(var fieldName in param){
                formData.append(fieldName, param[fieldName]);
            }
            let config = {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }
            var response = await axios.post(submitURL, formData, config);
            return response.data;
  
        }
    }