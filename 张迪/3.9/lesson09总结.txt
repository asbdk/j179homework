1、为什么说 Mybatis 是半⾃动 ORM 映射⼯具？它与全⾃动的区别在哪⾥？
Hibernate属于全自动ORM映射工具，使用Hibernate查询关联对象或者关联集合对象时，可以根据对象关系模型直接获取，所以它是全自动的。而Mybatis在查询关联对象或关联集合对象时，需要手动编写sql来完成，所以，称之为半自动ORM映射工具。

2、简述 Mybatis 的 Xml 映射⽂件和 Mybatis 内部数据结构之间的映射关系？
MyBatis将所有的配置信息都保存在一个Configuration对象中。其中：
parameterMap会被解析为ParameterMap对象，每个子元素被解析为ParameterMapping对象
resultMap会被解析为ResultMap对象，每个子元素被解析为ResultMapping对象
增删改查标签被解析为MappedStatement对象
标签内的sql被解析为BoundSql对象

3、MyBatis中的#{}和${}的区别是什么？
${}属于静态文本替换，可以写在标签的属性值和sql内部
"#{}"是sql中的参数占位符，mybatis会把它替换成？执行sql的时候会使用PreparedStatement设置参数的方法按序给问号设置值。

4、Mybatis 动态 sql 是做什么的？都有哪些动态 sql？能简述⼀下动态 sql 的执⾏原理不？
Mybatis动态sql可以在Xml映射文件内，以标签的形式编写动态sql，执行原理是根据表达式的值 完成逻辑判断 并动态拼接sql的功能。
Mybatis提供了9种动态sql标签：trim | where | set | foreach | if | choose | when | otherwise | bind。
实际项目中，我们最常用的就是where和if标签，可以用在列表的不定条件查询的SQL中；如果有批量插入，也会使用foreach标签。trim标签也可以去除多余的and和多余的逗号。

5、Mybatis 是如何将 sql 执⾏结果封装为⽬标对象并返回的？都有哪些映射形式？
第一种是使用<resultMap>标签，逐一定义数据库列名和对象属性名之间的映射关系。
第二种是使用<resultType>标签sql列的别名功能，将列的别名书写为对象属性名。

6、Mybatis 能执⾏⼀对⼀、⼀对多的关联查询吗？都有哪些实现⽅式，以及它们之间的区别
有联合查询和嵌套查询,联合查询是几个表联合查询,只查询一次, 通过在resultMap里面配置association节点配置一对一的类就可以完成; 嵌套查询是先查一个表,根据这个表里面 的结果的外键id,去再另外一个表里面查询数据,也是通过association配置,但另外一个表的查询通过select属性配置。
有联合查询和嵌套查询,联合查询是几个表联合查询,只查询一次,通过在resultMap里面配 置collection节点配置一对多的类就可以完成; 嵌套查询是先查一个表,根据这个表里面的 结果的外键id,去再另外一个表里面查询数据,也是通过配置collection,但另外一个表的查询通过select节点配置。

7、Mybatis 是否⽀持延迟加载？如果⽀持，它的实现原理是什么？
支持，可以配置lazyLoadingEnabled=true|false来实现延迟加载。
Mybatis仅支持association和collection的延迟加载，延迟加载的原理是，当实际使用到需要延迟加载的属性时，发现为null，然后会去执行SQL把对象查询出来，然后设置属性值。

8、ConcurrentHashMap 和 Hashtable 的区别
Hashtable和ConcurrentHashMap存储的内容为键-值对（key-value），且它们都是线程安全的容器。
Hashtable所有的方法都是同步的，因此，它是线程安全的。
ConcurrentHashMap采用了更细粒度的锁来提高在并发情况下的效率。大部分操作都没有用到锁，而对应的put、remove等操作也只需要锁住当前线程，不需要锁住整个数据。采用这种设计方式以后，在大并发的情况下，同时可以有16个线程来访问数据。大大提高了并发性。

9、描述Java类的加载过程
加载、校验、准备、解析、初始化、使用、卸载

10、SQL优化手段有哪些
1、在表中建立索引，优先考虑where、group by使用到的字段（较频繁地作为查询条件且唯一性不太差），不会再where中用到的字段不建立索引，因为建立索引也需要系统的开销。
2、减少使用 * ，用列名代替
3、避免在开头使用模糊查询（%），该查询数据库引擎会放弃索引进行全表扫描。
4、避免进行NULL值判断，可以给字段添加默认值0或空字符穿
5、避免在where条件中等号的左边进行表达式或函数操作，可以将表达式或函数移到等号右边。
6、当使用where子句连接的时候，要把能过滤掉最大数量记录的条件写在最右边。
7、需要删除所有记录的时候，用truncate而不用detele
8、当where和having都用的时候，先用where，再用having；where先过滤（数据量变少），再分组，效率高。
9、避免使用in和not in，会导致全表扫描
10、如果表名或列名过长，就使用别名，因为长的表名和列名也会消耗扫描时间。