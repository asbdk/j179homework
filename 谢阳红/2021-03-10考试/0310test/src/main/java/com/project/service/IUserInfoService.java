package com.project.service;

import com.project.bean.UserInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoService extends IService<UserInfoBean> {
    UserInfoBean findByName(String userName);
}
