package com.project.service.impl;

import com.project.bean.LikedInfoBean;
import com.project.dao.ILikedInfoDAO;
import com.project.service.ICommentInfoService;
import com.project.service.ILikedInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class LikedInfoServiceImpl implements ILikedInfoService {

    @Resource
    private ILikedInfoDAO likeInfoDAO;
    @Resource
    private ICommentInfoService commentInfoService;

    @Override
    public boolean addLiked(LikedInfoBean liked) {
        if(liked == null)
            return false;
        return likeInfoDAO.addLiked(liked);
    }

    @Override
    public void updateLiked(LikedInfoBean liked) {
        likeInfoDAO.updateLiked(liked);
        if(liked.getLikeStatus().equals("1")){
            //        同时增加该评论的点赞总数
            commentInfoService.updateCommentCount(liked.getCommentInfoBean().getCommentId(), liked.getCommentInfoBean().getCommentLikeCount()+1);
        }else {
            //        同时减少该评论的点赞总数
            commentInfoService.updateCommentCount(liked.getCommentInfoBean().getCommentId(), liked.getCommentInfoBean().getCommentLikeCount()-1);

        }
    }
}
