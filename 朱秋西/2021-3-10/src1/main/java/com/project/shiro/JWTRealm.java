package com.project.shiro;


import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

public class JWTRealm extends AuthorizingRealm {
    @Resource
    private IUserInfoService userInfoService;
    @Resource
    private IFunctionInfoService functionInfoService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取当前登录的用户
        UserInfoBean userInfoBean = (UserInfoBean) principalCollection.getPrimaryPrincipal();
        //通过SimpleAuthenticationInfo做授权
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        List<RoleInfoBean> roleInfoBeans = userInfoBean.getRoleInfoBeans();
        List<FunctionInfoBean> functionInfoBeans = functionInfoService.selectByRoleId(roleInfoBeans);
        for (RoleInfoBean role : roleInfoBeans) {
            //添加角色
            simpleAuthorizationInfo.addRole(role.getRoleName());
        }
        for (FunctionInfoBean function : functionInfoBeans) {
            //添加权限
            simpleAuthorizationInfo.addStringPermission(function.getFuncName());
        }
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String token = (String) authenticationToken.getPrincipal();
        if(token == null){
            return new SimpleAuthenticationInfo(null,"",getName());
        }
        UserInfoBean userInfoBean = null;
        try {
            userInfoBean = WebUtil.verifyToken(token,userInfoService);
        } catch (GlobalHandleException e) {}
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(userInfoBean,token,getName());
        return simpleAuthenticationInfo;
    }
}
