import ajax  from "../service/ajax.js"

export default {
    namespaced:true,
    state:{
        commenrList:[]
    },
    mutations:{
        setCommenrList(state,listObj){
            state.commenrList = listObj;
        }
    },
    actions:{
        async getComList(context){
            var info = await ajax.getSubmit("/project/comments");
            if(info.status == 1){
                context.commit("setCommenrList",info.data);
                
            }
        }
    }
}