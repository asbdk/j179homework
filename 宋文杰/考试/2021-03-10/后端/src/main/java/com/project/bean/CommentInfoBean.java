package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("comment_info")
public class CommentInfoBean {

    private static final long serialVersionUID = 1L;
    @TableId(value = "comt_id",type = IdType.AUTO)
    private Long commentId;
    @TableField("comt_info")
    private String info;
    @TableField("comt_like_num")
    private int likeNum;

}
