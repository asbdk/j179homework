package com.lovo.shiro;

import com.lovo.bean.UsersBean;
import com.lovo.result.GlobalHandleException;
import com.lovo.service.IUsersService;
import com.lovo.util.WebUtil;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.List;

public class JWTRealm extends AuthorizingRealm {
    @Resource
    private IUsersService userInfoService;
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
//        //获取当前登录的用户
//        UsersBean userInfoBean = (UsersBean) principalCollection.getPrimaryPrincipal();
//        //通过SimpleAuthenticationInfo做授权
//        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String token = (String) authenticationToken.getPrincipal();
        if (token == null) {
            return new SimpleAuthenticationInfo(null, "", getName());
        }
        UsersBean userInfoBean = null;
        try {
            userInfoBean = WebUtil.verifyToken(token, userInfoService);
        } catch (Exception e) {
        }
            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(userInfoBean, token, getName());
            return simpleAuthenticationInfo;

    }
}
