import axios from 'axios'
export const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}

export const showAllCommoment = async (userId) => {
    let response = await axios({
        headers,
        method:"get",
        params:{
            userId
        },
        url:"/api/commoments"
    })
    return response.data;
}
export const doLike = async (commomentId,userId,token) => {
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/commoments/doLike",
        params:{
            commomentId,userId
        }
    })
    return response.data;
}
export const cancelLike = async (commomentId,userId,token) => {
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/commoments/cancelLike",
        params:{
            commomentId,userId
        }
    })
    return response.data;
}