package com.lovo.service.impl;

import com.lovo.bean.DiscussBean;
import com.lovo.dao.IDiscussDAO;
import com.lovo.service.IDiscussService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class DiscussServiceImpl extends ServiceImpl<IDiscussDAO, DiscussBean> implements IDiscussService {
    @Resource
    private IDiscussDAO discussDAO;


    @Override
    public void deleteFavor(Integer userId, Integer discussId) {
        discussDAO.deleteFavor(userId, discussId);
    }

    @Override
    public void addFavor(Integer userId, Integer discussId) {
        discussDAO.addFavor(userId, discussId);
    }

    @Override
    public List<DiscussBean> findDiscuss() {
        return discussDAO.findDiscuss();
    }
}
