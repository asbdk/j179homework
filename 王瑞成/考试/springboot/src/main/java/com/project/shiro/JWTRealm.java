package com.project.shiro;

import com.project.bean.AdminInfoBean;
import com.project.result.GlobalHandleException;
import com.project.service.IAdminInfoService;
import com.project.util.WebUtil;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;

public class JWTRealm extends AuthorizingRealm {
    @Resource
    private IAdminInfoService adminInfoService;

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
//        //获取当前登录的用户
//        AdminInfoBean userInfoBean = (AdminInfoBean) principalCollection.getPrimaryPrincipal();
//        //通过SimpleAuthenticationInfo做授权
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
//        List<RoleInfoBean> roleInfoBeans = userInfoBean.getRoleInfoBeans();
//        List<FunctionInfoBean> functionInfoBeans = functionInfoService.selectByRoleId(roleInfoBeans);
//        for (RoleInfoBean role : roleInfoBeans) {
//            //添加角色
//            simpleAuthorizationInfo.addRole(role.getRoleName());
//        }
//        for (FunctionInfoBean function : functionInfoBeans) {
//            //添加权限
//            simpleAuthorizationInfo.addStringPermission(function.getFuncName());
//        }
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String token = (String) authenticationToken.getPrincipal();
        if(token == null){
            return new SimpleAuthenticationInfo(null,"",getName());
        }
        AdminInfoBean adminInfoBean = null;
        try {
            adminInfoBean = WebUtil.verifyToken(token,adminInfoService);
        } catch (GlobalHandleException e) {}
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(adminInfoBean,token,getName());
        return simpleAuthenticationInfo;
    }
}
