package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.UserBean;
import com.lovo.dao.IUserDao;
import com.lovo.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl extends ServiceImpl<IUserDao, UserBean> implements IUserService {
    @Resource
    private IUserDao userDao;

    @Override
    public UserBean selectByUserName(String userName) {
        return userDao.selectByUserName(userName);
    }
}
