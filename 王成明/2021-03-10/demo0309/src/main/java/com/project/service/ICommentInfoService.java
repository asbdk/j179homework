package com.project.service;

import com.project.bean.CommentInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Wangcm
 * @since 2021-03-10
 */
public interface ICommentInfoService extends IService<CommentInfoBean> {
    /**
     * 查询所有评论信息
     * @return
     */
    List<CommentInfoBean> findAll();
    /**
     * 根据评论id统计点赞次数
     * @param comId
     * @return
     */
    Integer countLikes(Integer comId);



}
