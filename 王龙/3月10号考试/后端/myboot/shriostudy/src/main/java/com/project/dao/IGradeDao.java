package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.GradeBean;
import com.project.bean.RoleBean;

import java.util.List;

public interface IGradeDao extends BaseMapper<GradeBean> {
    List<GradeBean> findByRoleList(List<RoleBean> roleBeanList);
}
