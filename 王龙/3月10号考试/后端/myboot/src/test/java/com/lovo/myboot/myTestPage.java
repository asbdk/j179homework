package com.lovo.myboot;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lovo.bean.StudentBean;
import com.lovo.service.IStudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class myTestPage {
    @Resource
    private IStudentService service;
    @Test
    public void t(){
        QueryWrapper<StudentBean> queryWrapper = new QueryWrapper();
        queryWrapper.ge("stu_sex","男");
        Page<StudentBean> page = new Page<>(1,2);
        IPage<StudentBean> iPage = service.getBaseMapper().selectPage(page,queryWrapper);
        System.out.println("总页数:"+iPage.getPages());
        System.out.println("总记录数:"+iPage.getTotal());

//        List<StudentBean> list = iPage.getRecords();
//        for(StudentBean stu:list){
//            System.out.println(stu);
//        }

    }
}
