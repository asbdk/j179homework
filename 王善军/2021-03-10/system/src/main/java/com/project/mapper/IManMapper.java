package com.project.mapper;

import com.github.pagehelper.PageInfo;
import com.project.bean.ManBean;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IManMapper {
    public void add(ManBean man);
    public List<ManBean> findAll();
}
