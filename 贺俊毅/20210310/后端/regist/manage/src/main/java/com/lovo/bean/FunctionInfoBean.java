package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author bdk
 * @since 2021-03-05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("function_info")
public class FunctionInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "func_id", type = IdType.AUTO)
    private Long funcId;
    @TableField("func_name")
    private String funcName;

    @TableField("func_path")
    private String funcPath;

    @TableField("func_method")
    private String funcMethod;


}
