package com.lovo.dao;

import com.lovo.bean.SpeakBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ISpeakDAO extends BaseMapper<SpeakBean> {
    public List<SpeakBean> findAll(@Param("user") Long user);

}
