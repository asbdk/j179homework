package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.CommentBean;
import com.project.bean.UserBean;
import com.project.dao.ICommentDao;
import com.project.service.ICommentService;
import com.project.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class CommentServiceImpl  extends ServiceImpl<ICommentDao, CommentBean> implements ICommentService {
    @Resource
    public ICommentDao commentDao;
    @Override
    public void update(Long commentid, int shu) {
        commentDao.update(commentid,shu);
    }

    @Override
    public void addUserComment(int userId, int commentId) {
        commentDao.addUserComment(userId, commentId);
    }

    @Override
    public void delUserComment(int userId, int commentId) {
        commentDao.delUserComment(userId, commentId);
    }
}
