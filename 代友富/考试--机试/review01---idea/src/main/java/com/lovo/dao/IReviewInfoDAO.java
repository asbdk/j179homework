package com.lovo.dao;

import com.lovo.bean.ReviewInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IReviewInfoDAO extends BaseMapper<ReviewInfoBean> {
    List selectAllReview();
    void del(@Param("userId") Integer userId, @Param("revId") Integer revId);
    void add(@Param("userId") Integer userId, @Param("revId") Integer revId);
    Integer findByRevId(@Param("userId") Integer userId, @Param("revId") Integer revId);
}
