package com.lovo.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lovo.bean.UserInfoBean;
import com.lovo.dao.IArticleDAO;
import com.lovo.dao.IUserInfoDAO;
import com.lovo.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.util.RedisUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<IUserInfoDAO, UserInfoBean> implements IUserInfoService {
    @Resource
    private IUserInfoDAO userInfoDAO;

    @Override
    public UserInfoBean findByUidAndAid(Integer userId,Integer articleId) {
        return userInfoDAO.findByUidAndAid(userId,articleId);
    }

    @Override
    public UserInfoBean selectByUserName(String username) {
        return userInfoDAO.selectByUserName(username);
    }

    @Override
    public void deleteGreat(Integer userId, Integer articleId) {
        userInfoDAO.deleteGreat(userId, articleId);
    }

    @Override
    public void addGreat(Integer userId, Integer articleId) {
        userInfoDAO.addGreat(userId, articleId);
    }
}
