package com.project.controller;

import com.project.bean.CommentBean;
import com.project.bean.UserBean;
import com.project.service.ICommentService;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("comment")
public class ICommentController {
    @Resource
    private ICommentService service;
    @RequestMapping("add")
    public String add(@Param("user")Long userId, @Param("info")String info){
        service.add(userId,info);
        return "ok";
    }

}
