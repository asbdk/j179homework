package com.project.controller;

import com.project.bean.StudentBean;
import com.project.service.IStudentService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class IndexController {
    @Resource
    private IStudentService service;

    @GetMapping("/index")
    public String get(Model model){
        model.addAttribute("name","张三");
        List<StudentBean> list = service.findAll();
        model.addAttribute("students", list);
        return "index.html";
    }
}
