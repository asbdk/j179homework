import Vue from 'vue'
import VueRouter from 'vue-router'
import login from "../user/login.vue"
import show from "../user/show.vue"
import index from "../user/index.vue"
import check from "../user/check.vue"


Vue.use(VueRouter)

const routes = [    
    {path:'/login',component:login},
    {path:'/index',component:index,
        children:[
            {path:'/index/show',component:show}
        ]
    },   
    {path:"/check",component:check}
] 

export default new VueRouter({
    mode:'history',
    routes
})