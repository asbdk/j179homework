package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("user_comments")
public class UserCommentBean {
    private static final long serialVersionUID = 1L;

    @TableId(value = "user_comments_id", type = IdType.AUTO)
    private Long userCommentsid;

    private Long userId;

    private Long commentsId;
}
