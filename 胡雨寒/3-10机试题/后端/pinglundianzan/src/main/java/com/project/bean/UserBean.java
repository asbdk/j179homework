package com.project.bean;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user_user_info")
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class UserBean {
    private static final long serialVersionUID =1L;
    @TableId(value = "uu_id",type = IdType.NONE)
    private Long id;
    @TableField(value = "uu_name")
    private String name;
    @TableField(value = "uu_pwd")
    private String pwd;
}
