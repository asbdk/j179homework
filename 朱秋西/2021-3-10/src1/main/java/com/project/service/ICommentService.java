package com.project.service;

import com.project.bean.UserBean;

import java.util.List;

public interface ICommentService {
    void update(Long commentid, int shu);
    void addUserComment(int userId, int commentId);
    void delUserComment(int userId, int commentId);
}
