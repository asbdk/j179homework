package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * 评论实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("user_comment")
public class CommentBean {

    private static final long serialVersionUID = 1L;
    @TableId(value = "comment_id", type = IdType.AUTO)
    private Long commentId;

    @TableField("comment_info")
    private String info;

//    @TableField("comment_praise")
//    private String praiseNum;

    @TableField("fk_userid")
    private  Long UserId;
    @Override
    public String toString() {
        return "CommentBean{" +
                "funcId=" + commentId +
                ", info='" + info + '\'' +
//                ", praiseNum='" + praiseNum+
                '}';
    }
}