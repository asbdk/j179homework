import Vue from 'vue'
import App from './App.vue'

import ElementUI from 'element-ui';//导入elementui组件
import 'element-ui/lib/theme-chalk/index.css';
import router from './router/index.js'
import ajax from './service/ajax.js'//导入ajax组件
import stroe from "./store/index.js"//导入store组件
import index from "./pages/index.vue"
import VueCookies from 'vue-cookies'

Vue.use(VueCookies)//在vue中使用cookie
Vue.use(ElementUI);//注册elementui组件
Vue.config.productionTip = false
Vue.prototype.ajax=ajax//注册ajax
new Vue({
  index:index,
  store:stroe,//注册store，注意store不能写错了
  router,//注册路由组件，添加的映射
  render: h => h(App),
}).$mount('#app')
