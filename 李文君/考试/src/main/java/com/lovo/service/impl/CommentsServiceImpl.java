package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.CommentsBean;
import com.lovo.bean.UserBean;
import com.lovo.dao.ICommentsDAO;
import com.lovo.dao.IUserDAO;
import com.lovo.service.ICommentsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class CommentsServiceImpl extends ServiceImpl<IUserDAO, UserBean> implements ICommentsService {

    @Resource
    private ICommentsDAO dao;




    @Override
    public List<CommentsBean> findAll() {
        return dao.findAll();
    }

}
