package com.lovo.service;

import com.lovo.bean.CommentInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentInfoService extends IService<CommentInfoBean> {
    List<CommentInfoBean> showAll();
    void updateAdd(Long userId,Long comId);
    void updateCut(Long userId,Long comId);
    Integer find(Long userId,Long comId);



}
