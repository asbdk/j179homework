package com.lax.controller;


import com.lax.result.ResponseResult;
import com.lax.service.ICommentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/comments")
public class CommentController {

    @Resource
    private ICommentService service;


    @GetMapping("/findAll")
    @ResponseResult
    public Object selectAll(){
        return  service.findAll();
    }

    @RequestMapping("/updateInfo")
    public Object update(int idAdmin,int idComment){
        service.updateZan(idAdmin, idComment);
        return "ok";
    }

    @RequestMapping("/check")
    public Boolean checkStatus(int idAdmin,int idComment){
        return service.findById(idAdmin, idComment);
    }


}

