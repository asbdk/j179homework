package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.CommendBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ICommendDAO extends BaseMapper<CommendBean> {
    /**
     * 查询所有
     * @return
     */
    List<CommendBean> findByPage();
    /**
     * 判断用户是否点赞
     * @param userId
     * @param commendId
     * @return
     */
    Long checkIsZan(@Param("userId") Long userId,@Param("commendId")  Long commendId);
}
