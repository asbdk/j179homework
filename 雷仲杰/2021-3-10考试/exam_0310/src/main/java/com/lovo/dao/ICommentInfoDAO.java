package com.lovo.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.lovo.bean.CommentInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentInfoDAO extends BaseMapper<CommentInfoBean> {
    List<CommentInfoBean> showAll();
    void updateAdd(Long comId);
    void updateCut(Long comId);
    Integer find(@Param("userId") Long userId,@Param("comId") Long comId);
    void del(@Param("userId") Long userId,@Param("comId") Long comId);

}
