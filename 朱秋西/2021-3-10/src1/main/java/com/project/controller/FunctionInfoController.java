package com.project.controller;


import com.lovo.bean.FunctionInfoBean;
import com.lovo.result.GlobalHandleException;
import com.lovo.result.ResponseResult;
import com.lovo.result.Result;
import com.lovo.result.ResultCode;
import com.lovo.service.IFunctionInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-05
 */
@RestController
@RequestMapping("/functions")
@ResponseResult
public class FunctionInfoController {
    @Resource
    private IFunctionInfoService functionInfoService;
    @PostMapping
    public Object add(@RequestBody FunctionInfoBean functionInfoBean) throws GlobalHandleException {
        try{
            functionInfoService.add(functionInfoBean);
        }catch (Exception e){
            throw new GlobalHandleException(ResultCode.SERVER_UNKNOW_ERROR);
        }

        return null;
    }
    @DeleteMapping("/{id}")
    public Object add(@PathVariable("id") Long id) throws GlobalHandleException {
        try {
            functionInfoService.delete(id);
        } catch (Exception e) {
            throw new GlobalHandleException(ResultCode.SERVER_UNKNOW_ERROR);
        }
        return null;
    }

}

