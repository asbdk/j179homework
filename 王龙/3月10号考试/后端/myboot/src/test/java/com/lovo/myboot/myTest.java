package com.lovo.myboot;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lovo.bean.StudentBean;
import com.lovo.service.IStudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class myTest {
    @Resource
    private IStudentService iStudentService;
    @Test
    public void add(){
//        System.out.println(iStudentService.selectAll("张三"));
//        StudentBean studentBean = new StudentBean(null,18,"周杰伦","男");
//        iStudentService.save(studentBean);
//        StudentBean studentBean02 = new StudentBean(10,65,"刘德华","女");
//        iStudentService.updateById(studentBean02);
//        iStudentService.removeById(7);
        QueryWrapper queryWrapper = new QueryWrapper<StudentBean>();
        queryWrapper.eq("stu_name","张三");
        System.out.println(iStudentService.list(queryWrapper));

    }
}
