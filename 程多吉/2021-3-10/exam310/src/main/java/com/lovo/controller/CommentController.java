package com.lovo.controller;

import com.lovo.bean.CommentBean;
import com.lovo.service.ICommentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("comment")
public class CommentController {

    @Resource
    private ICommentService service;

    @GetMapping
    @ResponseBody
    public Object findByOrder(){
        return service.findAllByOrder();
    }

    @RequestMapping("check")
    @ResponseBody
    public boolean findByUserId(Integer userId, Integer commentId){
        List<CommentBean> list = service.checkCommentById(userId);

        for(CommentBean bean : list){
            if(bean.getId() == commentId){
                return true;
            }
        }

        return false;
    }

}
