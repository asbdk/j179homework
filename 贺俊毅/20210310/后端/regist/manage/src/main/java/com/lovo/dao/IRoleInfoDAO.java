package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.RoleInfoBean;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-05
 */
public interface IRoleInfoDAO extends BaseMapper<RoleInfoBean> {

}
