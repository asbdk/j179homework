package com.project.service;

import com.project.bean.CommentInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentInfoService extends IService<CommentInfoBean> {
    List<CommentInfoBean> findAll();
    void del(@Param("adminId") Integer adminId, @Param("commentId") Integer commentId);
    void add(@Param("adminId") Integer adminId, @Param("commentId") Integer commentId);
    Integer findByCommentId(@Param("adminId") Integer adminId, @Param("commentId") Integer commentId);

}
