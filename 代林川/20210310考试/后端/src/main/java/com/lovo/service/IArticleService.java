package com.lovo.service;

import com.lovo.bean.ArticleBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IArticleService extends IService<ArticleBean> {
    void updateNum(ArticleBean articleBean);
    Object selectAll(Integer page, Integer size);
}
