package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("comment_s")
public class CommentBean implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @TableId(value = "comment_id", type = IdType.NONE)
    private Long commentid;
    @TableField("namec")
    private String namec;
    @TableField("comment")
    private String comment;
    @TableField("shu")
    private int shu;

}
