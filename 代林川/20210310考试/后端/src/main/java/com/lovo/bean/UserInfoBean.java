package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author johndai
 * @since 2021-03-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("user_info")
public class UserInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户Id
     */
    @TableId(value = "user_id", type = IdType.NONE)
    private Long userId;

    /**
     * 用户名
     */
    @TableField("user_code")
    private String userCode;

    /**
     * 用户密码
     */
    @TableField("user_pass")
    private String userPass;

    /**
     * 真实姓名
     */
    @TableField("user_realName")
    private String userRealName;

    private List<ArticleBean> articleBeans;


}
