package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.CommentInfoBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ICommentInfoDao extends BaseMapper<CommentInfoBean> {

    void addUC(@Param("userId") int userId, @Param("commentId") int commentId);

    void delUC(@Param("commentId") int commentId, @Param("userId") int userId);

}
