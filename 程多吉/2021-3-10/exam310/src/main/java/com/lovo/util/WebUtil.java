package com.lovo.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lovo.bean.UserBean;
import com.lovo.result.GlobalHandleException;
import com.lovo.result.Result;
import com.lovo.service.IUserService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

import javax.servlet.ServletResponse;
import java.io.IOException;

public class WebUtil {
    public static void writeValueAsString(ServletResponse response, Result result) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().print(mapper.writeValueAsString(result));
    }



}
