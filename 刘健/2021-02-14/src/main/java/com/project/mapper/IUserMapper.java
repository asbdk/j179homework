package com.project.mapper;

import com.project.bean.UserBean;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserMapper {
    /**
     * 用户登录方法
     * @param name 用户id
     * @param pwd 用户密码
     * @return 返回登录用户对象
     */
    @Select("SELECT * FROM t_user WHERE p_name=#{name} AND p_pwd = #{pwd}")
    @ResultMap("userMap")
    public UserBean login(@Param("name")String name,@Param("pwd")String pwd);

    /**
     * 注册添加用户
     * @param user 用户对象
     */
    @Insert("INSERT INTO t_user(p_name,p_pwd)VALUES(#{name},#{pwd})")
    public void add(UserBean user);
}
