package com.project.service.impl;


import com.project.bean.StudentBean;
import com.project.dao.IStudentDAO;
import com.project.service.IStudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class StudentServiceImpl implements IStudentService {
    @Resource
    private IStudentDAO studentDAO;


    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void add(StudentBean studentBean) {
        studentDAO.insert(studentBean);
    }

    @Override
    public StudentBean findById(int id) {
        return studentDAO.findById(id);
    }

    @Override
    public void del(int id) {
        studentDAO.del(id);
    }

    @Override
    public void update(int id, String age) {
        studentDAO.update(id, age);
    }

    @Override
    public List<StudentBean> findAll() {
        return studentDAO.findAll();
    }

}
