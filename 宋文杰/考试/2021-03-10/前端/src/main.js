import Vue from 'vue'
import App from './App.vue'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import router from './router/router.js'
import store from './store/store.js'
import ajax from './service/ajax.js'

Vue.use(ElementUI);
Vue.prototype.ajax=ajax;

Vue.config.productionTip = false;

new Vue({
  router,
  store, 
  ajax,
  render: h => h(App),
}).$mount('#app')
