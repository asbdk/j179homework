package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.RoleInfoBean;
import com.lovo.dao.IRoleInfoDAO;
import com.lovo.service.IRoleInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-05
 */
@Service
public class RoleInfoServiceImpl extends ServiceImpl<IRoleInfoDAO, RoleInfoBean> implements IRoleInfoService {

}
