package com.project.controller;

import com.github.pagehelper.PageInfo;
import com.project.bean.DoTeyBean;
import com.project.service.DoTeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("doTey")
public class IDoTeyController {
    @Autowired
    private DoTeyService service;

    @RequestMapping("addDoTey")
    public String addDoTey(int userId, DoTeyBean doTey) {
        service.addDoTey(userId,doTey);
        return "OK";

    }

    @RequestMapping("update")
    public String update(int id,String info) {
        service.update(id,info);
        return "ok";
    }

    @RequestMapping("findByItem")
    public PageInfo<DoTeyBean> findByItem(int pageNO,int userId, String type, String doTeyName, String rank) {
        return service.findByItem(pageNO,userId,type,doTeyName,rank);
    }

    @RequestMapping("del")
    public String del(int doTeyId) {
        service.del(doTeyId);
        return "ok";
    }
}
