1、SpringMVC 工作原理
客户端请求提交到DispatcherServlet
由DispatcherServlet控制器查询一个或多个HandlerMapping，找到处理请求的Controller
DispatcherServlet将请求提交到Controller
Controller调用业务逻辑处理后，返回ModelAndView
DispatcherServlet查询一个或多个ViewResoler视图解析器，找到ModelAndView指定的视图,视图负责将结果显示到客户端

2、SpringMVC 的控制器是不是单例模式,如果是,有什么问题,怎么解决
是单列模式，线程不够安全，不能在控制器里写成员变量

3、什么是 CSRF 攻击
指跨站点请求伪造：
攻击者在用户已经登录目标网站之后，诱使用户访问一个攻击页面，利用目标网站对用户的信任，
以用户身份在攻击页面对目标网站发起伪造用户操作的请求，达到攻击目的

4、什么是 WebSockets
WebSockets是一种通过TCP工作的有状态通信协议。
WebSocket 是双向的消息发送
客户端和服务器通信相互独立

5、怎么样在方法里面得到 Request,或者 Session
ActionContext得到：ActionContext context = ActionContext.getContext();通过实现 RequestAware, SessionAware,ApplicationAware等接口来得到
经过ServletActionContext：HttpServletRequest request = ServletActionContext.getRequest();HttpSession session = request.getSession()

6、SpringMVC 怎么样设定重定向和转发的
重定向：是服务器告诉了客户端要转向哪个地址，客户端再自己去请求转向的地址
转发：是在服务器内部控制权的转移，是由服务器区请求，因此客户端浏览器的地址不会显示出转向的地址。 	

7、SpringMvc 用什么对象从后台向前台传递数据的
使用Model对象：第一步：使用model对象往前台传递数据，第二步：在jsp中接收从后台传递过来的参数
使用HttpServletRequest对象：第一步：使用HttpServletRequest对象往前台传递数据，第二步：在jsp中接收从后台传递过来的参数
使用Map对象

8、Spring Data JPA中一对一唯一外键关系和一对多双向关系在配置上有什么区别？
一对一中：主外键的配置：@OneToOne(cascade = CascadeType.ALL)
 		        @JoinColumn(name = "detail_id")
	 外键的配置：   @OneToOne(mappedBy = "detail")
一对多中：主外键配置：    @ManyToOne()
    	    	        @JoinColumn(name = "class_id")
	外键的配置：@JoinColumn(name = "class_id", referencedColumnName = "class_id")

9、请解释级联和关系维护这二者的特点
级联：在双向多对一中，一方设置级联，如cascade = CascadeType.ALL，当主外键放的值发生改变时，外键表的值也会跟着改变。如果某一方没有设置级联，那么它在操作时不会影响另一方
关系维护：在双向多对一中，设置多的一方为关系维护方，该方做增删改时，会去维护另一方，并且，有级联的情况下操作关系维护方才能成功

10、级联操作有哪些，分别解释下各自的特点
包括：
CascadeType.PERSIST   	只有A类新增时，会级联B对象新增。
CascadeType.MERGE    	指A类新增或者变化，会级联B对象
CascadeType.REMOVE 	删除当前实体时，与它有映射关系的实体也会跟着被删除。
CascadeType.ALL  		包含所有
CascadeType.REFRESH   	级联刷新操作


