1、SpringMVC 工作原理
1、当客户端请求服务器，服务器使用前端控制器DispatcherServlet接收请求
2、DispatcherServlet借助HandlerMapping，根据请求的URL路径，定位到具体的Controller，和应用控制器的具体方法。并将封装好的数据的实体对象传入应用控制器方法
3、由应用控制器方法，完成业务组件的业务方法调用，然后根据业务方法处理的结果返回需要转发的页面路径。DispatcherServlet根据路径完成页面转发

2、SpringMVC 的控制器是不是单例模式,如果是,有什么问题,怎么解决
SpringMvc的控制器是单例模式，所以在多线程访问的时候有线程安全问题，不要用同步，会影响性能的，解方案是在控制器里面不能写字段。

3、什么是 CSRF 攻击
CSRF（Cross-site request forgery），中文名称：跨站请求伪造，也被称为：one click attack/session riding，缩写为：CSRF/XSRF。

4、什么是 WebSockets
WebSocket是一种在单个TCP连接上进行全双工通信的协议。WebSocket通信协议于2011年被IETF定为标准RFC 6455，并由RFC7936补充规范。WebSocket API也被W3C定为标准。
WebSocket使得客户端和服务器之间的数据交换变得更加简单，允许服务端主动向客户端推送数据。在WebSocket API中，浏览器和服务器只需要完成一次握手，两者之间就直接可以创建持久性的连接，并进行双向数据传输。

5、怎么样在方法里面得到 Request,或者 Session
直接在方法的形参中声明request,SpringMvc就自动把request对象传入。

6、SpringMVC 怎么样设定重定向和转发的
一般情况下,控制器方法返回字符串类型的值会被当成逻辑视图名处理。如果返回的字符串中带 forward: 或 redirect: 前缀时,SpringMVC 会对他们进行特殊处理:将 forward: 和redirect: 当成指示符,其后的字符串作为 URL 来处理。
（1）转发：在返回值前面加"forward:"，譬如"forward:user.do?name=method4"
（2）重定向：在返回值前面加"redirect:"，譬如"redirect:http://www.baidu.com"

7、SpringMvc 用什么对象从后台向前台传递数据的
通过ModelMap对象,可以在这个对象里面调用put方法,把对象加到里面,前台就可以通过el表达式拿到。

8、Spring Data JPA中一对一唯一外键关系和一对多双向关系在配置上有什么区别？
一对一唯一外键，拥有外键的为主控方
@OneToOne(mappedBy = "detailBean")
private StudentBean studentBean;
@JoinColumn中的name是指向外键字段
一对多（双向）：多方为主控方
@ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE},fetch = FetchType.LAZY)
@JoinColumn(name="s_c_id")
private ClassesBean classesBean;
@OneToMany(mappedBy = "classesBean")
private List<StudentBean> studentBeans = new ArrayList<StudentBean>();

9、请解释级联和关系维护这二者的特点
级联：在操作设置级联的那一方时会影响另一方，比如在保存时，当然前提是要对对方属性有设置，不然就是空，那么对方也会插入数据，但对方插入数据时，有没有本方的属性就要看本方是不是关系维护方了。如果某一方没有设置级联，那么它在操作时不会影响另一方。
关系维护：关系维护方是指在该方做增删改时，会去维护另一方

10、级联操作有哪些，分别解释下各自的特点
① CascadeType.ALL：增删改所有级联
② CascadeType.PERSIST：级联增加操作
③ CascadeType.MERGE：级联更新操作
④ CascadeType.REMOVE：级联删除操作
⑤CascadeType.REFRESH：级联刷新操作
⑥CascadeType.DETACH：级联脱管/游离操作
