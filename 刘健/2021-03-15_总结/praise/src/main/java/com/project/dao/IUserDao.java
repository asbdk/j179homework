package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.UserBean;
import org.apache.ibatis.annotations.Param;

public interface IUserDao extends BaseMapper<UserBean> {
    UserBean login(@Param("userName") String userName,@Param("userPwd") String userPwd);




}
