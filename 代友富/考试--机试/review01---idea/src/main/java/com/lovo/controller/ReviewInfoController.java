package com.lovo.controller;


import com.lovo.bean.ReviewInfoBean;
import com.lovo.result.ResponseResult;
import com.lovo.service.IReviewInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@ResponseResult
@RequestMapping("/reviews")
@Api("评论点赞")
public class ReviewInfoController {
    @Resource
    private IReviewInfoService reviewInfoService;

    @ApiOperation("点赞/取消点赞")
    @PostMapping("/updateNum")
    public Object updateNum(Integer userId, Integer revId){
        Integer flag = reviewInfoService.findByRevId(userId,revId);
        if(flag == 1){
            reviewInfoService.del(userId,revId);
        }else {
            reviewInfoService.add(userId,revId);
        }
        return "ok";
    }

    @ApiOperation("点赞/取消点赞")
    @GetMapping("/look")
    public Object look(Integer userId, Integer revId){
        Integer flag = reviewInfoService.findByRevId(userId,revId);
        return flag;
    }

    @ApiOperation("查看评论")
    @GetMapping("/selectAllReview")
    public Object selectAllReview(){
        return reviewInfoService.selectAllReview();
    }

}

