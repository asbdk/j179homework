package com.project.controller;


import com.project.result.ResponseResult;
import com.project.service.ICommentInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Wangcm
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/comments")
public class CommentInfoController {

    @Resource
    private ICommentInfoService commentInfoService;


    @ResponseResult
    @GetMapping
    public Object findAll(){
        return commentInfoService.findAll();
    }

    @ResponseResult
    @GetMapping("/countLikes")
    public Object countLikes(int comId){
        return commentInfoService.countLikes(comId);
    }

}

