package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.DianzanBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IDianzanDao extends BaseMapper<DianzanBean> {
    List selectAll();
    void updateScore(@Param("id") int id, @Param("num") int num);
    void addUserDian(@Param("userId") int userId, @Param("dianzanId") int dianzanId);
    void delUserDian(@Param("userId") int userId, @Param("dianzanId") int dianzanId);
}
