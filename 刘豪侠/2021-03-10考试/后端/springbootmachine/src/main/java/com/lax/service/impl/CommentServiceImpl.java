package com.lax.service.impl;

import com.lax.bean.CommentBean;
import com.lax.dao.ICommentDAO;
import com.lax.service.ICommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class CommentServiceImpl extends ServiceImpl<ICommentDAO, CommentBean> implements ICommentService {

    @Override
    public List<CommentBean> findAll() {
        return this.baseMapper.findAll();
    }

    @Override
    public void updateZan(int idAdmin ,int idComment) {
       Integer info =  this.baseMapper.findById(idAdmin, idComment);
       if(info >= 1){
           this.baseMapper.delAdminCommentInfo(idAdmin, idComment);
       } else {
           this.baseMapper.addAdminCommentInfo(idAdmin, idComment);
       }
    }

    @Override
    public boolean findById(int idAdmin, int idComment) {
        Integer info =  this.baseMapper.findById(idAdmin, idComment);
        if(info >= 1){
            return true;
        }
        return false;
    }
}
