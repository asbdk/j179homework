package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("comment_info")
@AllArgsConstructor
@NoArgsConstructor
public class CommentInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "com_id", type = IdType.AUTO)
    private Long comId;
    @TableField("com_info")
    private String comInfo;
    @TableField("com_count")
    private Long comCount;




}
