package com.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.CommentInfoBean;

public interface ICommentInfoService extends IService<CommentInfoBean> {

    Object findAll(Integer pageNO, Integer size);

    void addUC(int userId, int commentId);

    void delUC(int commentId, int userId);

    void updateAddLikeNum(int commentId);

    void updateReduceLikeNum(int commentId);

}
