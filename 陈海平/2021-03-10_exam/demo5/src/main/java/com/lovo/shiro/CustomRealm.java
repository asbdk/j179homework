package com.lovo.shiro;
import com.lovo.bean.UsersBean;
import com.lovo.service.IUsersService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.List;

public class CustomRealm extends AuthorizingRealm {
    @Resource
    private IUsersService userInfoService;
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取当前登录的用户
//        UsersBean userInfoBean = (UsersBean) principalCollection.getPrimaryPrincipal();
//
//        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();


        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 身份认证的操作
        //1.获取用户输入的账号
        String username = (String) authenticationToken.getPrincipal();
        String userpwd = (String) authenticationToken.getCredentials();
        //2.通过username从数据库中查找到user实体
        UsersBean userInfoBean = userInfoService.selectByUserName(username);
        if (userInfoBean == null) {
            return null;

        }
        //3.通过SimpleAuthenticationInfo做身份处理
        SimpleAuthenticationInfo simpleAuthenticationInfo =
                new SimpleAuthenticationInfo(userInfoBean, userInfoBean.getUserPwd(), getName());
        //4.返回身份处理对象
        return simpleAuthenticationInfo;

    }
}
