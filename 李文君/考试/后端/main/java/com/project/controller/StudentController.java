package com.project.controller;

import com.project.bean.StudentBean;
import com.project.result.ResponseResult;
import com.project.service.IStudentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author 一期一会
 */
@RestController
@RequestMapping("/student")
public class StudentController {
    @Resource
    private IStudentService service;

    @PostMapping
    @ResponseResult
    public Object add(@RequestBody @Valid StudentBean studentBean){
        service.add(studentBean);
        return "ok";
    }

    @GetMapping("/{id}")
    @ResponseResult
    public Object findById(@PathVariable("id") int id){
        return service.findById(id);
    }

    @DeleteMapping("/{id}")
    public String del(@PathVariable("id") int id){
        service.del(id);
        return "delok";
    }

    @PutMapping("/{id}")
    public String update(@PathVariable("id") int id,String age){
        service.update(id, age);
        return "updateok";
    }

    @GetMapping
    @ResponseResult
    public Object showAll(){
        return service.findAll();
    }
}
