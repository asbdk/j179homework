import axios from 'axios'
import qs from 'qs'
export const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}

export const login = async (userName,userPass) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/api/users/login",
        data:qs.stringify({
            userName,userPass
        })
    })
    return response.data;
}
export const showAll = async () => {
  
    let response = await axios({
        method:"get",
        url:"/api/comments"
    })
    return response.data;
}
export const getUser = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/users/token"
    })
    return response.data;
}
export const refreshToken = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/users/refreshToken"
    })
    return response.data;
}
export const logout = async () => {

    let response = await axios({
        method:"get",
        url:"/api/users/removeSession"
    })
    return response.data;
}
export const quxiao = async (id) => {
    
    let response = await axios({
    
        method:"delete",
        url:"/api/userComments/id",
        success: function(data){
            //成功处理函数体
            return response.data;
        }
    });
}