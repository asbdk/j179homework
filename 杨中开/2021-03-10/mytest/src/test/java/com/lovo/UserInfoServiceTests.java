package com.lovo;

import com.lovo.service.ICommentService;
import com.lovo.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserInfoServiceTests {
    @Resource
    private ICommentService commentService;

    @Test
    public void testSelectByUserName(){
        System.out.println(commentService.findByCount(1, 1));
    }
}
