package com.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.UserBean;

public interface IUserService extends IService<UserBean> {
    UserBean login(String userName,String userPwd);
    void add(UserBean user);


}
