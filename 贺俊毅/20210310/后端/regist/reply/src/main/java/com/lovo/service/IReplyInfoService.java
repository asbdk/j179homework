package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.ReplyInfoBean;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
public interface IReplyInfoService extends IService<ReplyInfoBean> {
    List<ReplyInfoBean> selectAll();
    void add(ReplyInfoBean replyInfoBean);
}
