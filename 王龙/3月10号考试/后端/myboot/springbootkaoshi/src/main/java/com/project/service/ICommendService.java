package com.project.service;

import com.github.pagehelper.PageInfo;
import com.project.bean.CommendBean;

import java.util.List;

public interface ICommendService {
    /**
     * 分页查询所有评论
     * @param pageNO 页码
     * @param size 每页显示数
     * @return
     */
    PageInfo<CommendBean> findAll(Integer pageNO, Integer size);

    /**
     * 查找所有的评价
     * @return
     */
    List<CommendBean> findTotal();

    /**
     * 判断用户是否点赞
     * @param userId
     * @param commendId
     * @return
     */
    boolean checkIsZan(Long userId,Long commendId);
}
