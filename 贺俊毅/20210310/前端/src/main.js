import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from "./router/index.js"
import store from "./store/index.js"
import ajax from "./service/ajax.js"

Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.prototype.ajax=ajax
new Vue({
  store:store,
  router:router,
  render: h => h(App),
}).$mount('#app')
