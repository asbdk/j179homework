import axios from 'axios';
import qs from 'qs';   //将js转化为a=1&b=2格式的字符串
const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
export default {
    async getSubmit (serverURL,paramObj){
    let response = await axios({
        method:"get",
        url:serverURL,
        params:paramObj
    })
    return response.data;
},
   async postSubmit(serverURL,paramObj)  {
    let response = await axios({
        headers,
        method:"post",
        url:serverURL,
        data:qs.stringify(paramObj)
    })
    return response.data;
},
async uploadSubmit (param,submitURL) {
        
            let formData = new FormData();

            for(var fieldName in param){
                formData.append(fieldName, param[fieldName]);
            }
            let config = {
              headers: {
                'Content-Type': 'multipart/form-data'
              }
            }
            var response = await axios.post(submitURL, formData, config);
            return response.data;
  
        }
    }