package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.CommentBean;
import com.project.dao.ICommentDao;
import com.project.service.ICommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ICommentServiceImpl extends ServiceImpl<ICommentDao, CommentBean> implements ICommentService {
    @Resource
    private ICommentDao commentDao;

    @Override
    public List<CommentBean> selectAll() {
        return commentDao.selectAll();
    }

    @Override
    public void updateAdd(Long id, Long num) {
                commentDao.updateAdd(id, num);
    }

    @Override
    public void updateDel(Long id, Long num) {
                commentDao.updateDel(id, num);
    }

    @Override
    public Object selectByIdNum(Long id, Long num) {
        return commentDao.selectByIdNum(id, num);
    }


}
