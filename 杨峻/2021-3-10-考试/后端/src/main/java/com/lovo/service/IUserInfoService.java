package com.lovo.service;

import com.lovo.bean.DiscussBean;
import com.lovo.bean.UserInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoService extends IService<UserInfoBean> {
    boolean checkFavor(Integer userId, Integer discussId);
    UserInfoBean selectByCode(String code);
}
