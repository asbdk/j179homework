import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import ajax from './service/ajax.js'
import store from "./store/index.js"
import router from './router/index.js'

Vue.config.productionTip = false
Vue.use(ElementUI)

Vue.prototype.ajax=ajax

new Vue({
  router:router,
  store:store,
  render: h => h(App),
}).$mount('#app')
