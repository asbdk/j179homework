package com.lovo.controller;


import com.lovo.bean.ReplyInfoBean;
import com.lovo.bean.UserInfoBean;
import com.lovo.dao.IReplyInfoDAO;
import com.lovo.service.IUserInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/users")
public class UserInfoController {
    @Resource
    private IUserInfoService userInfoService;
    @Resource
    private IReplyInfoDAO replyInfoDAO;
    @GetMapping("/login")
    public Object login(String userName, String pwd, HttpSession session){
        UserInfoBean userInfoBean = userInfoService.login(userName, pwd);
        if(userInfoBean != null){
            session.setAttribute("login",userInfoBean);
        }

        return userInfoService.login(userName, pwd);

    }

    @GetMapping("/like")
    public String like(Long userId,Long replyId){
        userInfoService.like(userId, replyId);
        return "ok";
    }

    @GetMapping("/getUser")
    public UserInfoBean getUser(HttpSession session){
        return (UserInfoBean)session.getAttribute("login");
    }

    @GetMapping("/ifZan")
    public boolean ifZan(Long userId,Long replyId){
        ReplyInfoBean replyInfoBean =  replyInfoDAO.selectLike(replyId, userId);
        if(replyInfoBean==null){
            return false;
        }else {
            return true;
        }

    }


}

