import ajax from "../service/ajax.js"

export default{
    namespaced:true,

    state:{
        list:[],
        param:null,
    },


    mutations:{
        setList(state,listInfo){
            state.list = listInfo;
        }
    },


    actions:{
        async findAllComment(context){
            var ListInfo = await ajax.getSubmit("/api/comment/",context.state.param);
            context.commit("setList",ListInfo);   
        },
    },



}