import ajax from "../service/ajax.js"
export default{
    namespaced:true,    //允许使用命名空间
    state:{
        discussList:[],
    },
    mutations:{
        setDiscussList(state,discussList){
            state.discussList = discussList;
        },
    },
    actions:{
        async findDiscuss(context){
            var list = await ajax.getSubmit("/project/discuss/findDiscuss");
            context.commit("setDiscussList",list.data);
        }
    }
}