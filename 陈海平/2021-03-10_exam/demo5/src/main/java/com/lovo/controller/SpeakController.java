package com.lovo.controller;


import com.lovo.bean.UsersBean;
import com.lovo.result.ResponseResult;
import com.lovo.service.ISpeakService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/speak-bean")
@ResponseResult
@Api("用户注册和登录以及查看用户信息的控制器")
public class SpeakController {
    @Resource
    private ISpeakService service;
    @RequestMapping("/findAll")
    public Object findAll(UsersBean usersBean){
    return service.findAll(usersBean.getUserId());
    }



}

