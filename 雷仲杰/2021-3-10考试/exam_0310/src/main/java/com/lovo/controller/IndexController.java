package com.lovo.controller;

import com.lovo.result.GlobalHandleException;
import com.lovo.result.ResponseResult;
import com.lovo.result.ResultCode;
import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Api("上传文件的控制器")
@ResponseResult
public class IndexController {
    @RequestMapping("/authentication_fail")
    @ResponseBody
    public Object authenticationFail() throws GlobalHandleException {
        throw new GlobalHandleException(ResultCode.USER_AUTHENTICATION_ERROR);
    }
}
