package com.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.CommentBean;

import java.util.List;

public interface ICommentService extends IService<CommentBean> {

   List<CommentBean> selectAll();
   void updateAdd(Long id,Long num);
   void updateDel(Long id,Long num);
   Object selectByIdNum(Long id,Long num);
}
