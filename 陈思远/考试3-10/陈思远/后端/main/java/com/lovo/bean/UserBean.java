package com.lovo.bean;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("t_user")
public class UserBean {
    private static final long serialVersionUID = 1L;

    @TableId(value = "user_id",type = IdType.NONE)
    private Long userId;
    @TableField("user_name")
    private String userName;
    @TableField("user_pass")
    private String userPass;
    @TableField(exist = false)
    private List<ReviewBean> reviewBeans = new ArrayList<ReviewBean>();
}
