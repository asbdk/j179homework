package com.project.dao;

import com.project.bean.CommentInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentInfoDAO extends BaseMapper<CommentInfoBean> {
    List<CommentInfoBean> findAll();
    void del(@Param("adminId") Integer adminId, @Param("commentId") Integer commentId);
    void add(@Param("adminId") Integer adminId, @Param("commentId") Integer commentId);
    Integer findByCommentId(@Param("adminId") Integer adminId, @Param("commentId") Integer commentId);

}
