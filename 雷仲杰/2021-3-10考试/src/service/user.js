import axios from 'axios'
import qs from 'qs'
export const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}

export const logins = async (userName,pwd) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/api/users/login",
        data:qs.stringify({
            userName,pwd
        })
    })
    return response.data;
}

export const showAlls = async () => {
    let response = await axios({ 
        method:"get",
        url:"/api/comments/showAll",
        params:{}
    })
    return response.data;
}




export const updateAdd = async (userId,comId) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/api/comments/updateAdd",
        data:qs.stringify({
            userId,comId
        })
    })
    return response.data;
}