package com.lovo.dao;

import com.lovo.bean.ProductInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wanglong
 * @since 2021-02-24
 */
public interface IProductInfoDAO extends BaseMapper<ProductInfoBean> {

}
