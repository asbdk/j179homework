package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@TableName("t_role")
public class RoleBean implements Serializable {
    @TableId(value = "role_id",type = IdType.ASSIGN_ID)
    private Long id;
    @TableField("role_name")
    private String name;
    @TableField(exist = false)
    private List<GradeBean> list;
}
