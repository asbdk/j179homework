package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.UserBean;
import com.project.dao.IUserDao;
import com.project.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class IUserServiceImpl extends ServiceImpl<IUserDao,UserBean> implements IUserService {
    @Resource
    private IUserDao dao;
    /**
     * 登录方法
     * @param userName 用户姓名
     * @param userPwd 用户密码
     * @return 返回用户对象
     */
    @Override
    public UserBean login(String userName, String userPwd) {
        return dao.login(userName,userPwd);
    }

    /**
     * 注册用户
     * @param user
     */
    @Override
    public void add(UserBean user) {
        dao.insert(user);
    }


}
