package com.lovo.controller;

import com.lovo.result.GlobalHandleException;
import com.lovo.result.ResultCode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {

    @RequestMapping("/authentication_fail")
    @ResponseBody
    public Object authenticationFail() throws GlobalHandleException {
        throw new GlobalHandleException(ResultCode.USER_AUTHENTICATION_ERROR);
    }
}
