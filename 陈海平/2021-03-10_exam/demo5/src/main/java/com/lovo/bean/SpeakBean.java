package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("speak")

public class SpeakBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "speak_id",type = IdType.NONE)
    private Long speakId;
    @TableField("speak_massage")
    private String speakMassage;
    @TableField(exist = false)
    private Integer tiktokNum;
    @TableField(exist = false)
    private List<TiktokBean> tiktokBeans;
    @TableField(exist = false)
    private Integer users;

}
