package com.lovo.service.impl;

import com.lovo.bean.ProductInfoBean;
import com.lovo.dao.IProductInfoDAO;
import com.lovo.service.IProductInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wanglong
 * @since 2021-02-24
 */
@Service
public class ProductInfoServiceImpl extends ServiceImpl<IProductInfoDAO, ProductInfoBean> implements IProductInfoService {

}
