package com.lovo.service;

import com.lovo.bean.ProductInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wanglong
 * @since 2021-02-24
 */
public interface IProductInfoService extends IService<ProductInfoBean> {

}
