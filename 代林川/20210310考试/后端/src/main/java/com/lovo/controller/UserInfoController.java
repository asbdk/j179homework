package com.lovo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lovo.bean.ArticleBean;
import com.lovo.bean.UserInfoBean;
import com.lovo.result.GlobalHandleException;
import com.lovo.result.ResultCode;
import com.lovo.service.IArticleService;
import com.lovo.service.IUserInfoService;
import com.lovo.util.JWTUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/users")
public class UserInfoController {
    @Resource
    private IArticleService articleService;

    @Resource
    private IUserInfoService userInfoService;

    @PostMapping("/register")
    @ApiOperation(value = "注册请求")
    public Object register(@RequestBody UserInfoBean userInfoBean){
        // 使用SHA256加密
        userInfoBean.setUserPass(DigestUtils.sha256Hex(userInfoBean.getUserPass()));
        userInfoService.save(userInfoBean);
        return null;
    }

    @ApiOperation("登录方法")
    @PostMapping("/login")
    public Object login(String userName, String userPass) throws GlobalHandleException {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_code",userName);
        wrapper.eq("user_pass", userPass);
        UserInfoBean userInfoBean = userInfoService.getOne(wrapper);
        if(userInfoBean == null){
            throw new GlobalHandleException(ResultCode.USER_LOGIN_ERROR);
        }

        String token = JWTUtil.createToken(userInfoBean.getUserCode(),userInfoBean.getUserPass(), Calendar.MINUTE,30);
        String refreshToken = JWTUtil.createToken(userInfoBean.getUserCode().toString(),userInfoBean.getUserPass(), Calendar.DATE,7);
        Map map = new HashMap();
        map.put("token",token);
        map.put("refreshToken",refreshToken);
        return map;

    }
    @ApiOperation("点赞")
    @PostMapping("/getGreat")
    public void getGreat(Integer userId,Integer articleId) {
        //查询是否有该用户对该文章的点赞记录
        UserInfoBean userInfoBean=userInfoService.findByUidAndAid(userId, articleId);
        if(userInfoBean!=null){
            //如果找到了这条记录，则删除该记录，同时文章的点赞数减1
            userInfoService.deleteGreat(userId, articleId);
            ArticleBean articleBean=articleService.getById(articleId);
            articleBean.setNum(articleBean.getNum()-1);
            articleService.updateNum(articleBean);
        }else {
            userInfoService.addGreat(userId, articleId);
            ArticleBean articleBean=articleService.getById(articleId);
            articleBean.setNum(articleBean.getNum()+1);
            articleService.updateNum(articleBean);
        }

    }


}

