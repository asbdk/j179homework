package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("reply_info")
@AllArgsConstructor
@NoArgsConstructor
public class ReplyInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "reply_id", type = IdType.AUTO)
    private Long replyId;
    @TableField("reply_content")
    private String replyContent;
    @TableField("reply_num")
    private Long replyNum;
    @TableField(exist = false)
    private UserInfoBean userInfoBean;

    public ReplyInfoBean(String replyContent,UserInfoBean userInfoBean) {
        this.replyContent = replyContent;
        this.userInfoBean = userInfoBean;
    }
}
