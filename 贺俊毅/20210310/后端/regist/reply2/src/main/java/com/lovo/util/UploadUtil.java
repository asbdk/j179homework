package com.lovo.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;

public  class UploadUtil {
    public static String Upload(MultipartFile mf, String dirPath) {
        System.out.println("进入打入");
        //得到上传文件名
        String fileName = mf.getOriginalFilename();
        try {
            //重命名文件
            fileName = System.currentTimeMillis() + fileName.substring(fileName.lastIndexOf("."));
            //得到上传文件存放目录的真实路径
            URL url = null;
            url = Thread.currentThread().getContextClassLoader()
                    .getResources(dirPath).nextElement();
            String filePath = URLDecoder.decode(url.getFile(), "utf-8");
            System.out.println("写入前");
            //将上传文件的二进制数据，写入指定的文件
            mf.transferTo(new File(filePath + "/" + fileName));
            System.out.println("写入后");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }




}
