package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.RoleInfoBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-05
 */
public interface IRoleInfoService extends IService<RoleInfoBean> {

}
