package com.lovo.service;

import com.lovo.bean.FunctionInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.RoleInfoBean;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-05
 */
public interface IFunctionInfoService extends IService<FunctionInfoBean> {
    List selectByRoleId(List<RoleInfoBean> roleInfoBeans);
    void add(FunctionInfoBean functionInfoBean) throws Exception;
    void delete(Long id) throws Exception;
}
