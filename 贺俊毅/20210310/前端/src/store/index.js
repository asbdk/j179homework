import Vue from "vue";
import Vuex from "vuex"; // 导入Vuex
import login from "./login.js";
import reply from "./reply.js";





Vue.use(Vuex); //应用Vuex

export default new Vuex.Store({
    modules:{
        login,reply
    },
    //定义共享数据,主要用于显示
    state:{
        // num:10
    },
    getters:{//定义计算属性
        // computNum(state){
        //     return "$"+state.num
        // }
    },
    mutations:{
        // addNum(state){//改变state方法
        //     state.num ++;
        // }
    }
});