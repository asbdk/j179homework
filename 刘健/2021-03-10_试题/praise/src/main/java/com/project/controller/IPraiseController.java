package com.project.controller;

import com.project.service.IPraiseService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("praise")
public class IPraiseController {

    @Resource
    private IPraiseService service;
    @RequestMapping("add")
    public String add(Long userId,Long commentId){
        if(service.addPraise(userId,commentId)){
            return "ok";
        }
        return "on";
    }
}
