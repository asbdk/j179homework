1、说说你对Spring MVC的理解
MVC是Model—View—Controler的简称。即模型—视图—控制器。MVC是一种设计模式，它强制性的把应用程序的输入、处理和输出分开。 


2、谈谈你对Spring AOP的理解
AOP面向切面编程，通过预编译和程序运行期间动态代理实现同一维护，，实现交叉非核心业务，可以是方法，异常-抛出，

3、SpringMVC常用的注解有哪些？
@Controller  @RequestMapping @ResponseBody  @RestController

4、Spring AOP和AspectJ AOP有什么区别？
Spring AOP基于动态代理来实现，默认如果使用接口的，用JDK提供的动态代理实现，如果是方法则使用CGLIB实现
Spring AOP需要依赖IOC容器来管理，并且只能作用于Spring容器，使用纯Java代码实现
AspectJ属于静态织入，通过修改代码来实现


5、什么是通知？有哪些类型？
前置通知，后通知，环绕通知，异常通知

6、说说事务的隔离级别和传播级别
隔离级别
ISOLATION_DEFAULT使用后端数据库默认的隔离级别。

ISOLATION_READ_UNCOMMITTED允许读取尚未提交的更改。可能导致脏读、幻影读或不可重复读。

ISOLATION_READ_COMMITTED允许从已经提交的并发事务读取。可防止脏读，但幻影读和不可重复读仍可能会发生。

ISOLATION_REPEATABLE_READ对相同字段的多次读取的结果是一致的，除非数据被当前事务本身改变。可防止脏读和不可重复读，但幻影读仍可能发生。

ISOLATION_SERIALIZABLE完全服从ACID的隔离级别，确保不发生脏读、不可重复读和幻影读。这在所有隔离级别中也是最慢的，因为它通常是通过完全锁定当前事务所涉及的数据表来完成的。

传播级别
PROPAGATION_MANDATORY表示该方法必须运行在一个事务中。如果当前没有事务正在发生，将抛出一个异常

PROPAGATION_NESTED表示如果当前正有一个事务在进行中，则该方法应当运行在一个嵌套式事务中。
被嵌套的事务可以独立于封装事务进行提交或回滚。如果封装事务不存在，行为就像PROPAGATION_REQUIRES一样。

PROPAGATION_NEVER表示当前的方法不应该在一个事务中运行。如果一个事务正在进行，则会抛出一个异常。

PROPAGATION_NOT_SUPPORTED表示该方法不应该在一个事务中运行。如果一个现有事务正在进行中，它将在该方法的运行期间被挂起。

PROPAGATION_SUPPORTS表示当前方法不需要事务性上下文，但是如果有一个事务已经在运行的话，它也可以在这个事务里运行。

PROPAGATION_REQUIRES_NEW表示当前方法必须在它自己的事务里运行。一个新的事务将被启动，而且如果有一个现有事务在运行的话，则将在这个方法运行期间被挂起。

PROPAGATION_REQUIRES表示当前方法必须在一个事务中运行。如果一个现有事务正在进行中，该方法将在那个事务中运行，否则就要开始一个新事务。




7、Spring事务的实现方式有哪些？
基于xml配置文件的方式；另一个实在业务方法上进行@Transaction注解，将事务规则应用到业务逻辑中。


8、Spring 框架的事务管理器有哪些优点？
1.控制反转将对象的创建交给了spring,简化了开发，降低了代码之间的耦合性和侵入性。

2.方便对程序进行声明式事物管理，我们只需通过配置就可以完成对事物管理。

3.方便集成各种优秀的框架，spring不排斥各种优秀框架，其内部提供了对各种优秀框架如(struts2，hibernate,mybatis,quartz，jpa)等的直接支。

4.方便对程序进行测试，spring对于Junit4的支持，可通过注解方便测试程序。

5.降低了JavaEE API的使用难度，JDBC,Javamail,远程调用等，spring对它们进行了封装，使这些API的使用难度大大降低。



9、事务的三要素是什么？
原子性(Atomicity） 一致性（Consistency）隔离性（Isolation）

10、什么是乐观锁？什么是悲观锁？
乐观锁悲观锁语句

SELECT ... LOCK In SHARE MODE;

SELECT ... FOR UPDATE;

悲观锁：假设每一次拿数据，都有认为会被修改，所以给数据库的行或表上锁。要注意for update要用在索引上，不然会锁表。
乐观锁：就是很乐观，每次去拿数据的时候都认为别人不会修改。更新时如果version变化了，更新不会成功。
