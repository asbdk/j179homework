1、Spring Boot是什么？
Spring Boot 是springMVC的升级版，其可以允许使用者经过基本配置后就能够实现布置好spring工程，相较于springMVC更加简化了配置操作
2、Spring Boot有哪些优点？

1，配置文件相对简单，实体类自动启动类和实体类，可以安装许多简化操作的插件；轻量化，功能强大，Spring Boot 是解决这个问题的方法。
Spring Boot 已经建立在现有 spring 框架之上。使用 spring 启动，我们避免了之前我们必须做的所有样板代码和配置。因此，Spring Boot 可
以帮助我们以最少的工作量，更加健壮地使用现有的 Spring功能

3、Spring Boot自动配置的原理是什么？
Spring Boot启动的时候会通过@EnableAutoConfiguration注解找到META-INF/spring.factories配置文件中的所有自动配置类，并对其进行加载，
而这些自动配置类都是以AutoConfiguration结尾来命名的，它实际上就是一个JavaConfig形式的Spring容器配置类，它能通过以Properties结尾命
名的类中取得在全局配置文件中配置的属性如：server.port，而XxxxProperties类是通过@ConfigurationProperties注解与全局配置文件中对应的
属性进行绑定的

4、介绍下Spring Boot配置的加载顺序。
命令行参数。所有的配置都可以在命令行上进行指定；
来自java:comp/env的JNDI属性；
Java系统属性（System.getProperties()）；
操作系统环境变量 ；
jar包外部的application-{profile}.properties或application.yml(带spring.profile)配置文件
jar包内部的application-{profile}.properties或application.yml(带spring.profile)配置文件 再来加载不带profile
jar包外部的application.properties或application.yml(不带spring.profile)配置文件
jar包内部的application.properties或application.yml(不带spring.profile)配置文件
@Configuration注解类上的@PropertySource

5、YAML配置比起Properties配置的优势在哪里？
首先yaml配置可以分级，层级结构更加简单明了，其次是加载顺序优先于Properties文件

6、如何实现Spring Boot的热部署
导入热部署相关依赖：
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-devtools</artifactId>
   <optional>true</optional>
</dependency>
自定以配置热部署（文件）：
# 热部署开关，false即不启用热部署
spring.devtools.restart.enabled: true
# 指定热部署的目录
#spring.devtools.restart.additional-paths: src/main/java
# 指定目录不更新
spring.devtools.restart.exclude: test/**
Intellij Idea修改：
 1、勾上自动编译或者手动重新编译：
    File > Settings > Compiler-Build Project automatically
 2、注册：
    ctrl + shift + alt + / > Registry > 勾选Compiler autoMake allow when app running

7、Spring Boot的核心注解有哪些？
@SpringBootApplication（等于同时添加注解@Configuration ， @EnableAutoConfiguration 和 @ComponentScan）
@RestController （Spring MVC 注解）
@RequestMapping（Spring MVC 注解）
@EnableAutoConfiguration （扫描jar包，来配置spring工程）
@Configuration（spring 主要源）
@ResponseBody（Spring MVC 注解将获得的数据封装成一个json数据对象）
@AutoWired（自动导入适配的beanFactory管理的bean）

8、介绍下Spring Boot中的监视器。
Spring boot actuator是spring启动框架中的重要功能之一。Spring boot监视器可帮助您访问生产环境中正在运行的应用程序的当前状态。
有几个指标必须在生产环境中进行检查和监控。即使一些外部应用程序可能正在使用这些服务来向相关人员触发警报消息。监视器模块公开
了一组可直接作为HTTP URL访问的REST端点来检查状态

9、什么是 Spring Boot Stater ？
大概意思就是说starter是一种对依赖的synthesize（合成）能自动把配置文件搞好，不用我们手动配置。所以说，Spring Boot 是简化配置。

10、Spring 和 Spring Boot 有什么不同？
Spring 框架提供多种特性使得 web 应用开发变得更简便，包括依赖注入、数据绑定、切面编程、数据存取等等。
随着时间推移，Spring 生态变得越来越复杂了，并且应用程序所必须的配置文件也令人觉得可怕。这就是 Spirng Boot 派上用场的地方了 –
它使得 Spring 的配置变得更轻而易举。
实际上，Spring 是 unopinionated（予以配置项多，倾向性弱） 的，Spring Boot 在平台和库的做法中更 opinionated ，使得我们更容易上手。
这里有两条 SpringBoot 带来的好处：
根据 classpath 中的 artifacts 的自动化配置应用程序
提供非功能性特性例如安全和健康检查给到生产环境中的应用程序

