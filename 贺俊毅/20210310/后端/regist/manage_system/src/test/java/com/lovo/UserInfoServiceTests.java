package com.lovo;

import com.lovo.service.IUserInfoService;
import com.lovo.util.RedisUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserInfoServiceTests {
    @Resource
    private IUserInfoService userInfoService;
    @Resource
    private RedisUtil redisUtil;
    @Test
    public void testSelectAll(){
        System.out.println(userInfoService.selectAll(1,5));
    }
    @Test
    public void testAdd(){
//        UserInfoBean userInfoBean = new UserInfoBean();
//        userInfoBean.setUserName("xxxxxx");
//        userInfoBean.setUserPass(DigestUtils.sha256Hex("123123"));
//        userInfoService.add(userInfoBean);
        for(int i = 0;i <10;i++){
            System.out.println(redisUtil.generateOrderId());
        }
    }
    @Test
    public void testSelectByUserName(){
        System.out.println(userInfoService.selectByUserName("abc123"));
    }
}
