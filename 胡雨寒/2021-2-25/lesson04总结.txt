1、什么是 YAML？
是配置文件数据中心基于xml，json，配置文件数据结构更清晰
2、什么是JPA？
jpa是javapersistenceAPI，基于OR的标准，简化javaee和持久化的开发，
3、阐述Spring Data JPA的概念
是orm规范，hibernate也是jpa规范具体实现，是面向对象的思想进行持久层开发，
4、描述MyBatis和Hibernate框架的区别
mybatis是没有封装数据，hibernate对数据结构完整的封装方法可以直接调用，mybatis可以更复杂洗澡的sql语句
hibernate是持久层开发比mybatis简化，
5、什么是JPQL，它和SQL有什么区别
jpql是面向对象的查询语言，可以继承多态关联关系，sql是面向关系查询语言，sql操作的是数据表列。
6、阐述Jpa、Hibernate、Spring Data Jpa三者之间的关系。
springdatajpa是一个规范，hibernate实现了jpa的规范，datajpa是更高级的封装
7、Spring Data提供了哪些接口来实现持久化操作？请详细描述它们。
jparepository接口 repository父接口  pagingandsortingrepository分页
8、描述实体对象在持久化容器中的状态。
持久状态，游离状态，临时状态
9、自定义持久层接口的方法有哪些？
and  or betw  lesseen  isnull isnot notnull  not  in
10、在Spring Data JPA中，ID生成策略有哪些？如何实现？
GenericGenerator(name="idGenerator", strategy="uuid")
GenericGenerator(name = "hbincrement",strategy = "increment")
GeneratedValue(strategy = GenerationType.IDENTITY)