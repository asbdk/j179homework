import Vue from 'vue'
import VueRouter from 'vue-router'
import Register from '../pages/Register'
import Login from '../pages/Login'
import Manage from '../pages/Manage'
import User from '../pages/User'
import CommentLogin from '../pages/CommentLogin'
import Comment from '../pages/Comment'

Vue.use(VueRouter)

const routes = [
    {path:'/login',component:Login},
    {path:'/commentLogin',component:CommentLogin},
    {path:'/register',component:Register},
    {path:'/comment',component:Comment},
    {path:'/manage',component:Manage,
    children:[
        {path:'/manage/user',component:User}
    ]    
}
]

export default new VueRouter({
    mode:'history',
    routes
})