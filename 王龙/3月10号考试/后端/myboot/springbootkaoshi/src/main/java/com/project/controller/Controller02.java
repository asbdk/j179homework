package com.project.controller;

import com.project.bean.UserBean;
import com.project.service.IUserService;
import com.project.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/users")
@Api("kankan")
public class Controller02 {
    @Resource
    private IUserService userService;

    @PostMapping("/login")
    public Object login(String name,String pwd){
        UserBean userBean = userService.login(name, pwd);
        if(userBean == null){
            return  null;
        }
        String token = JWTUtil.createToken(userBean.getName(),userBean.getPwd(), Calendar.MINUTE,30);
        String refreshToken = JWTUtil.createToken(userBean.getName().toString(),userBean.getPwd(), Calendar.DATE,7);
        Map map = new HashMap();
        map.put("token",token);
        map.put("refreshToken",refreshToken);
        return map;
    }

    @ApiOperation(value = "点赞",notes = "增加点赞")
    @PostMapping("/addZan")
    public Object addZan(Long commendId){
        UserBean userBean = (UserBean) SecurityUtils.getSubject().getPrincipal();
        Long userId = 1l;
        userId = userBean.getId();
        try{
            userService.addZan(userId,commendId);
        }catch (Exception e){
            e.printStackTrace();
            return "no";
        }
        return "ok";
    }

    @ApiOperation(value = "取消赞",notes = "取消点赞")
    @GetMapping("/removeZan")
    public Object removeZan(Long commendId){
        UserBean userBean = (UserBean) SecurityUtils.getSubject().getPrincipal();
        Long userId = userBean.getId();
        try{
            userService.removeZan(userId,commendId);
        }catch (Exception e){
            e.printStackTrace();
            return "no";
        }
        return "ok";
    }


    @RequestMapping("/add")
    @ResponseBody
    public String add(){
        System.out.println(userService.login("张三", "123"));
        return "hello";
    }
}
