package com.project.service;

import com.github.pagehelper.PageInfo;
import com.project.bean.ManBean;

public interface IManService {
    public int PAGESIZE=3;
    public void add(ManBean man);
    public PageInfo<ManBean> findAll(int pageNO);
}
