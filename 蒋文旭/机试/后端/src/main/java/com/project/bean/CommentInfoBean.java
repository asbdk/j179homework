package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("comment_info")
public class CommentInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "comment_id", type = IdType.AUTO)
    private Long commentId;

    /**评论内容*/
    @TableField("comment_content")
    private String commentContent;

    /**评论点赞总数*/
    @TableField("comment_likeCount")
    private int commentLikeCount = 0;

    @TableField(exist = false)
    private UserInfoBean userInfoBean ;

    public CommentInfoBean(Long commentId, String commentContent) {
        this.commentId = commentId;
        this.commentContent = commentContent;
    }
}
