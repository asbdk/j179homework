package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.UserCommentBean;
import org.apache.ibatis.annotations.Param;

public interface IUserCommentsDao extends BaseMapper<UserCommentBean> {
    UserCommentBean find(@Param("userId") Long userId, @Param("commentsId") Long commentsId);

}
