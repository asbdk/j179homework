package com.lovo.service;

import com.lovo.bean.DiscussBean;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IDiscussService extends IService<DiscussBean> {
    void deleteFavor(Integer userId, Integer discussId);
    void addFavor(Integer userId, Integer discussId);
    List<DiscussBean> findDiscuss();
}
