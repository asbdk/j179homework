package com.project.controller;

import com.project.result.ResponseResult;
import com.project.util.WebUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/grade")
public class GradeController {
    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/test")
    @ResponseBody
    @ResponseResult
    public Object getCode(String code){
        String tokenUrl = "https://gitee.com/oauth/token?" +
                "grant_type=authorization_code" +
                "&code="+ code +
                "&client_id=5ce37843b772b835323c39254b9c3296640a4e5becdc370f51f5372d308823d9" +
                "&redirect_uri=http://localhost:8080/project/grade/test" +
                "&client_secret=aae838042a19ae9fb54d819611c7e1263b108e4971c634bb5088a9b9adeac24b";
        String resourceUrl = "https://gitee.com/api/v5/user?access_token=";
        //获取令牌
        Map<String,String> tokenMap = restTemplate.postForObject(tokenUrl, WebUtil.httpEntity(), Map.class);
        // 根据令牌获取第三方应用资源
        Map<String,String> resourceMap = restTemplate.getForObject(resourceUrl + tokenMap.get("access_token"),Map.class);

        return  resourceMap;
    }


    @RequestMapping("/add")
    public String add(){
        return "权限添加";
    }
}
