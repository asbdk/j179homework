package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author bdk
 * @since 2021-03-02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("user_info")
public class UserInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "user_id",type = IdType.NONE)
    private Long userId;
    @TableField("user_name")
    private String userName;
    @TableField("user_pass")
    private String userPass;
    @TableField("user_header")
    private String userHeader;
    @TableField(exist = false)
    private List<RoleInfoBean> roleInfoBeans = new ArrayList<RoleInfoBean>();

}
