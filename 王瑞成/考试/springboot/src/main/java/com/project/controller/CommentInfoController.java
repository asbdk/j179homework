package com.project.controller;


import com.project.result.ResponseResult;
import com.project.result.UserValid;
import com.project.service.ICommentInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@ResponseResult
@RequestMapping("/comment")
public class CommentInfoController {
    @Resource
    ICommentInfoService commentInfoService;
    @ApiOperation(value = "显示所有评论")
    @GetMapping("/findAll")
    @UserValid
    public Object findAll(){
        return commentInfoService.findAll();
    }

    @ApiOperation("点赞/取消点赞")
    @GetMapping("/num")
    public Object updateNum(Integer adminId, Integer commentId){
        Integer flag = commentInfoService.findByCommentId(adminId,commentId);
        if(flag == 1){
            commentInfoService.del(adminId,commentId);
        }else {
            commentInfoService.add(adminId,commentId);
        }
        return "ok";
    }

}

