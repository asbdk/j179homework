package com.lovo.service;

import com.lovo.bean.UserInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-02
 */
public interface IUserInfoService extends IService<UserInfoBean> {
    Object selectAll(Integer page, Integer size);
    void add(UserInfoBean userInfoBean);
    UserInfoBean selectByUserName(String userName);
}
