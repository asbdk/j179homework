package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.CommentBean;

import java.util.List;

public interface ICommentService extends IService<CommentBean> {
    void addComment(CommentBean commentBean);

    List<CommentBean> findAllByOrder();

    List<CommentBean> checkCommentById(Integer id);
}
