package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.UserBean;

public interface IUserDao extends BaseMapper<UserBean> {
    UserBean findByName(String name);
}
