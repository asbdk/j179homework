package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.PraiseBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IPraiseDao extends BaseMapper<PraiseBean> {

    PraiseBean findByPraise(@Param("commentId")Long commentId,@Param("userId") Long userId);

    boolean addPraise(@Param("userId") Long userId, @Param("commentId") Long commentId);

    void deletePraise(@Param("userId") Long userId, @Param("commentId") Long commentId);
}
