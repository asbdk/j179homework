
import Vue from "vue";//导入vue组件
import Vuex from "vuex"; // 导入Vuex
import ajax from "../service/ajax.js"

Vue.use(Vuex); //应用Vuex

export default new Vuex.Store({
   
    //定义共享数据，主要用于显示
    state:{
        num:10,//定义初始值
        showLogin:false,
        userList:[],
        loginUser:{},
        speakList:[],
        param:0
    },
    getters:{//定义计算属性
        myCountnum(state){
            return "$"+state.num;
        }
    },
    mutations:{//改变state方法
        addNum(state){
            state.num ++;
        },
        delNum(state){
            state.num --;
        },
        setShowLogin(state,isShow){
            state.showLogin=isShow
        },
        setSpeakList(state,info){
            state.speakList=info
        },
        setUser(state,info){
            state.loginUser=info
        },
        setParam(state,info){
            state.param=info

        }
    },
    actions:{
        async findAll(context){//context是一个容器固定的参数写法
            var info=await ajax.getSubmit("/project/speak-bean/findAll");
            alert(JSON.stringify(info))
            context.commit("setSpeakList",info)//调方法设置参数值
        },
        async getUsers(context){//context是一个容器固定的参数写法
    
            var info=await ajax.getSubmit("/project/user/getUser");
            context.commit("setUser",info)//调方法设置参数值
        },
       
    },


});

