package com.project.service.impl;

import com.project.bean.UserInfoBean;
import com.project.dao.IUserInfoDAO;
import com.project.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wangcm
 * @since 2021-03-10
 */
@Service
@Transactional
public class UserInfoServiceImpl extends ServiceImpl<IUserInfoDAO, UserInfoBean> implements IUserInfoService {

    @Resource
    private IUserInfoDAO userInfoDAO;

    @Override
    public void addLikes(int userId,int commentId) {
        userInfoDAO.addLikes(userId, commentId);
    }

    @Override
    public void cancelLikes(int userId, int commentId) {
        userInfoDAO.cancelLikes(userId, commentId);
    }

    @Override
    public Integer checkLikes(Integer userId, Integer comId) {
        return userInfoDAO.checkLikes(userId,comId);
    }
}
