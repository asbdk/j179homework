package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.project.bean.CommendBean;
import com.project.dao.ICommendDAO;
import com.project.service.ICommendService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class CommendServiceImpl extends ServiceImpl<ICommendDAO, CommendBean> implements ICommendService {
    @Resource
    private ICommendDAO iCommendDAO;
    @Override
    public PageInfo<CommendBean> findAll(Integer pageNO, Integer size) {
        PageHelper.startPage(pageNO,size);
        return PageInfo.of(iCommendDAO.findByPage());
    }

    @Override
    public List<CommendBean> findTotal() {
        return iCommendDAO.findByPage();
    }

    @Override
    public boolean checkIsZan(Long userId, Long commendId) {
        Long id = iCommendDAO.checkIsZan(userId, commendId);
//        System.out.println("**************"+id);
        if(id == null || id<1){
            return false;
        }
        return true;
    }
}
