package com.lovo.service;

import com.lovo.bean.RoleInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-05
 */
public interface IRoleInfoService extends IService<RoleInfoBean> {

}
