import axios from 'axios'
import qs from 'qs'
export const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
export const register = async (userName,userPass,userHeader) => {
    let response = await axios({
        
        method:"post",
        url:"/project/users",
        data:{
            userName,userPass,userHeader
        }
    })
    return response.data;
}
export const login = async (userName,userPass) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/project/users/login",
        data:qs.stringify({
            userName,userPass
        })
    })
    return response.data;
}
export const showAll = async (page=1,size=5) => {
    console.log(page,size)
    let response = await axios({
        method:"get",
        url:"/project/users",
        params:{page,size}
    })
    return response.data;
}
export const getUser = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/project/users/token"
    })
    return response.data;
}
export const refreshToken = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/project/users/refreshToken"
    })
    return response.data;
}
export const logout = async () => {

    let response = await axios({
        method:"get",
        url:"/project/users/removeSession"
    })
    return response.data;
}