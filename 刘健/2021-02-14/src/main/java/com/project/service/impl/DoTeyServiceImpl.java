package com.project.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.project.bean.DoTeyBean;
import com.project.mapper.IDoTeyMapper;
import com.project.service.DoTeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DoTeyServiceImpl implements DoTeyService {
    @Autowired
    private IDoTeyMapper mapper;
    @Override
    public void addDoTey(int userId, DoTeyBean doTey) {
        mapper.addDoTey(userId,doTey);
    }

    @Override
    public void update(int id,String info) {
        mapper.update(id,info);
    }

    @Override
    public PageInfo<DoTeyBean> findByItem(int pageNO,int userId, String type, String doTeyName, String rank) {
        PageHelper.startPage(pageNO, PAGESIZE);
        PageInfo<DoTeyBean>info = new PageInfo<>(mapper.findByItem(userId,type,doTeyName,rank));
        return info;
    }

    @Override
    public void del(int doTeyId) {
        mapper.del(doTeyId);
    }
}
