import {showAll} from '../service/user'
export default {
    namespaced:true,
    state:{
        token:"",
        reviews:[]
    },
    mutations:{
        setToken(state,token){
            state.token = token
        },
        setReviews(state,reviews){
            state.reviews = reviews;
        },

    },
    actions:{
        async setReviews({commit}){
            let data = await showAll();
            commit("setReviews",data.data.records);
        }
    }
}