1、Spring Boot是什么？

pring Boot 其实不是什么新的框架，它默认配置了很多框架的使用方式，从本质上来说，springboot是一个专注于框架的框架。
Spring Boot就是对各种框架的整合，让他们集成在一起更加简单，简化了我们在集成过程中的模板化配置，它做了那些没有它
你自己也会去做的Spring Bean配置。你不用再写这些样板配置
了，可以专注于应用程序的逻辑

2、Spring Boot有哪些优点？

为spring的开发提供了更好的入门体验
零配置
集成了大量常用的第三方配置，为这些第三方配置提供了开箱即用的能力
提供了一系列大型项目的非公能性特征，如嵌入服务器、安全性、度量、运行状态检查

3、Spring Boot自动配置的原理是什么？

SpringBoot的核心就是自动配置，自动配置又是基于条件判断来配置Bean。关于自动配置的源码在spring-boot-autoconfigure-2.0.3.RELEASE.jar
@EnableAutoConfiguration可以帮助SpringBoot应用将所有符合条件的@Configuration配置都加载到当前SpringBoot创建并使用的IoC容器:
通过@Import(AutoConfigurationImportSelector.class)导入的配置功能，
AutoConfigurationImportSelector中的方法getCandidateConfigurations，得到待配置的class的类名集合,这个集合就是所有需要进行自动配置
的类，而是是否配置的关键在于META-INF/spring.factories文件中是否存在该配置信息

4、介绍下Spring Boot配置的加载顺序。

1、开发者工具 `Devtools` 全局配置参数；

2、单元测试上的 `@TestPropertySource` 注解指定的参数；

3、单元测试上的 `@SpringBootTest` 注解指定的参数；

4、命令行指定的参数，如 `java -jar springboot.jar --name="Java技术栈"`；

5、命令行中的 `SPRING_APPLICATION_JSONJSON` 指定参数, 如 `java -Dspring.application.json='{"name":"Java技术栈"}' -jar springboot.jar`

6、`ServletConfig` 初始化参数；

7、`ServletContext` 初始化参数；

8、JNDI参数（如 `java:comp/env/spring.application.json`）；

9、Java系统参数（来源：`System.getProperties()`）；

10、操作系统环境变量参数；

11、`RandomValuePropertySource` 随机数，仅匹配：`ramdom.*`；

12、JAR包外面的配置文件参数（`application-{profile}.properties（YAML）`）

13、JAR包里面的配置文件参数（`application-{profile}.properties（YAML）`）

14、JAR包外面的配置文件参数（`application.properties（YAML）`）

15、JAR包里面的配置文件参数（`application.properties（YAML）`）

16、`@Configuration`配置文件上 `@PropertySource` 注解加载的参数；

17、默认参数（通过 `SpringApplication.setDefaultProperties` 指定）；

5、YAML配置比起Properties配置的优势在哪里？

配置有序。在一些特殊场景下，配置有序很关键。
支持数组，数组中的元素可以是基本数据类型也可以是对象。
简洁。
相比properties配置文件，YAML还有一个缺点，就是不支持@PropertySource注解导入自定义的YAML配置。

6、如何实现Spring Boot的热部署

springboot有三种实现热部署方式：

1. 使用springloaded配置pom.xml文件，使用mvn spring-boot:run启动

2. 使用springloaded本地加载启动，配置jvm参数-javaagent:<jar包地址> -noverify

3. 使用devtool工具包，操作简单，但是每次需要重新部署

7、Spring Boot的核心注解有哪些？

@SpringBootConfiguration：标记当前类为配置类
@EnableAutoConfiguration：开启自动配置
@ComponentScan：扫描主类所在的同级包以及下级包里的Bean
关键是@EnableAutoConfiguration

8、介绍下Spring Boot中的监视器。

Spring boot actuator 是 spring 启动框架中的重要功能之一。Spring boot 监视器可帮助您访问生产环境中正在运行的应用程序的当前状态。
有几个指标必须在生产环境中进行检查和监控。即使一些外部应用程序可能正在使用这些服务来向相关人员触发警报消息。监视器模块公开了一
组可直接作为 HTTP URL 访问的REST 端点来检查状态。

9、什么是 Spring Boot Stater ？

Starter主要用来简化依赖用的。比如我们之前做MVC时要引入日志组件，那么需要去找到log4j的版本，然后引入，现在有了Starter之后，
直接用这个之后，log4j就自动引入了，也不用关心版本这些问题。

10、Spring 和 Spring Boot 有什么不同？

Spring 框架提供多种特性使得 web 应用开发变得更简便，包括依赖注入、数据绑定、切面编程、数据存取等等。

随着时间推移，Spring 生态变得越来越复杂了，并且应用程序所必须的配置文件也令人觉得可怕。这就是 Spirng Boot 派上用场的地方了 – 
它使得 Spring 的配置变得更轻而易举。

实际上，Spring 是 unopinionated（予以配置项多，倾向性弱） 的，Spring Boot 在平台和库的做法中更 opinionated ，使得我们更容易上手。

