package com.lovo.service;

import com.lovo.bean.UserInfosBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hjy
 * @since 2021-03-02
 */
public interface IUserInfosService extends IService<UserInfosBean> {
    UserInfosBean login(String userName,String userPass);
    Object selectAll(Integer page,Integer size);


}
