import ajax from "../service/ajax.js"
export default{
    namespaced:true,    //允许使用命名空间
    state:{
        userBean:{}
    },
    mutations:{
        setUserBean(state,obj){
            state.userBean = obj;
        }
    },
    actions:{
        async config(context){
            var User =await ajax.getSubmit("/project/user/add",{userBean});
            context.commit("setUserBean",User);
        },
    }
}