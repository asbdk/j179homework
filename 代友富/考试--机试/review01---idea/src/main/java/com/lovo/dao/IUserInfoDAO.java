package com.lovo.dao;

import com.lovo.bean.UserInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    UserInfoBean login(@Param("userName") String userName, @Param("userPwd") String userPwd);
    UserInfoBean selectByUserName(String userName);
}
