package com.project.dao;


import com.project.bean.StudentBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IStudentDAO {
    void insert(StudentBean studentBean);
    StudentBean findById(int id);
    void del(int id);
    void update(@Param("id") int id,@Param("age") String age);
    List<StudentBean> findAll();
}
