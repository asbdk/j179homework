package com.lovo.service.impl;

import com.lovo.bean.UserBean;
import com.lovo.dao.IUserDAO;
import com.lovo.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class UserServiceImpl extends ServiceImpl<IUserDAO, UserBean> implements IUserService {
    @Resource
    private IUserDAO userInfoDAO;

    @Override
    public UserBean selectByUserName(String userName) {
        return userInfoDAO.selectByUserName(userName);
    }
}
