package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.GradeBean;
import com.project.bean.RoleBean;
import com.project.dao.IGradeDao;
import com.project.service.IGradeService;
import com.project.util.ShiroUtil;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class GradeServiceImpl extends ServiceImpl<IGradeDao, GradeBean> implements IGradeService {
    @Resource
    private IGradeDao gradeDao;
    @Resource
    @Lazy
    private ShiroFilterFactoryBean shiroFilterFactoryBean;
    @Override
    public List<GradeBean> findByRoleList(List<RoleBean> roleBeanList) {
//        gradeDao.selectList(null);
        return gradeDao.findByRoleList(roleBeanList);

    }
    @Override
    public void add(GradeBean gradeBean) throws Exception {
        gradeDao.insert(gradeBean);
        List functionInfoBeans = gradeDao.selectList(null);
        Map map = ShiroUtil.loadFilterChainMap(functionInfoBeans);
        ShiroUtil.updatePermission(shiroFilterFactoryBean,map);
    }

    @Override
    public void delete(Long id) throws Exception {
        gradeDao.deleteById(id);
        List functionInfoBeans = gradeDao.selectList(null);
        Map map = ShiroUtil.loadFilterChainMap(functionInfoBeans);
        ShiroUtil.updatePermission(shiroFilterFactoryBean,map);
    }


    @Override
    public List<GradeBean> findAll() {
        return gradeDao.selectList(null);
    }
}
