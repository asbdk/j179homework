package com.lovo.service;

import com.lovo.bean.UserInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoService extends IService<UserInfoBean> {
    UserInfoBean findByUidAndAid(Integer userId,Integer articleId);
    UserInfoBean selectByUserName(String username);
    void deleteGreat(Integer userId,Integer articleId);
    void addGreat(Integer userId,Integer articleId);
}
