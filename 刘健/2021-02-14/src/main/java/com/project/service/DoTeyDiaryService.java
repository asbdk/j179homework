package com.project.service;

import com.github.pagehelper.PageInfo;
import com.project.bean.DoTeyDiaryBean;

/**
 * 宠物日记业务接口
 */
public interface DoTeyDiaryService {

    public int PAGESIZE=3;
    /**
     *按宠物id分页查询宠物日志信息
     * @param pageNO 页码
     * @param doTeyDiaryId 宠物id，日志宠物外键
     * @return 宠物日志分页对象
     */
    public PageInfo<DoTeyDiaryBean> findByItem(int pageNO,int doTeyDiaryId);

    /**
     * 按id添加宠物日志信息
     * @param doTeyId  宠物id
     * @param doTeyDiary 日志对象
     */
    public void addDiary( int doTeyId,DoTeyDiaryBean doTeyDiary);

    /**
     * 按日志id查看日志对象
     * @param diaryId 日志id
     * @return 返回日志对象
     */
    public DoTeyDiaryBean findById(int diaryId);

    /**
     * 按宠物日志id删除宠物日志
     * @param diaryId 日志id
     */
    public void delDiary(int diaryId);

    //批量删除
    public void batchDel();

}
