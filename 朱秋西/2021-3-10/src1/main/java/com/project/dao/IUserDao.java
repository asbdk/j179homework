package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.UserBean;

public interface IUserDao extends BaseMapper<UserBean> {
    public Object login(String name, String wpd);
}
