package com.lovo.service;

import com.lovo.bean.UserInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoService extends IService<UserInfoBean> {
    UserInfoBean login(String userName, String userPwd);
    UserInfoBean selectByUserName(String userName);

}
