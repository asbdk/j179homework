1、实现状态管理的方式有哪些？各自的特点是什么？
    1，隐藏表单：将值隐藏放在HTML表单。当用户提交表单时，隐藏中的值也传送到服务器。不安全，太繁琐。
    2，cookie：cookie是自动地在Web服务器和浏览器之间来回传递的一小块信息。cookie适用于那些需要跨越许多页面的信息，其信息保存于客户端，安全性小，
在同一个网站中，可以在用户访问不同资源时，都可以将cookie信息发送给服务器。
    3，session：以Map的方式保存于服务器端，其中sessionId交给客户端以cookie方式保存，保存的信息量大，安全性更高
    4，token:Token是通过加密算法来实现session对象验证的，这样使得攻击者无法伪造token来达到攻击或者其他对服务器不利的行为。

2、什么是jwt？
     Json web token（JWT）是为了网络应用环境间传递声明而执行的一种基于JSON的开发标准，该token被设计为紧凑且安全的，特别适用于分布式
站点的单点登陆（SSO）场景。JWT的声明一般被用来在身份提供者和服务提供者间传递被认证的用户身份信息，以便于从资源服务器获取资源，也可以增加一些额外
的其它业务逻辑所必须的声明信息，该token也可直接被用于认证，也可被加密。

3、什么是https？它的原理是什么？
    https：是以安全为目标的 http通道，在http的基础上通过传输加密和身份认证保证了传输过程的安全性。
    https由两部分组成：http+ SSL／TLS，也就是在http上又加了一层处理加密信息的模块。服务端和客户端的信息传输都会通过TLS加密，所以传输的数据都是加密后的数据

4、解释下什么是同步？什么是异步？什么是多线程？什么是IO多路复用？
    同步：发送一个请求,等待返回,然后再发送下一个请求。
    异步：发送一个请求,不等待返回,随时可以再发送下一个请求 。
    多线程：多个任务同时进行。
    IO多路复用：单个线程，通过记录跟踪每个I/O流(sock)的状态，来同时管理多个I/O流 。

5、Redis常见的性能问题和解决方案有哪些？
    1.Master写内存快照，save命令调度rdbSave函数，会阻塞主线程的工作，当快照比较大时对性能影响是非常大的，会间断性暂停服务，所以Master最好不要写内存快照。
    2.Master AOF持久化，如果不重写AOF文件，这个持久化方式对性能的影响是最小的，但是AOF文件会不断增大，AOF文件过大会影响Master重启的恢复速度。
    3.Master调用BGREWRITEAOF重写AOF文件，AOF在重写的时候会占大量的CPU和内存资源，导致服务load过高，出现短暂服务暂停现象。
    4.Redis主从复制的性能问题,根本问题的原因都离不开系统io瓶颈问题，也就是硬盘读写速度不够快，主进程 fsync()/write() 操作被阻塞。
    5.单点故障问题

6、Redis中各种常用数据类型的应用场景有哪些？
    1，string：String是最常用的一种数据类型，普通的key/ value 存储都可以归为此类，即可以完全实现目前 Memcached 的功能，并且效率更高
    2，hash：Redis hash 是一个 string 类型的 field（字段）和value（值）的映射表，hash 特别适合用于存储对象。
    3，list：Redis列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）
    4，set：Redis 的 Set 是 String 类型的无序集合。集合成员是唯一的，这就意味着集合中不能出现重复的数据。Redis 中集合是通过哈希表实现的。
    5，sorted_set：Redis 有序集合和集合一样也是 string 类型元素的集合,且不允许重复的成员。不同的是每个元素都会关联一个 double 类型的分数。
redis 正是通过分数来为集合中的成员进行从小到大的排序。有序集合的成员是唯一的,但分数(score)却可以重复。集合是通过哈希表实现的。

7、详解解释synchronized的作用及使用方法
    synchronized关键字表示线程同步，及在目前线程加锁，此操作未执行完毕，不能继续执行下个操作，需要等待锁的释放；直到把锁释放，再能继续进行下一步操作。
    1，同步块
    2，同步方法

8、什么是进程？什么是线程？它们的区别是什么？
    进程是基于操作系统的应用程序，每个进程都独立运行，每个进程都有拥有独立的内存空间。
    线程是进程内部的顺序控制流，是可以独立运行的程序片段。
    区别：一个进程可以同时拥有多个线程。多个线程共享一个进程的内存空间。

9、谈谈并发和并行的区别。
    并行是指两个或者多个事件在同一时刻发生；而并发是指两个或多个事件在同一时间间隔内发生。

10、什么是线程死锁？如何避免死锁？
    死锁是指在多线程情况下，多个线程同步竞争相互依赖的资源，从而造成多线程无法继续执行的情况。
    1，使用 事务时，尽量缩短事务的逻辑处理过程，及早提交或回滚事务；
    2，设置死锁超时参数为合理范围，超过时间，自动放弃本次操作，避免进程悬挂；
    3，优化程序，检查并避免死锁现象出现；
    4，对所有的脚本和SP都要仔细测试，在正式版本之前。
    5，所有的SP都要有错误处理（通过@error）
    6，一般不要修改SQL SERVER 事务的默认级别。不推荐强行加锁