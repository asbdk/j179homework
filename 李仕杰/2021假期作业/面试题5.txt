程序题
1、假定C盘下没有1.txt文件。在执行下面代码后会发生什么情况？
	   Writer w = null;
		BufferedWriter bw = null;
		try {
			w = new FileWriter("c:/1.txt");
			bw = new BufferedWriter(w);
			
			bw.write(“abcde”);
			bw.newLine();
			bw.write(“123”);
		} catch (IOException e) { 
			e.printStackTrace();
		}
结果是

2、
public static void main(String[] args) {
		String s = "abcd";
		StringBuffer sb = new StringBuffer("abcd");
		test(s,sb);
		System.out.println(s+"   "+sb);
	}
	
	public static void test(String s,StringBuffer sb){
		s = s + "&&&";
		sb.append("***");
	}
}
结果是
abcd   abcd***

3、		try{
			JFrame j = null;
			j.setSize(400,300);
		}
		catch(NullPointerException e){
			System.out.println("空指针异常");
		}
		catch(Exception e){
			System.out.println("发生异常");
		}
		finally{
			System.out.println("finally代码");
		}
		
		System.out.println("异常外代码");
结果是
空指针异常
finally代码
异常外代码

4、interface  A{ 
  int x = 0;
}
class B{ 
  int x =1;
}
class C extends B implements A { 
  public void pX(){ 
     System.out.println(x);

  }
  public static void main(String[] args) { 
     new C().pX();
  }
}
结果是
x

5、class A{ 
   public void speak(int a){ 
    System.out.println("ok"); 
    } 
    } 
     class B extends A{ 
  public void speak(int a, int c){ 
       System.out.println("yes");  
      } 
      public static void main(String[] args){ 
        B b=new B(); 
        b.speak(0); 
      } 
    }   
结果是
ok

6、interface Playable { 
   void play();
}
interface Bounceable { 
   void play();
}
interface Rollable extends Playable, Bounceable { 
   Ball ball = new Ball("PingPang");
}
class Ball implements Rollable { 
   private String name;
   public String getName() { 
       return name;
    }
   public Ball(String name) { 
       this.name = name;        
    }
  public void play() {
       ball = new Ball("Football");
       System.out.println(ball.getName());
    }

   public static void main(String[] args){
        Ball a=new Ball("abc");
        a.play();
   }
}
结果是
Football		

选择题：
1.下面的main方法执行到A时，str的值是？（A　　）
void main () {
   string str=＂BEA＂;
   this.modify (str);
   //A
}
void modify (Sting str) {
   str.replace (＇A ＇, ＇E ＇);
   str.toLowerCase ();
   str+=＂B＂;
 }
A.＂BEA＂
B.＂BEE＂
C.＂bee＂
D.＂beeB＂

2.下面有关表和视图叙述中错误的是（B　　）
A.视图的数据可以来自多个表
B.对视图的数据修改最终传递到基表
C.基表不存在，不能创建视图
D.删除视图不会影响基表的数据


3.下面对数组的操作代码哪个是正确的？（ABCDE　）（选择所有正确答案）
    char [ ] a=＂hello world＂.toCharArray ();
A.Class cc=a.getClass();
B.int len=a.length;
C.char c=a[999];
D.char c=a[0];
E.a.equals (new Object());


4、MyObject哪个方法会影响到这段代码的正确性以及效率？（B　　）（选择所有正确答案）
   Set set=new java.util.HashSet ();
     set.add (new MyObject (＂A＂));
     if (set.contains (new MyObject (＂A＂))){ ．．．}
A.toString
B.hashCode
C.equals
D.clone

5、下面代码中对result描述正确的是？（BC　　）（选择所有正确答案）
InputStream in=．．．;
  byte[ ] buff=new byte[1024];
  int result=in.read (buff,0,256);
A.result可能等于-1
B.result可能等于０
C.result可能等于256
D.result可能等于1024

6、下面哪个SQL属于DML？（AD　）（选择所有正确答案）
A.INSERT INTO table_name (column1,column2)VALUES (value1,value2);
B.drop table;
C.commit;
D.Select column from table;

7、SpringFramework提供了哪种IoC实现？（A　　）（选择所有正确答案）
A.基于接口的IoC实现（Interface Injection）
B.基于Setter的IoC实现（setter-based Injection）
C.基于构造函数的IoC实现（Constructor-based Injection）
D.基于静态方法的IoC实现（Static-Method Injection）

8、下列数据结构中，按先进后出原则组织数据的是（B）
A、线性链表
B、栈
C、循环链表
D、顺序表

9、根据以下代码，哪些类可以访问并改变变量name的值？（A）
package test;
class Target{
	public String name = "hello";
}
A、任何类
B、仅限Target类
C、任何test包中的类
D、任何继承Target的类

10、关于异常，下面哪个代码是合法的？（ACD　）（选择所有正确答案）
A.try{}finally{}
B.try{}catch (Exception e){}catch (ArithmeticException a){}
C.try{}catch (Throwable th){}finally{}
D.try{}catch (Throwable th){}
E.try{}catch (NullpointerException ex){}catch (Exception ex){}




问答题
1、Java中字符流和字节流的区别及使用场景，各自对应的Java类是什么？

2、以下程序创建了几个对象？分别是什么？
String A,B,C;
A="a";
B="b";
A=A+B;
StringBuffer D = new StringBuffer("abc");
D = D.append("567");

3、数据库中left join,right join,join有什么区别？

4、线程有几种状态？并尽可能描述引起状态变化的原因。

5、删除数据库表中的重复记录。

6、简述socket通信原理

7、描述一下public,protected,private,final关键字在Java中的用法?

8、Java中创建一个对象有哪几种方法？

9、描述一下Java中的异常机制,什么是Checked Exception, Unchecked Exception?

10、描述三层架构

11、描述TCP和UDP的区别

12、什么是同步，什么是异步，什么是阻塞，什么是非阻塞。

13、写clone()时，通常都有一行代码，是什么？

14、springMVC中如何实现文件上传

15、springMVC的拦截器如何实现？

16、spring组件从spring容器中取出时默认为单例模式，如何实现多实例？

17、AOP常见的通知类型有哪些？

18、谈谈mybatis的延迟加载。


编程题

1、写两个线程，一个线程打印1到52，另外一个线程打印字母A到Z，打印顺序为12A34B56C....5152Z。要求用线程间的通信。

2、请用SQL语句查询学生成绩表（name，gender，score），获得男生和女生中成绩最高的前3名。

3、写一个工具函数，去除字符串中重复的字母，并保持其他顺序不变。

4、某超市为服装类和生鲜类产品搞活动，服装类产品一律9折，生鲜类一律8折，其余商品原价。要求书写一个方法，输入商品类别和原价，返回该商品的销售价。