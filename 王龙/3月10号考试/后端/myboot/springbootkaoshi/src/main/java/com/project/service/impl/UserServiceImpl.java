package com.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.UserBean;
import com.project.dao.IUserDAO;
import com.project.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
//@Transactional
public class UserServiceImpl extends ServiceImpl<IUserDAO, UserBean> implements IUserService {
    @Resource
    private IUserDAO iUserDao;
    @Override
    public UserBean login(String name, String pwd) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_name",name);
        wrapper.eq("user_pwd",pwd);
        return iUserDao.selectOne(wrapper);
    }

    @Override
    public UserBean findByName(String name) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_name",name);
        return iUserDao.selectOne(wrapper);
    }

    @Override
    public void addZan(Long userId, Long commendId) {
        iUserDao.addZan(userId, commendId);
    }

    @Override
    public void removeZan(Long userId, Long commendId) {
        iUserDao.removeZan(userId, commendId);
    }
}
