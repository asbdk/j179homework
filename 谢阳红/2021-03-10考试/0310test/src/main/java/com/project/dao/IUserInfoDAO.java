package com.project.dao;

import com.project.bean.UserInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    UserInfoBean findByName(String userName);
}
