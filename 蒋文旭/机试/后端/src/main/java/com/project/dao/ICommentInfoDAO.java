package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.CommentInfoBean;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
public interface ICommentInfoDAO extends BaseMapper<CommentInfoBean> {
    List<CommentInfoBean> selectAll();

    /**
     * 修改评论点赞数量
     * @param commentId 评论ID
     * @param count 新数量
     */
    void updateCommentCount(Long commentId,Integer count);

    /**
     * 检测当前用户是否点赞该评论。
     * @param userId 当前用户
     * @param commentId 当前评论ID
     * @return 返回值为1：已点赞。返回值0：未点赞
     */
    int checkedLike(Long userId,Long commentId);
}
