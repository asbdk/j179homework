package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("dianzan_info")
public class DianzanBean {
    private static final long serialVersionUID = 1L;
    @TableId(value = "dianzan_id",type = IdType.NONE)
    private Long dianzanId;
    @TableField("dianzan_name")
    private String dianzanName;
    @TableField("dianzan_content")
    private String dianzanContent;
    @TableField("dianzan_score")
    private int dianzanScore;

}
