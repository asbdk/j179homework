java基础
1、请用文字描述super()和this()的区别
this()：当前类的无参构造方法，也可以指定有参的如：this(a)
super()：直接父类的无参构造方法，也可以指定有参的如：super(a)

2、什么是java序列化，如何实现java序列化。
在传输对象时，由于对象数据庞大，无法直接传输，所以，在传输之前，先将对象打散成字节序列，以利于传输，这个过程，称为序列化。
所有需要实现对象序列化的对象，必须首先实现Java.io.Serializable接口


3、Overload(重载)和Override（重写）的区别，Overloaded的方法是否可以改变返回值的类型？
如果在子类中定义某方法与其父类有相同的名称和参数，那么我们说该方法被重写了。子类的对象使用这个方法时，将调用子类中的定义。对子类而言，父类中的定义如同被“屏蔽”了一样
关于重载，如果在一个类中定义了多个同名的方法，它们或有不同的参数个数或有不同的参数类型，也就是参数签名不同，这种情况出现方法的重载。重载的方法是可以改变返回值的类型。

4、请指出下列代码中的问题
package com.aplus.interview;
public abstract class InterViewClass1{
	public final abstract int thMethod1(int para1);
}
final的类不能被重写和继承,而abstract的类是抽象类,本身没有实现,只能通过子类来实现,也就是说必须被继承。所以说它们是没法同时做修饰符的


5、有字符串为1234567890，请用最简短的代码将此字符串倒序输出到控制台。（ok）

6、请问这个程序会有下面哪种结果？
public class Cygnus{
	static int value = 9;
	public static void main(String[] args) throws Exception{
		new Cygnus().printValue();
	}
	private void printValue(){
		int value = 69;
		System.out.println(this.value);
	}
}
B
A、编译错误    B、打印9    C、打印69  D、运行时抛出异常

7、请问NullPointerException，RuntimeException各是什么异常，有什么异同？
空指针异常，运行时异常，RuntimeException是NullPointerException的父类
Exception：在程序中必须使用try...catch进行处理。
RuntimeException：可以不使用try...catch进行处理，但是如果有异常产生，则异常将由JVM进行处理。



8、请问&和&&的区别
&&只要第一个条件是false，就不会去执行第2个条件 
但&不管第一个条件是不是false，都会执行第2个条件

9、在old.txt中，乱序排列着abcdefg等字符，请用代码描述使用文件读写流，把old.txt中的内容写入到new.txt中。（ok）
old.txt        
bcdafge

new.txt
abcdefg

10、请用简短语言描述用jdbc连接mysql数据库，查询某表A的所有数据，并打印第一列的数据到控制台
1、建立连接
2、执行SQL语句,查询第一列的数据
3、关闭连接

11、war包是什么，ear包是什么，他们之间是什么关系，请大体指出war包的目录结构
JAVA WEB工程，都是打成WAR包，进行发布，如果我们的服务器选择TOMCAT等轻量级服务器，一般就打出WAR包进行发布；
EAR包：这针对企业级项目的，实际上EAR包中包含WAR包和几个企业级项目的配置文件而已，一般服务器选择WebSphere等，都会使用EAR包

12、ArrayList和LinkedList的区别。
ArrayList:底层采用数组实现，遍历速度快，中间插入删除元素速度慢
LinkedList：采用双向链表实现。每个元素不仅仅存储数据，还要存储上一个和下一个元素的引用，空间不连续，
中间插入和删除元素速度快，遍历速度慢

java web
1、实现一个servlet从编码到部署，需要做哪些工作，请简要描述。
1、设置编码集
2、注册servlet





2、sendRedirect()和forward()有什么不同
forward是服务器内部的跳转，浏览器的地址栏不会发生变化，同时可以把request和response传递给后一个请求。
sendRedirect()是浏览器方面的跳转，要发送两次请求，地址栏也会发生变化，同时request和response也会发生变化，重新生成新的对象


3、简述spring的加载机制
1、容器的初始化
2、web.xml配置
   
	
4、简要描述spring中声明事务配置过程。
在springMVC配置类中声明事务的可行性
然后再在实现类中声明支持事务
	

5、使用javascript向网页输出<h1>hello</h1>，以下代码可行的是（B）
A、<script>
	document.write(<h1>hello</h1>);
</script>
B、<script>
	document.write("<h1>hello</h1>");
</script>
C、<script>
<h1>hello</h1>
</script>
D、<script>
	document.write("hello");
</script>

6、描述servlet生命周期。
1、容器加载并实例化Servlet
2、调用init方法完成初始化
3、当请求到达，调用service处理请求，产生响应
4、销毁阶段，调用destroy()，完成资源清理

7、servlet、filter、listener加载顺序
listener->Filter->servlet.


8、为一个图片设置背景图像后，设置背景图像在纵向和横向上平铺，下面哪个是实现该功能的？B
A、no-repeat   B、repeat    C、repeat-x    D、repeat-y


数据库

1、表名t_User（ok）
userName   tel   	content   birthday
张三  13333663366   大专毕业  2006-10-11
张三  13612312331   本科毕业  2006-10-15
张四  021-55665566  中专毕业  2006-10-15
a)有一新记录（小王 13254748547  高中毕业  2007-05-06）请用SQL语句新增至表中


b)请用sql语句把张三的手机号更新13709092345

c)请写出删除名为张四的全部记录的SQL



2、请写出下列要求的SQL（ok）
表名Sates
id		district		bookName		amount
1		西南			词典1			0
2		西北			书籍1			12
3		西南			杂志2			23
请书写降序排列各地区销售情况，结果要求展现district字段



3、什么是主键，什么是外键，什么是索引，各有什么作用？
主键是每条记录的唯一标识，用于区分不同的行和不同的实体。特点：不能为空，不能重复。
外键是描述两个表之间关联关系的列。
索引分单列索引和组合索引。单列索引，即一个索引只包含单个列，一个表可以有多个单列索引，但这不是组合索引。组合索引，即一个索引包含多个列。


4、递归考核：
数据库中，有一个员工关联关系表：
id		pid		name
1		0		刘备
2		0		曹操
3		1		关羽
4		2		张辽
5		3		关平
……
分别表示：员工编号、上级领导编号、姓名。
把它从数据库查询出来，递归成如下树结构：（使用java查询数据库递归实现）
const list = [{
	id:1,
	name:'刘备',
	children:[
		{
			id:3,
			name:'关羽'
			childern:[……]
		},
		……
	]
},……]