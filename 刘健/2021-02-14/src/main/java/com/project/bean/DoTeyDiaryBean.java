package com.project.bean;

import java.time.LocalDate;

/**宠物日记实体类*/
public class DoTeyDiaryBean {
    /**宠物日记Id*/
    private int DiaryId;
    /**日记发表时间*/
    private LocalDate date;
    /**日记标题*/
    private String title;
    /**日记内容*/
    private String content;
    /**日志图片*/
    private String pic;
    /**宠物外键*/
    private int doTeyId;

    public DoTeyDiaryBean() {
    }

    public DoTeyDiaryBean(LocalDate date, String title, String content, String pic) {
        this.date = date;
        this.title = title;
        this.content = content;
        this.pic = pic;
    }

    public int getDiaryId() {
        return DiaryId;
    }

    public void setDiaryId(int diaryId) {
        DiaryId = diaryId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getDoTeyId() {
        return doTeyId;
    }

    public void setDoTeyId(int doTeyId) {
        this.doTeyId = doTeyId;
    }

    @Override
    public String toString() {
        return "DoTeyDiaryBean{" +
                "DiaryId=" + DiaryId +
                ", date=" + date +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", pic='" + pic + '\'' +
                '}';
    }
}
