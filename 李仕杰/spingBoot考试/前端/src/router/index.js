import Vue from 'vue'
import VueRouter from 'vue-router'
// import Register from '../pages/Register'
// import Manage from '../pages/Manage'
// import User from '../pages/User'
// import login from '../pages/login'
import Register from '../ui/Register'
import Manage from '../ui/Manage'
import User from '../ui/User'
import login from '../ui/Login'
import comment from '../ui/comment.vue'

Vue.use(VueRouter)

const routes = [
    {path:'/register',component:Register},
    {path:'/login',component:login},
    {path:'/comment',component:comment},
    {path:'/manage',component:Manage,
    children:[
        {path:'/manage/user',component:User},
        // {path:'/manage/comment',component:User}
    ]    
}
]

export default new VueRouter({
    mode:'history',
    routes
})