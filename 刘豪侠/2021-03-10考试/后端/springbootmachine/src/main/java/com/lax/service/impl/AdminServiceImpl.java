package com.lax.service.impl;

import com.lax.bean.AdminBean;
import com.lax.dao.IAdminDAO;
import com.lax.service.IAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class AdminServiceImpl extends ServiceImpl<IAdminDAO, AdminBean> implements IAdminService {

    @Override
    public AdminBean findByName(String name) {
        return this.baseMapper.findByName(name);
    }

    @Override
    public List<Integer> findCommentZan(int id) {
        List<Integer> list = new ArrayList<>();
        List<AdminBean> listId = this.baseMapper.findCommentZan(id);
//        System.out.println(listId);
        for (AdminBean adminBean : listId) {
            list.add(adminBean.getCommentZanList().get(0));
        }
        return list;
    }
}
