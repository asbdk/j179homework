package com.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.UserBean;

public interface IUserService extends IService<UserBean> {

        public Object login(String name,String pwd);

        UserBean selectByUserName(String name);
}
