package com.project.mapper;

import com.project.bean.DoTeyBean;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IDoTeyMapper {
    /**
     * 按用户id添加宠物
     * @param userId 用户id
     * @param doTey 宠物对象
     */
    public void addDoTey(@Param("userId") int userId,@Param("doTey") DoTeyBean doTey);

    /**
     * 根据宠物id，修改宠物属性
     * 动态修改宠物属性 点击“喂食”--------->力量+5,爱心+1 info='喂食'
     * 点击“讲故事”--------->智力+5,爱心+1 info='讲故事'
     * 点击“玩游戏”--------->力量+1,智力+2,爱心+3  info='玩游戏'
     * @param id 宠物id
     * @param info 如果info
     *
     */
    public void update(@Param("id")int id,@Param("info") String info);
    /**
     * 按用户id查询该用户所拥有的宠物
     * 按昵称、类别 动态条件分页查询，可以选择排序方式
     * @param userId 用户id
     * @param type 类别
     * @param doTeyName 宠物昵称
     * @param rank 排序方式
     * @return 宠物分页对象
     */
    public List<DoTeyBean> findByItem(@Param("userId")int userId,
                                      @Param("type")String type,
                                      @Param("doTeyName")String doTeyName,
                                      @Param("rank")String rank);

    /**
     * 按id删除宠物,同时删除该宠物的日志
     */
    public void del(@Param("doTeyId") int doTeyId);

}
