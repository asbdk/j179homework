package com.lovo.service.impl;

import com.lovo.bean.CommentBean;
import com.lovo.dao.ICommentDAO;
import com.lovo.service.ICommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zack
 * @since 2021-03-10
 */
@Service
@Transactional
public class CommentServiceImpl extends ServiceImpl<ICommentDAO, CommentBean> implements ICommentService {
    @Resource
    private ICommentDAO commentDAO;

    @Override
    public List<CommentBean> findAll() {
        return commentDAO.findAll();
    }

    @Override
    public int findByCount(Integer userId, Integer commentId) {
        return commentDAO.findByCount(userId, commentId);
    }

    @Override
    public void addCount(Integer userId, Integer commentId) {
        commentDAO.addCount(userId, commentId);
    }

    @Override
    public void deleteCount(Integer userId, Integer commentId) {
        commentDAO.deleteCount(userId, commentId);
    }


}
