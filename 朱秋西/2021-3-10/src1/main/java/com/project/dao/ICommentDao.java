package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.CommentBean;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Past;
@Service
public interface ICommentDao extends BaseMapper<CommentBean> {
    void update(@Param("commentid") Long commentid, @Param("shu") int shu);
    void addUserComment(@Param("userId") int userId,@Param("commentId") int commentId);
    void delUserComment(@Param("userId") int userId,@Param("commentId") int commentId);
}
