package com.project.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("user_comment")
public class UserCommentBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "user_comment_id", type = IdType.AUTO)
    private Long userCommentId;
    @TableField("user_id")
    private Long userId;
    @TableField("comment_id")
    private Long commentId;


}
