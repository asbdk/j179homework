import ajax from "../service/ajax.js"
export default{
    namespaced:true,//允许使用命名空间
    state:{
        replyList:[],
        sessionUser:{}
     
    },
    mutations:{
        setReplyList(state,replyList){
            state.replyList = replyList;
        },
        setSessionUser(state,sessionUser){
            state.sessionUser = sessionUser;
        }
    },
        
    actions:{
        async getUser(context){
            var info = await ajax.getSubmit("/api/users/getUser")
            context.commit("setSessionUser",info);
        // alert(JSON.stringify(this.sessionUser))
        },
        async showAll(context){
            var info = await ajax.getSubmit("/api/replys/showAll")
            context.commit("setReplyList",info);
        },
        
    }
}