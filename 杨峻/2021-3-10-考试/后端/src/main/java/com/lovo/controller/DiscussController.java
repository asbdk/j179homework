package com.lovo.controller;



import com.lovo.bean.UserInfoBean;
import com.lovo.result.ResponseResult;
import com.lovo.service.IDiscussService;
import io.swagger.annotations.Api;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("discuss")
@ResponseResult
@Api("评论")
public class DiscussController {
    @Resource
    private IDiscussService discussService;

    @GetMapping("findDiscuss")
    public Object findDiscuss(){
        return discussService.findDiscuss();
    }

    @PostMapping("addFavor")
    public Object addFavor(Integer discussId){
        UserInfoBean user = (UserInfoBean) SecurityUtils.getSubject().getPrincipal();
        discussService.addFavor(user.getUserInfoId(), discussId);
        return "ok";
    }

    @PostMapping("deleteFavor")
    public Object deleteFavor(Integer discussId){
        UserInfoBean user = (UserInfoBean) SecurityUtils.getSubject().getPrincipal();
        discussService.deleteFavor(user.getUserInfoId(), discussId);
        return "ok";
    }

}

