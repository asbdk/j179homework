package com.lax;

import com.lax.service.IAdminService;
import com.lax.service.ICommentService;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Test {

    @Resource
    private IAdminService service;

    @Resource
    private ICommentService commentService;

    @org.junit.Test
    void test(){

        System.out.println(commentService.findAll());
    }
}
