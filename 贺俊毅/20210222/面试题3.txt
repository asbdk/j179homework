选择题：
1、
考虑下面的类：
1、 class Test{
2、		void test(int i){
3、			System.out.println("I am an int.");
4、		}
5、		void test(String s){
6、			System.out.println("I am an String.");
7、		}
8、
9、		public static void main(String[] args){
10、		Test t = new Test();
11、		char ch = 'y';
12、		t.test(ch);
		}
	}
下面描述哪一个是正确的：
A、5行将不能编译，因为返回值是void的方法不能重载。
B、12行将不能编译，因为没有一个test()版本能够适合char参数。
C、代码能够编译，但在12行会抛出一个异常。
D、代码能够编译，同时输出I am an int。
E、代码能够编译，同时输出I am an String。
B

2、
class Super{   
       protected float getNum(){return 3.0f;} 
   } 

  public class Sub extends Super{ 
       //Here
  }
下面哪些代码可放在Here处
	
a)	float getNum(){return 4.0f;} 
b)	public void getNum(){}  
c)	private void getNum(double d){} 
d)	public double Getnum(){return 4.0d;} 
e)	public float getNum(){return 9;}  
bcd

3、
public class Foo{
		public static void main(String[] args){
			try{return;}
			finally{System.out.println("finally");}
		}
}
结果是：
A、程序运行并且不打印任何东西
B、程序运行并且打印"finally"
C、代码编译通过，但运行时有一个异常抛出
D、代码不能编译，因为异常没有捕捉，缺少catch
B

4、
public class Test{
	public int aMethod(){
		static int i=0;
		i++;
		return i;
	}
	public static void main(String[] args){
		Test test = new Test();
		test.aMethod();
		int j = test.aMethod();
		System.out.println(j);
	}
}
A、编译错误
B、编译成功并且输出"0"
C、编译成功并且输出"1"
D、编译成功并且输出"2"
A

程序题
1、public class MM {
	public static void test(Object[] x){
		System.out.println("Object[]");
	}
	public static void test(Object x){
		System.out.println("Object");
	}
	public static void main(String[] args) {
		test(null);
	}
}
运行结果是：Object[]
理由是：就近原则

2、public class X{
	public int x;
	public int y;
	public X(int x){
	    this.y = x;
	}
}

class Child extends X{
	public Child(int x){
	    super(x);
	}
}

class Test{
	public static void main(String[] aa){
	   Child c = new Child(10);
	   System.out.println(c.x);
	}
}
运行结果是：0


3、
public class FinallyTest {
	private static String output = "";
	
	public static void foo(int x){
		try{
			if(x == 0){
				throw new Exception();
			}
				output += "1";
			
		}catch(Exception e){
			output += "2";
			return;
		}
		finally{
			output += "3";
		}
		output += "4";
	}
	public static void main(String[] args) {
		foo(0);
		foo(1);
		System.out.println(output);
	}
}
运行结果是：
23134

4、
class B{
	private int radius = 10;
	public void draw(){
		System.out.println("B.draw(),radius="+radius);
	}
	public B(){
		System.out.println("B constructor");
		draw();
	}
}
class A extends B{
	private int radius = 1;
	public void draw(){
		System.out.println("A.draw(),radius="+radius);
	}
	public A(int radius){
		this.radius = radius;
		System.out.println("A constructor");
	}
	public static void main(String[] args) {
		A a = new A(5);
	}
}
运行结果是：A constructor


5、
class  A{   
	static{
	System.out.println("1");
	}
	public  A(){
	System.out.println("2"); 
	}
}
class B extends A{
	static{
	System.out.println("a");
	}
	public B(){
	System.out.println("b"); 
	}
}
class Test{
	public static void main(String [] args){
	A ab = new B();
	ab = new B();
	}
}
运行结果是：编译不通过


6、<script>
	function Test(){
		var k = 0;
		for(j=0,i=0;j<6,i<10;j++,i++){
			k = i+j;
		}
		alert(k);
	}

</script>
运行结果是：
2
4
6
8
10


7、<script>
	var x=1,y=z=0;
	function add(n){
		return n=n+1;
	}
	
	y=add(x);
	
	function add(n){
		return n=n+3;
	}
	z=add(x);
	
	alert(x+"  "+y+"  "+z);

</script>
运行结果是：1  4  4


数据库：
1、
表1  student学生信息表
	id		int	 		学生编号
	name	varchar	 	学生姓名
	sex		bit		 	性别（男0   女1）
	Class	int		 	班级编号
	
表2   schedule课程信息表
	id		int			课程编号
	Name	varchar		课程名称
表3	  Grade 成绩信息表
	id		int			自动编号
	UID		int			学生编号
	SID		int			课程编号
	Num		int			考试成绩
(a)求各班的总人数
select Class,sum(id)  from student group by Class

(b)求1班女生和男生的平均成绩
select s.Class,s.sex,avg(g.Num) from student s left join Grade g on s.id=g.UID left join schedule sc on sc.id=g.SID where s.Class = 1 and s.sex = 1 gourp by s.Class;
select s.Class,s.sex,avg(g.Num) from student s left join Grade g on s.id=g.UID left join schedule sc on sc.id=g.SID where s.Class = 1 and s.sex = 0 gourp by s.Class;

(c)各班"数据结构"(课程名称)不及格的人数
select s.Class,sum(s.id) from student s left join Grade g on s.id=g.UID left join schedule sc on sc.id=g.SID where sc.Name='数据结构' and g.Num<60 

2、t_table(t_name,t_sex,t_cat,t_score)-- 名字、性别、科目、成绩
   a、修改表中t_sex值，男变女，女变男
update t_table set t_sex  = 1 where t_sex = 0;
update t_table set t_sex  = 0 where t_sex = 1;

   b、求每科成绩最高分同学的姓名
select t_name from t_table order by t_score desc limit 0,1;

   c、查询有1门以上科目成绩的同学，并显示有几门科目
select t_name,count(t_cat)c from t_table where c >=1 

3、二张表document和udt_flow
document
id			int
title		varchar
state		int
createdate	date

udt_flow
doc_id		int
udt_id		int

udt_flow通过doc_id与document表id关联，document表的state值：=0  带流转   
=1 流转中   =2 流程结束   =3 终止流程

查询udt_flow表中udt_id为49的记录数据，且结果集按时间倒序排列
请写出SQL语句，得到如下结果集：
title			state
AAA				带流转
BBB				流程结束
select d.title,d.state from document d left join udt_flow u on d.id = u.doc_id where u.udt_id = 49 order by d.createdate desc


问答题：
1、请列出你所知道的java设计模式，并对每种模式进行简短的描述说明。
工厂模式
	将对象的创建和对象的使用分开进行，其主要目的是解耦

代理模式
	在执行一个方法时，在方法执行前和方法执行后添加非功能性业务。

单例模式
	无论一个类的方法使用多少次，该类的对象只产生唯一的一个。单例模式可以大大减少内存消耗。

装饰器模式
	对象功能的扩展能够根据需要来动态的实现

适配器模式
	把一个类的接口变换成客户端所期待的另一种接口，从而使原本因接口原因不匹配而无法一起工作的两个类能够一起工作

原型模式
	通过复制和克隆的方式创建对象，这样无需知道创建对象的细节。

观察者模式
	让多个观察者同时监听某一个主题对象，这个主题对在状态发生变化的时候，会通知所有的观察者对象，使他们能够自动更新自己。


2、UML中视图的作用是什么？UML中有几种视图，对每种视图做简短说明。
统一建模语言UML是专门用来进行软件系统设计和架构建模的一门可视化建模语言，通过各种图示展示了软件系统的方方面面。
UML描述类
	访问修饰符
		+：public共有的
		-：private私有的
		#：protected受保护的
		~：默认的
	抽象类和抽象方法以斜体表示
UML描述接口
UML描述包
类与类的关系
	实现
		一个类实现一个接口
	泛化
		一个类继承另一个类
	关联
		依赖
			一个类是另一个类的方法参数，一个类是另一个类的返回类型，一个类是另一个类的方法的局部变量
				
		聚合
			一个类是另一个类的属性，是整体和部分的关系
				
		组合
			一个类是另一个类的属性，是整体中不可分割的一部分，是强聚合

3、写出字符串常用的方法。
toCharArray(); 
charAt(i);
substring();
split();

4、javascript有哪几种数据类型？
值类型(基本类型)：字符串（String）、数字(Number)、布尔(Boolean)、空（Null）、未定义（Undefined）5
引用数据类型：对象(Object)、数组(Array)、函数(Function)。3

5、写出你所知道的开源项目或软件，并说明他们的好处以及作用。
java 开源项目
Spring Cloud 分布式应用服务开发的一站式解决方案，迅速搭建分布式应用系统。

Druid：是一个 JDBC 组件。
1.监控数据库访问性能。
2.提供了一个高效、功能强大、可扩展性好的数据库连接池。
3.数据库密码加密。
4.SQL执行日志。

VirtualLayout是一个针对RecyclerView的LayoutManager扩展, 主要提供一整套布局方案和布局间的组件复用的问题。

6、写出你在编码过程中，遇到的5个常见的java异常。
1、java.lang.NullpointerException(空指针异常)
2、 java.lang.ClassNotFoundException（指定的类不存在）
3、java.lang.IndexOutOfBoundsException（数组下标越界异常）
4、java.lang.ArithmeticException（数学运算异常）
5、java.lang.ClassCastException（数据类型转换异常）
6、 java.lang.FileNotFoundException（文件未找到异常）
7、java.lang.NoSuchMethodException（方法不存在异常）

7、java是传值还是传引用？
传引用

8、说出ArrayList、Vector、LinkedList的存储性能和特性
三者均为可伸缩数组，动态可变长度数组

ArrayList 和Vector 
     1. 都是使用数组方式存储数据，此数组元素数大于实际存储的数据以便增加和插入元素，它们都允许直接按序号索引元素，但是插入元素要涉及数组元素移动等内存操作，所以索引数据快而插入数据慢. Vector中的方法由于添加了synchronized修饰，因此Vector是线程安全的容器，但性能上较ArrayList差，因此已经是较少使用。
     2. 都有一个初始化的容量大小，当里面存储的元素超过次大小时需要扩容。为了提高效率，Vector默认扩充为2倍（可自定），而ArrayList为1.5倍（不可自定义，未提供方法）

ArrayList实现了List接口，以数组的方式来实现的，因此对于快速的随机取得对象的需求，使用ArrayList实现执行效率上会比较好。
LinkedList是采用链表的方式来实现List接口的，因此在进行insert和remove动作时效率要比ArrayList高。适合用来实现Stack(堆栈)与Queue(队列)。

9、什么是AJAX
Ajax全称是"Asynchronous Javascript And XML"（异步JavaScript和XML），是指一种创建交互式，快速动态网页应用的网页开发技术，无需重新加载整个网页的情况下，能够更新部分网页的技术。
Ajax利用javascript代替传统的表单提交方式，和服务器进行少量数据交换。可以将响应信息利用javascript进行网页数据的部分更新。

10、为什么需要验证码？描述验证的实现原理。
防止批量注册，防止机器人注册使服务器崩溃，
生成随机数或英文字母与输入字符串进行匹配

11、什么是数据库连接池，，描述原理
为数据库连接建立一个“缓冲池”。预先在缓冲池中放入一定数量的连接，当需要建立数据库连接时，只需从“缓冲池”中取出一个，使用完毕之后再放回去。通过设定连接池最大数量连接数，来防止系统无休止的数据库连接。
当一个请求需要一个连接对象时，首先查看连接池中是否有空闲连接，如果有，则分配给该请求。如果没有，则查看是否到达最大连接数。如果没有到达最大连接数，则在连接池中创建新连接，分配给该请求。如果已经到达最大连接数，则该请求需要等待一段时间，在等待时间内如果有连接被释放，则分配给等待的请求。如果超过等待时间则返回null。

12、B/S应用程序中，表单提交的数据怎样保证合法性？
使用matches() 方法 ，用于检测字符串是否匹配给定的正则表达式
也可以降低由于拒绝误操作输入提交到服务器端处理而导致的服务器端负担。

13、为什么我们需要编码规范？例举你在java编码中用过的编码规范。
需要
遵守单一原则
命名规范
代码格式规范
写文档注释及其格式规范

14、请说出你所知道的线程同步的方法
synchronized 修饰方法。
synchronized 同步语句块

需要同步的变量加上volatile

ReentrantLock() :
 创建一个ReentrantLock实例
lock() : 获得锁
unlock() : 释放锁

16、spring依赖注入方式及其优缺点。
1.构造方法注入:
优点：
在构造方法中体现出对其他类的依赖，一眼就能看出这个类需要其他那些类才能工作。
脱离了IOC框架，这个类仍然可以工作，POJO的概念。
一旦对象初始化成功了，这个对象的状态肯定是正确的。
缺点：
构造函数会有很多参数（Bad smell）。
有些类是需要默认构造函数的，比如MVC框架的Controller类，一旦使用构造函数注入，就无法使用默认构造函数。
这个类里面的有些方法并不需要用到这些依赖（Bad smell）。

2.  Set方法注入：
优点：
在对象的整个生命周期内，可以随时动态的改变依赖。
非常灵活。
缺点：
对象在创建后，被设置依赖对象之前这段时间状态是不对的。
不直观，无法清晰地表示哪些属性是必须的。

3. 方法参数注入：
方法参数注入的意思是在创建对象后，通过自动调用某个方法来注入依赖。
优点：
比较灵活。
缺点：
新加入依赖时会破坏原有的方法签名，如果这个方法已经被其他很多模块用到就很麻烦。
与构造方法注入一样，会有很多参数。


17、描述servlet生命周期
三个阶段
servlet生命周期：1、初始化阶段，Servlet容器会创建一个Servlet实例并调用【init()】方法；
2、处理客户端请求阶段，每收到一个客户端请求，服务器就会产生一个新的线程去处理；
3、终止阶段，调用destroy方法终止

18、web应用程序常见的漏洞有哪些？
SQL注入
cookies修改
命令行注入


19、说说spring的事务传播特性。
事务的传播行为是指，如果在开始当前事务之前，一个事务上下文已经存在，此时有若干选项可以指定一个事务型方法的执行行为。

20、说说常见的数据库引擎
MySQL
InnoDB
ODBC
Memory
MYISAM

编码题：
1、思考一个需要使用递归的例子，并用java代码实现

2、int a=1;int b=5;在不增加任何常量的情况下，使a和b的值对调！

3、用javascript自定义一个用户对象，拥有姓名、年龄、性别属性和跑步行为（至少使用2种方式实现）

4、解析url的信息，分段打出相关内容
输入 http://job.csdn.net/n/20061020/96512.html
打印出
protocol=http
host=job.csdn.net
path=/n/20061020/
file=96512.html

5、读取c:/1.txt文件中所有的内容，打印出来。（文件内容为标准的txt信息）
