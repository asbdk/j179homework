package com.lovo.dao;

import com.lovo.bean.UserInfosBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjy
 * @since 2021-03-02
 */
public interface IUserInfosDAO extends BaseMapper<UserInfosBean> {
    @Select("select * from user_infos where user_name=#{userName} and user_pass=#{userPass}")
    @ResultMap("userMap")
    UserInfosBean login(@Param("userName") String userName,@Param("userPass") String userPass);
}
