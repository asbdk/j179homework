import ajax from "../service/ajax.js"

export default{
    namespaced:true,

    state:{
        user:{},
        param:{},
    },

    mutations:{
        setUser(state,userInfo){
            state.user = userInfo;
        }
    },

    actions:{
        async login(context){
            var userInfo = await ajax.getSubmit("/api/user/login",context.state.param);
            context.commit("setUser",userInfo);
        },
    },

}