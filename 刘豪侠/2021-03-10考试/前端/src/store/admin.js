import ajax  from "../service/ajax.js"
export default{
    namespaced:true,//允许使用命名空间
    state:{
        loginUser:{}, //登陆用户对象       
    },
    mutations:{
        setLoginUser(state,obj){ //设置登陆用户
            state.loginUser = obj;
            // alert(JSON.stringify(state.loginUser));
        },
        setSessionObj(state,obj){ //设置session用户
            state.sessionObj = obj;
        },
    },
    actions:{
        // async getLoginUser(context){//得到登陆用户对象
        //     var userObj = await ajax.getSubmit("/project/users/getUserSession",{});
        // //    alert(JSON.stringify(userObj));
        //     context.commit('setSessionObj',userObj);

        // }
    }
}