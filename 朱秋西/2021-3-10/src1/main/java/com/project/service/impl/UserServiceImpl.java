package com.project.service.impl;


import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.UserBean;

public class UserServiceImpl extends IService<UserBean> {
    UserBean login(String userName, String userPwd);
    UserBean selectByUserName(String userName);
}
