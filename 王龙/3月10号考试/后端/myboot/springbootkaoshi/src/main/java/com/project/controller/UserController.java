package com.project.controller;

import com.project.service.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users02")
//@Api("用户业务接口")
//@ResponseResult
public class UserController {
    @Autowired
    private IUserService userService;
//    @PostMapping("/login")
//    @ApiOperation(value = "登录接口",notes = "需要输入密码、账号登录")
//    private Object login(String name,String pwd) throws GlobalHandleException {
////        AnnotationConfigApplicationContext a = new AnnotationConfigApplicationContext();
////        IUserService userService02 = a.getBean(IUserService.class);
//        UserBean userBean = userService.login(name,pwd);
//        System.out.println(userBean);
//        if(userBean == null){
//            throw new GlobalHandleException(ResultCode.USER_LOGIN_ERROR);
//        }
//        String token = JWTUtil.createToken(userBean.getName(),userBean.getPwd(), Calendar.MINUTE,30);
//        String refreshToken = JWTUtil.createToken(userBean.getName().toString(),userBean.getPwd(), Calendar.DATE,7);
//        Map map = new HashMap();
//        map.put("token",token);
//        map.put("refreshToken",refreshToken);
//        return map;
//    }

    @ApiOperation(value = "点赞",notes = "增加点赞")
    @PostMapping("/addZan")
    public Object addZan(Long userId,Long commendId){
        userService.addZan(userId,commendId);
        return "ok";
    }

    @ApiOperation(value = "取消赞",notes = "取消点赞")
    @GetMapping("/removeZan")
    public Object removeZan(Long userId,Long commendId){
        userService.removeZan(userId,commendId);
        return "ok";
    }

    @GetMapping("/removeUser")
    public Object removeSession( ){
        return null;
    }

//    @GetMapping("/refreshToken")
//    @UserValid
//    public Object refreshToken(HttpServletRequest request){
//        UserBean userInfoBean = (UserBean) request.getAttribute("user");
//        return JWTUtil.createToken(userInfoBean.getUserId().toString(),userInfoBean.getUserPass(),Calendar.SECOND,10);
//    }


}
