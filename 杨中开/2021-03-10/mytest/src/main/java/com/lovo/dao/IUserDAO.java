package com.lovo.dao;

import com.lovo.bean.UserBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zack
 * @since 2021-03-10
 */
public interface IUserDAO extends BaseMapper<UserBean> {
    UserBean selectByUserName(String userName);

}
