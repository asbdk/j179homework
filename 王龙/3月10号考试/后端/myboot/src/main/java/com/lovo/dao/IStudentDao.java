package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.StudentBean;

import java.util.List;

public interface IStudentDao extends BaseMapper<StudentBean> {
    List<StudentBean> selectAll(String name);

}
