package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.CommentsBean;
import com.lovo.bean.UserBean;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentsService extends IService<UserBean> {


    public List<CommentsBean> findAll();

}
