package com.lovo.demo02.packResult;

public enum ResoultCode {
    SUCCESS(1,"成功"),
    // 参数错误1001-1999
    PARAM_IS_INVALID(1001,"参数无效"),
    PARAM_IS_BLANK(1002,"参数为空"),
    PARAM_TYPE_BAND_ERROR(1003,"参数类型错误"),
    PARAM_NOT_COMPLETE(1004,"参数缺失"),
    //业务层异常
    SERVICE_ERROR(6001,"业务层出错啦");


    private Integer status;
    private String message;

    ResoultCode(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    ResoultCode() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
