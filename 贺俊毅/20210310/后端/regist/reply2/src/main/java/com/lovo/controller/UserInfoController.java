package com.lovo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lovo.bean.ReplyInfoBean;
import com.lovo.bean.UserInfoBean;
import com.lovo.dao.IReplyInfoDAO;
import com.lovo.result.GlobalHandleException;
import com.lovo.result.ResponseResult;
import com.lovo.result.ResultCode;
import com.lovo.result.UserValid;
import com.lovo.service.IUserInfoService;
import com.lovo.util.JWTUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/users")
@ResponseResult
public class UserInfoController {
    @Resource
    private IUserInfoService userInfoService;
    @Resource
    private IReplyInfoDAO replyInfoDAO;
    @PostMapping("/login")
    public Object login(String userName, String userPass) throws GlobalHandleException {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_name", userName);
        wrapper.eq("user_pwd", userPass);
        UserInfoBean userInfoBean = userInfoService.getOne(wrapper);
        if (userInfoBean == null) {
            throw new GlobalHandleException(ResultCode.USER_LOGIN_ERROR);
        }

//        session.setAttribute("user",userInfoBean);
        String token = JWTUtil.createToken(userInfoBean.getUserName(), userInfoBean.getUserPwd(), Calendar.MINUTE, 30);
        String refreshToken = JWTUtil.createToken(userInfoBean.getUserName().toString(), userInfoBean.getUserPwd(), Calendar.DATE, 7);
        Map map = new HashMap();
        map.put("token", token);
        map.put("refreshToken", refreshToken);
        return map;
    }

    /**
     * 从token中获取用户对象
     * @return
     */
    @GetMapping("/token")
    @UserValid
    public Object getUserWithToken(HttpServletRequest request){
        return SecurityUtils.getSubject().getPrincipal();
    }


    @GetMapping("/like")
    public String like(Long userId,Long replyId){
        userInfoService.like(userId, replyId);
        return "ok";
    }

    @GetMapping("/getUser")
    public UserInfoBean getUser(HttpSession session){
        return (UserInfoBean)session.getAttribute("login");
    }

    @GetMapping("/ifZan")
    public boolean ifZan(Long userId,Long replyId){
        ReplyInfoBean replyInfoBean =  replyInfoDAO.selectLike(replyId, userId);
        if(replyInfoBean==null){
            return false;
        }else {
            return true;
        }

    }


}

