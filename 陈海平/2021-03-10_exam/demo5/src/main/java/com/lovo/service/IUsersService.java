package com.lovo.service;

import com.lovo.bean.UsersBean;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUsersService extends IService<UsersBean> {
    UsersBean selectByUserName( String userNmae);



}
