import Vue from 'vue'
import VueRouter from 'vue-router'

import loginUser from "../pages/user/LoginUser.vue"
import listDiscuss from "../pages/discuss/ListDiscuss.vue"

Vue.use(VueRouter)

const routes = [    
    {path:'/loginUser',component:loginUser},
    {path:'/listDiscuss',component:listDiscuss},
]

export default new VueRouter({
    mode:'history',
    routes
})