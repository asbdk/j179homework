package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.UserBean;
import org.apache.ibatis.annotations.Param;

public interface IUserDAO extends BaseMapper<UserBean> {
    /**
     * 点赞
     * @param userId
     * @param commendId
     */
    void addZan(@Param("userId") Long userId,@Param("commendId") Long commendId);

    /**
     * 取消赞
     * @param userId
     * @param commendId
     */
    void removeZan(@Param("userId")Long userId,@Param("commendId")Long commendId);
}
