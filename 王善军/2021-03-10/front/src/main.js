import Vue from 'vue'
import App from './App.vue'
import router from "./router/index.js"
import ajax from "./service/ajax.js"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueCookies from 'vue-cookies';


Vue.use(VueCookies)
Vue.use(ElementUI);

Vue.config.productionTip = false
Vue.prototype.ajax=ajax;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
