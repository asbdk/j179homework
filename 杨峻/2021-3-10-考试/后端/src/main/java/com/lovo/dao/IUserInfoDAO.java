package com.lovo.dao;

import com.lovo.bean.DiscussBean;
import com.lovo.bean.UserInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    UserInfoBean selectByCode(String code);
    Integer checkFavor(Integer userId, Integer discussId);
}
