import Vue from 'vue'
import App from './App.vue'
import router from "./router/index.js"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import ajax from "./service/ajax.js"
import store from "./store/index.js"

// ——引用cookies——————————————————————————————————————————
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)




Vue.use(ElementUI);
Vue.prototype.ajax=ajax;

Vue.config.productionTip = false

new Vue({
  store,
  router:router,
  render: h => h(App),
}).$mount('#app')
