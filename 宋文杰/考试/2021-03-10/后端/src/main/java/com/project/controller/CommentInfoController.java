package com.project.controller;

import com.project.bean.CommentInfoBean;
import com.project.result.ResponseResult;
import com.project.service.ICommentInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/comments")
@ResponseResult
public class CommentInfoController {

    @Resource
    private ICommentInfoService commentInfoService;

    @GetMapping("/showAll")
    public Object showAll(Integer pageNO, Integer size) {

        return commentInfoService.findAll(pageNO, size);
    }

    @PostMapping("/updateAdd")
    public Object updateAddLikeNum(int commentId) {
        commentInfoService.updateAddLikeNum(commentId);

        return "ok";
    }

    @PostMapping("/updateReduce")
    public Object updateReduceLikeNum(int commentId) {
        commentInfoService.updateReduceLikeNum(commentId);

        return "ok";
    }

    @PostMapping("/addUC")
    public Object addUserComt(int userId,int commentId) {
        commentInfoService.addUC(userId, commentId);

        return "ok";
    }

    @PostMapping("/delUC")
    public Object delUC(int commentId, int userId) {
        commentInfoService.delUC(commentId, userId);

        return "ok";
    }



}
