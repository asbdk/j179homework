package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.StudentBean;

import java.util.List;

public interface IStudentService extends IService<StudentBean> {
}
