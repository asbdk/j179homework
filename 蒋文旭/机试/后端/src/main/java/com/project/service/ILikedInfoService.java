package com.project.service;

import com.project.bean.LikedInfoBean;

public interface ILikedInfoService{
    /**
     * 点赞
     * @param liked 点赞对象
     * @return
     */
    boolean addLiked(LikedInfoBean liked);


    /**
     * 修改点赞的状态。1标识已赞，0标识没有赞
     * @param liked
     * @return
     */
    void updateLiked(LikedInfoBean liked);


}
