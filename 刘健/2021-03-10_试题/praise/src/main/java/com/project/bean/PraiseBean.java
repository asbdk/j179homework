package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 用户和评论中间表
 * 点赞类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("t_praise")
public class PraiseBean {
    private static final long serialVersionUID = 1L;
    @TableId(value = "praise_id", type = IdType.AUTO)
    private Long praiseId;

    @TableField("praise_userid")
    private Long userId;

    @TableField("praise_commentid")
    private Long commentId;


}
