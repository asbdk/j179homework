package com.project.dao;

import com.project.bean.AdminInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.CommentInfoBean;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IAdminInfoDAO extends BaseMapper<AdminInfoBean> {
    AdminInfoBean findByName(String name);
}
