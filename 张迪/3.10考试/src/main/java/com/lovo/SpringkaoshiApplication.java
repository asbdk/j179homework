package com.lovo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringkaoshiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringkaoshiApplication.class, args);
    }

}
