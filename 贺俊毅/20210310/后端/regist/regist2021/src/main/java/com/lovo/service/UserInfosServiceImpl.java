package com.lovo.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.UserInfosBean;
import com.lovo.dao.IUserInfosDAO;
import com.lovo.service.IUserInfosService;
import com.lovo.util.RedisUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hjy
 * @since 2021-03-02
 */
@Service
public class UserInfosServiceImpl extends ServiceImpl<IUserInfosDAO, UserInfosBean> implements IUserInfosService {
    @Resource
    private IUserInfosDAO userInfosDAO;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public UserInfosBean login(String userName, String userPass) {
        UserInfosBean userInfosBean  =userInfosDAO.login(userName, userPass);
        return userInfosBean;
    }

    @Override
    public Object selectAll(Integer page,Integer size) {
        long start = System.currentTimeMillis();
        // 从缓存中获取数据
        Object obj = redisUtil.get("selectAllUser_"+page+"_"+size);
        if(obj == null){
            // 如果缓存中没有数据，从数据库获取，并加入到缓存
            obj = userInfosDAO.selectList(null);
            IPage pageObject = new Page(page,size);
            obj = userInfosDAO.selectPage(pageObject,null);
            redisUtil.set("selectAllUser",obj);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        return obj;
    }
}
