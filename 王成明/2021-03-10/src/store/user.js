import ajax  from "../service/ajax.js"

export default {
    namespaced:true,
    state:{
        param:{curr:1},
        pageObj:[],
        // token:""
    },
    mutations:{
        setParam(state,param){
            state.param = param;
        },
        setPageObj(state,pageObj){
            state.pageObj = pageObj;
        },
        // setToken(state,token){
        //     state.token = token;
        // }
    },
    actions:{
        async getPageList(context){
            var info = await ajax.getSubmitWithToken("/project/users/"+context.state.param.curr,$cookies.get('token'));
            if(info.status == 1){
                context.commit("setPageObj",info.data);
            }
        }
    }
}