package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.UserBean;

public interface IUserService extends IService<UserBean> {
    UserBean selectByUserName(String userName);
}
