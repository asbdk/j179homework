package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.CommentBean;
import com.lovo.bean.UserBean;

import java.util.List;

public interface IUserService extends IService<UserBean> {
    void addUser(UserBean userBean);

    UserBean findByName(String userName);

    UserBean findById(Integer id);

    List<CommentBean> findByUserName(String userName);


}
