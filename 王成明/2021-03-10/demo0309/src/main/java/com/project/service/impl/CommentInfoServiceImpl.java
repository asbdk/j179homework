package com.project.service.impl;

import com.project.bean.CommentInfoBean;
import com.project.dao.ICommentInfoDAO;
import com.project.service.ICommentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wangcm
 * @since 2021-03-10
 */
@Service
@Transactional
public class CommentInfoServiceImpl extends ServiceImpl<ICommentInfoDAO, CommentInfoBean> implements ICommentInfoService {

    @Resource
    private ICommentInfoDAO commentInfoDAO;

    @Override
    public List<CommentInfoBean> findAll() {
        List<CommentInfoBean> commentInfoBeans = commentInfoDAO.findAll();
        for (CommentInfoBean  commentBean: commentInfoBeans){
            commentBean.setCount(commentInfoDAO.countLikes(commentBean.getComId()));
        }
        return commentInfoBeans;
    }

    @Override
    public Integer countLikes(Integer comId) {
        return commentInfoDAO.countLikes(comId);
    }


}
