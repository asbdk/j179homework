package com.project.service;

import com.project.bean.UserBean;

public interface IUserService {
    /**
     * 登录
     * @param name 用户名
     * @param pwd 密码
     * @return
     */
    UserBean login(String name,String pwd);

    /**
     * 根据姓名查找
     * @param name 姓名
     * @return
     */
    UserBean findByName(String name);

    /**
     * 点赞
     * @param userId
     * @param commendId
     */
    void addZan(Long userId,Long commendId);

    /**
     * 取消赞
     * @param userId
     * @param commendId
     */
    void removeZan(Long userId,Long commendId);
}
