package com.project.controller;

import com.project.bean.LikedInfoBean;
import com.project.service.ILikedInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/liked")
public class LikedInfoController {
    @Resource
    private ILikedInfoService likedInfoService;

    @PostMapping
    public void addLiked(@RequestBody LikedInfoBean liked){
        likedInfoService.addLiked(liked);
    }
    @PutMapping
    public void updateLiked(@RequestBody LikedInfoBean liked){
        likedInfoService.updateLiked(liked);
    }

}
