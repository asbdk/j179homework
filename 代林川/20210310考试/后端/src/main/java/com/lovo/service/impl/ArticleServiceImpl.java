package com.lovo.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lovo.bean.ArticleBean;
import com.lovo.dao.IArticleDAO;
import com.lovo.service.IArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.util.RedisUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<IArticleDAO, ArticleBean> implements IArticleService {
    @Resource
    private IArticleDAO articleDAO;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public void updateNum(ArticleBean articleBean) {
        articleDAO.updateNum(articleBean);
    }

    @Override
    public Object selectAll(Integer page,Integer size) {
        // 从缓存中获取数据
        Object obj = redisUtil.get("selectAllArticle_"+page+"_"+size);
        if(obj == null){
            // 如果缓存中没有数据，从数据库获取，并加入到缓存
            IPage pageObject = new Page(page,size);
            obj = articleDAO.selectPage(pageObject,null);
            redisUtil.set("selectAllArticle_"+page+"_"+size,obj,60 * 60);
        }
        return obj;
    }
}
