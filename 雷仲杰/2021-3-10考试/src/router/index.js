import Vue from 'vue'
import VueRouter from 'vue-router'

import logins from '../pages/logins'
import comment from '../pages/comment'

Vue.use(VueRouter)

const routes = [
    
    {path:'/comment',component:comment},
    {path:'/logins',component:logins},
    

]

export default new VueRouter({
    mode:'history',
    routes
})