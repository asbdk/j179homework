package com.project.controller;

import com.github.pagehelper.PageInfo;
import com.project.bean.DoTeyDiaryBean;
import com.project.service.DoTeyDiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("diary")
public class IDoTeyDiaryController {
    @Autowired
    private DoTeyDiaryService service;

    @RequestMapping("findByItem")
    public PageInfo<DoTeyDiaryBean> findByItem(int pageNO, int doTeyDiaryId) {
        return service.findByItem(pageNO, doTeyDiaryId);
    }

    @RequestMapping("addDiary")
    public String addDiary(int doTeyId, DoTeyDiaryBean doTeyDiary) {
        service.addDiary(doTeyId, doTeyDiary);
        return "ok";
    }

    @RequestMapping("findById")
    public DoTeyDiaryBean findById(int diaryId) {
        return service.findById(diaryId);
    }

    @RequestMapping("delDiary")
    public String delDiary(int diaryId) {
        service.delDiary(diaryId);
        return "ok";
    }

    @RequestMapping("batchDel")
    public void batchDel() {

    }
}
