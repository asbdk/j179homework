package com.lovo.demo02.packResult;

public class ResoultPack {
    private Integer status;
    private String message;
    private Object data;

    public ResoultPack() {
    }

    public ResoultPack(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    public ResoultPack(Integer status, String message, Object data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public ResoultPack(ResoultCode resoultCode) {
        this.status = resoultCode.getStatus();
        this.message = resoultCode.getMessage();
    }
}
