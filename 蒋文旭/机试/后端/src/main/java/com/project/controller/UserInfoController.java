package com.project.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.project.bean.UserInfoBean;
import com.project.result.GlobalHandleException;
import com.project.result.ResultCode;
import com.project.result.UserValid;
import com.project.service.IUserInfoService;
import com.project.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/users")
@Api("用户注册和登录以及查看用户信息的控制器")
public class UserInfoController {

    @Resource
    private IUserInfoService userInfoService;

    @PostMapping("/login")
    @ApiOperation("登录方法")
    public Object login(String userName, String userPass) throws GlobalHandleException {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_name",userName);
        wrapper.eq("user_pass",DigestUtils.sha256Hex(userPass));
        UserInfoBean userInfoBean = userInfoService.getOne(wrapper);
        if(userInfoBean == null){
            throw new GlobalHandleException(ResultCode.USER_LOGIN_ERROR);
        }

//        session.setAttribute("user",userInfoBean);
        String token = JWTUtil.createToken(userInfoBean.getUserName(),userInfoBean.getUserPass(), Calendar.MINUTE,30);
        String refreshToken = JWTUtil.createToken(userInfoBean.getUserName(),userInfoBean.getUserPass(), Calendar.DATE,7);
        Map map = new HashMap();
        map.put("token",token);
        map.put("refreshToken",refreshToken);
        return map;
    }

    /**
     * 从token中获取用户对象
     * @return
     */
    @GetMapping("/token")
    @UserValid
    public Object getUserWithToken(HttpServletRequest request){
        return SecurityUtils.getSubject().getPrincipal();
    }

    @GetMapping("/refreshToken")
    @UserValid
    public Object refreshToken(HttpServletRequest request){
        UserInfoBean userInfoBean = (UserInfoBean) request.getAttribute("user");
        return JWTUtil.createToken(userInfoBean.getUserId().toString(),userInfoBean.getUserPass(),Calendar.SECOND,10);
    }

}

