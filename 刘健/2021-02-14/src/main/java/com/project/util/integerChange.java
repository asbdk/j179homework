package com.project.util;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class integerChange implements Converter<String,Integer> {

    @Override
    public Integer convert(String s) {
    if(s!=null && s.matches("\\d+")){
            return new Integer(s);
        }
        return null;
    }
}
