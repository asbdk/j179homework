package com.lovo;

import com.lovo.bean.UserInfoBean;
import com.lovo.service.IFunctionInfoService;
import com.lovo.service.IRoleInfoService;
import com.lovo.service.IUserInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FunctionInfoServiceTests {
    @Resource
    private IFunctionInfoService functionInfoService;
    @Resource
    private IUserInfoService userInfoService;
    @Test
    public void testSelectByRoleId(){
        UserInfoBean userInfoBean = userInfoService.selectByUserName("abc123");
        System.out.println(functionInfoService.selectByRoleId(userInfoBean.getRoleInfoBeans()));
    }
}
