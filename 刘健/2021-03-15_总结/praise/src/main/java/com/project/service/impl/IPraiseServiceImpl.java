package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.CommentBean;
import com.project.bean.PraiseBean;
import com.project.dao.ICommentDao;
import com.project.dao.IPraiseDao;
import com.project.service.IPraiseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class IPraiseServiceImpl extends ServiceImpl<IPraiseDao, PraiseBean> implements IPraiseService {
    @Resource
    private IPraiseDao dao;
    /**
     * 用中间表查询用户是否点赞。
     * @param userId
     * @return
     */
    @Override
    public PraiseBean findByPraise(Long commentId,Long userId){
        return dao.findByPraise(commentId,userId);
    }

    /**
     * 按用户id，评论id  增加点赞
     * @param userId 用户id
     * @param commentId 评论id
     */
    @Override
    public boolean addPraise(Long userId, Long commentId) {
        if(dao.findByPraise(commentId,userId)==null){
            dao.addPraise(userId,commentId);
            return true;
        }
        System.out.println("已经点赞");
        return false;
    }

    /**
     * 根据用户id ,评论id查询中间表，如果有的话就删除
     * @param userId
     * @param commentId
     */
    @Override
    public void deletePraise(Long userId, Long commentId){
        if(dao.findByPraise(commentId,userId)==null){
            dao.deletePraise(userId,commentId);
        }

    }

}
