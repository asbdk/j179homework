package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.UserInfoBean;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    @Select("select * from user_info where user_name=#{userName} and user_pwd=#{pwd}")
//    @ResultMap("userMap")
    UserInfoBean login(@Param("userName") String userName, @Param("pwd") String pwd);


    void like(@Param("userId") Long userId,@Param("replyId") Long replyId);
    void cancelLike(@Param("userId") Long userId,@Param("replyId") Long replyId);
}
