package com.lovo.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lovo.bean.UserInfoBean;
import com.lovo.result.GlobalHandleException;
import com.lovo.result.Result;
import com.lovo.service.IUserInfoService;

import javax.servlet.ServletResponse;
import java.io.IOException;

public class WebUtil {
    public static void writeValueAsString(ServletResponse response, Result result) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().print(mapper.writeValueAsString(result));
    }

    /**
     * 验证token的有效性
     * @param token
     * @param userInfoService
     * @return token对应的用户对象，如果为null表示验证失败
     * @throws IOException
     */
    public static UserInfoBean verifyToken(String token, IUserInfoService userInfoService) throws GlobalHandleException {
        if(token == null){
            return null;
        }
        String userName = null;
        try {
            userName = JWTUtil.getAudience(token);
        } catch (GlobalHandleException e) {
            return null;
        }
        UserInfoBean userInfoBean = userInfoService.selectByUserName(userName);
        if(userInfoBean == null){
            return null;
        }
        try {
            JWTUtil.verifyToken(token,userInfoBean.getUserPass());
        } catch (GlobalHandleException e) {
            return null;
        }

        return userInfoBean;
    }

}
