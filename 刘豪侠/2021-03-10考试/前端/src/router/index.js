import Vue from  "vue"
import VueRouter from "vue-router"
import loginks from "../loginks.vue"
import login from "../login.vue"
import registe from "../pages/registe.vue"
import index from "../pages/index.vue"
import menu from "../pages/menu.vue"
import commentInfo from "../pages/kaoshi/commentInfo.vue"




Vue.use(VueRouter)
const originalPush = VueRouter.prototype.push
  VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const routes = [ 
  {path:'/loginks',component:loginks},
  {path:'/commentInfo',component:commentInfo},
  {path:'/registe',component:registe},
  {path:'/login',component:login}, 
  {path:'/menu',component:menu,
  children:[
    {path:'/menu/index',component:index},]
  }
]

export default new VueRouter({
    mode:'history',
    routes  // routes:routes    可以简写成  routes
})
