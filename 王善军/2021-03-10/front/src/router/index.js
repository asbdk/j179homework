import Vue from 'vue'
import VueRouter from 'vue-router'
import app from "../App.vue"
import login from "../pages/Login.vue"
import index from "../pages/index.vue"



Vue.use(VueRouter)

const routes = [    
    {path:'/login',component:login},
    {path:'/app',component:app},
    {path:'/index',component:index}

]
export default new VueRouter({
    mode:'history',
    routes
})