package com.lovo.service;

import com.baomidou.mybatisplus.core.injector.methods.UpdateById;
import com.lovo.bean.CommentBean;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zack
 * @since 2021-03-10
 */
public interface ICommentService extends IService<CommentBean> {
    List<CommentBean> findAll();

    int findByCount(Integer userId, Integer commentId);
    void addCount(Integer userId, Integer commentId);
    void deleteCount(Integer userId, Integer commentId);
}
