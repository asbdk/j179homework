import Vue from 'vue'
import App from './App.vue'
import router from "./router/index.js"
import  store from "./store/index"
// import  "./css/mycss.css"

import ajax from "./service/ajax.js"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VueCookies from 'vue-cookies'

Vue.use(VueCookies);
Vue.use(ElementUI);
Vue.prototype.ajax = ajax;
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
