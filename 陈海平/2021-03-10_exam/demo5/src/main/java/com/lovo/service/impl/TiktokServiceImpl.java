package com.lovo.service.impl;

import com.lovo.bean.TiktokBean;
import com.lovo.dao.ITiktokDAO;
import com.lovo.service.ITiktokService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class TiktokServiceImpl extends ServiceImpl<ITiktokDAO, TiktokBean> implements ITiktokService {

}
