package com.lovo.dao;

import com.lovo.bean.UserInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    //UserInfoBean findByUserName(String userName);



}
