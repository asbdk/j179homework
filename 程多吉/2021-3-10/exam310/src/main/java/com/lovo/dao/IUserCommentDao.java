package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.UserCommentBean;

import java.io.Serializable;

public interface IUserCommentDao extends BaseMapper<UserCommentBean> {

    void delById(Integer id);


}
