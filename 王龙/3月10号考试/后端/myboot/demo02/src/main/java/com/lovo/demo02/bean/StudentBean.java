package com.lovo.demo02.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("t_student")
public class StudentBean implements Serializable {
    @TableId(value = "stu_id",type = IdType.ASSIGN_ID)
    private Long id ;
    @TableField(value = "stu_name")
    private String name;
}
