package com.project.service.impl;

import com.project.bean.UserBean;
import com.project.mapper.IUserMapper;
import com.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private IUserMapper mapper;

    @Override
    public UserBean login(String name, String pwd) {
    if(mapper.login(name, pwd)!=null){
        return mapper.login(name,pwd);
    }
        return null;
    }

    @Override
    public void add(UserBean user) {
        mapper.add(user);
    }
}
