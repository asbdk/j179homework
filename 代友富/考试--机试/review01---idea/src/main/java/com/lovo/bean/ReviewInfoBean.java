package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("review_info")
public class ReviewInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "rev_id", type = IdType.AUTO)
    private Long revId;
    @TableField("rev_content")
    private String revContent;

    private Integer num;

    @Override
    public String toString() {
        return "ReviewInfoBean{" +
                "revId=" + revId +
                ", revContent='" + revContent + '\'' +
                ", num=" + num +
                '}';
    }
}
