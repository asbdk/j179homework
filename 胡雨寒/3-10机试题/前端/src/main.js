import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import router from "./router/index.js";
import ajax from "./service/ajax.js"
import store from "./store/index.js"
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)
Vue.prototype.ajax=ajax;
Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.prototype.ajax=ajax;
new Vue({
  store,
  router:router,
  render: h => h(App),
}).$mount('#app')
