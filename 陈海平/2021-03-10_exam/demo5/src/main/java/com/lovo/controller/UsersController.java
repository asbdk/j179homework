package com.lovo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lovo.bean.UsersBean;
import com.lovo.result.GlobalHandleException;
import com.lovo.result.ResponseResult;
import com.lovo.result.ResultCode;
import com.lovo.result.UserValid;
import com.lovo.service.IUsersService;
import com.lovo.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@ResponseResult
@Api("用户注册和登录以及查看用户信息的控制器")
@RestController
@RequestMapping("/users-bean")
public class UsersController {
    @Resource
    private IUsersService service;
    @ApiOperation("登录方法")
    @PostMapping("/login")
    public Object login(String name, String pwd)throws GlobalHandleException {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_name",name);
        wrapper.eq("user_pwd",DigestUtils.sha256Hex(pwd));
        UsersBean userInfoBean = service.getOne(wrapper);
        if(userInfoBean == null){
            throw new GlobalHandleException(ResultCode.USER_LOGIN_ERROR);
        }

//        session.setAttribute("user",userInfoBean);
        String token = JWTUtil.createToken(userInfoBean.getUserName(),userInfoBean.getUserPwd(), Calendar.MINUTE,30);
        String refreshToken = JWTUtil.createToken(userInfoBean.getUserName(),userInfoBean.getUserPwd(), Calendar.DATE,7);
        String theretoken=JWTUtil.createToken(userInfoBean.getUserId().toString(), userInfoBean.getUserName(), Calendar.DATE,7);
        Map map = new HashMap();
        map.put("token",token);
        map.put("refreshToken",refreshToken);
        map.put("theretoken",theretoken);
        return map;

    }
    @GetMapping("/token")
    @UserValid
    public Object getUserWithToken(HttpServletRequest request) {
        return SecurityUtils.getSubject().getPrincipal();
    }
    @GetMapping("/user")
    @UserValid
    public Object getUser(HttpServletRequest request) {
        System.out.println(request.getHeader("theretoken"));
//        System.out.println(service.getById();
        return service.getById(request.getHeader("theretoken"));

    }


}

