package com.lovo.service;

import com.lovo.bean.ReviewInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IReviewInfoService extends IService<ReviewInfoBean> {
    List selectAllReview();
    void del( Integer userId, Integer revId);
    void add( Integer userId,  Integer revId);
    Integer findByRevId( Integer userId, Integer revId);
}
