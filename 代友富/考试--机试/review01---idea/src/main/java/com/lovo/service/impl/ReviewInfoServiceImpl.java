package com.lovo.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lovo.bean.ReviewInfoBean;
import com.lovo.dao.IReviewInfoDAO;
import com.lovo.service.IReviewInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class ReviewInfoServiceImpl extends ServiceImpl<IReviewInfoDAO, ReviewInfoBean> implements IReviewInfoService {
    @Resource
    private IReviewInfoDAO reviewInfoDAO;

    @Override
    public List selectAllReview() {
        return reviewInfoDAO.selectAllReview();
    }

    @Override
    public void del(Integer userId, Integer revId) {
        reviewInfoDAO.del(userId,revId);
    }

    @Override
    public void add(Integer userId, Integer revId) {
        reviewInfoDAO.add(userId,revId);
    }

    @Override
    public Integer findByRevId(Integer userId, Integer revId) {
        return reviewInfoDAO.findByRevId(userId,revId);
    }
}
