1、SpringMVC 工作原理
1、当客户端请求服务器，服务器使用前端控制器DispatcherServlet接收请求。DispatcherServlet借助HandlerMapping，根据请求的URL路径，定位到据图的Controller，和应用控制器的具体方法。并将封装好数据的实体对象传入应用控制器方法。
。DispatcherServlet根据路径完成页面转发。

2、SpringMVC 的控制器是不是单例模式,如果是,有什么问题,怎么解决
默认是单例模式
在多线程访问的时候有线程安全问题,不要用同步,会影响性能的,
解决方案是在控制器里面不能写成员变量。

3、什么是 CSRF 攻击
攻击者盗用了你的身份，以你的名义发送恶意请求。CSRF能够做的事情包括：以你名义发送邮件，发消息，盗取你的账号，甚至于购买商品，虚拟货币转账......造成的问题包括：个人隐私泄露以及财产安全。

4、什么是 WebSockets
WebSocket 是一种计算机通信协议，通过单个 TCP 连接提供全双工通信信道。
、WebSocket 是双向的 -使用 WebSocket 客户端或服务器可以发起消息发送。
WebSocket 是全双工的 -客户端和服务器通信是相互独立的。
单个 TCP 连接 -初始连接使用 HTTP，然后将此连接升级到基于套接字的连接。然后这个单一连接用于所有未来的通信。
。

5、怎么样在方法里面得到 Request,或者 Session
直接在方法的形参中声明request,SpringMvc就自动把request对象传入。

6、SpringMVC 怎么样设定重定向和转发的
转发：在返回值前面加"forward:"，譬如"forward:user.do?name=method4"
重定向：在返回值前面加"redirect:"，譬如"redirect:http://www.baidu.com"

7、SpringMvc 用什么对象从后台向前台传递数据的
通过ModelMap对象,可以在这个对象里面调用put方法,把对象加到里面,前台就可以通过el表达式拿到。

8、Spring Data JPA中一对一唯一外键关系和一对多双向关系在配置上有什么区别？
• 一对一唯一外键，拥有外键的为主控方
@OneToOne(mappedBy = "detailBean")
private StudentBean studentBean;
@JoinColumn中的name是指向外键字段
• 一对多（双向）：多方为主控方
@ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE},fetch = FetchType.LAZY)
@JoinColumn(name="s_c_id")
private ClassesBean classesBean;
@OneToMany(mappedBy = "classesBean")
private List<StudentBean> studentBeans = new ArrayList<StudentBean>();

9、请解释级联和关系维护这二者的特点
级联：对一个实体进行增删改操作时是否将它关联的其他对象
也进行相应的增删改操作。
关系维护：关系维护方负责维护外键。

10、级联操作有哪些，分别解释下各自的特点
① CascadeType.ALL：增删改所有级联
② CascadeType.PERSIST：级联增加操作
③ CascadeType.MERGE：级联更新操作
④ CascadeType.REMOVE：级联删除操作
⑤CascadeType.REFRESH：级联刷新操作
⑥CascadeType.DETACH：级联脱管/游离操作




