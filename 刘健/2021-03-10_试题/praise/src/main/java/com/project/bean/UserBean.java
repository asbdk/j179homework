package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//@EqualsAndHashCode(callSuper = false)
@TableName("t_user")
public class UserBean{
    private static final long serialVersionUID = 1L;
    @TableId(value = "user_id", type = IdType.NONE)
    private Long userId;

    @TableField("user_name")
    private String userName;

    @TableField("user_pwd")
    private String userPwd;

//    @TableField("user_header")
//    private String userHeader;

//    @TableField(exist = false)
//    private List<CommentBean> commentBeans = new ArrayList<CommentBean>();

    @Override
    public String toString() {
        return "UserBean{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userPwd='" + userPwd + '\'' +
//                ", commentBeans=" + commentBeans +
                '}';
    }
}