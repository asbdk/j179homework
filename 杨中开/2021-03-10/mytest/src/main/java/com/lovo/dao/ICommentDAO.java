package com.lovo.dao;

import com.lovo.bean.CommentBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zack
 * @since 2021-03-10
 */
public interface ICommentDAO extends BaseMapper<CommentBean> {
    List<CommentBean> findAll();

    int findByCount(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    void addCount(@Param("userId") Integer userId, @Param("commentId") Integer commentId);

    void deleteCount(@Param("userId") Integer userId, @Param("commentId") Integer commentId);
}
