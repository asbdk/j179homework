package com.project.service;

import com.project.bean.UserBean;

public interface IUserService {
    void add(UserBean userBean);

    /**
     * 更加用户名查找用户信息
     * @param name
     * @return
     */
    UserBean findByName(String name);

    /**
     * 登录查询
     * @param name
     * @param pwd
     * @return
     */
    UserBean getOne(String name,String pwd);
}
