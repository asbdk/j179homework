package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.DianzanBean;

import java.util.List;

public interface IDianzanService extends IService<DianzanBean> {
    List selectAll();
    void updateScore(int id, int num);
    void addUserDian(int userId, int dianzanId);
    void delUserDian(int userId, int dianzanId);
}
