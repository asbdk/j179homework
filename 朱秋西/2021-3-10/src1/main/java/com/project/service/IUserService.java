package com.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.UserBean;

public interface IUserService extends IService<UserBean> {
    UserBean login(@Param("userName") String userName, @Param("userPwd") String userPwd);

    UserBean selectByUserName(String userName);
}