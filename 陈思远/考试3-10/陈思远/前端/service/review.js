import axios from 'axios';
import qs from 'qs';
const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}

export const login = async (userName,userPass) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/project/users/login",
        data:qs.stringify({
            userName,userPass
        })
    })
    return response.data;
}
export const getUser = async (token) => {
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/project/users/token"
    })
    return response.data;
}
export const refreshToken = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/project/users/refreshToken"
    })
    return response.data;
}

export const UP = async (reviewId,userId) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/project/review/up",
        data:qs.stringify({
            reviewId,userId
        })
    })
    return response.data;
}
export const DOWN = async (reviewId,userId) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/project/review/down",
        data:qs.stringify({
            reviewId,userId
        })
    })
    return response.data;
}
