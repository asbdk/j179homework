package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("user_s")
public class UserBean implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @TableId(value = "user_id", type = IdType.NONE)
    private Long id;
    @TableField("name")
    private String name;
    @TableField("wpd")
    private String wpd;
    private List<CommentBean> commentList;


}