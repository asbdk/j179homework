1、Spring Boot是什么？
	Spring Boot是为了让程序员更好的使用Spring。简化了Spring应用初要搭建配置的过程。
	Spring Boot整合很多开发中普遍用到的组件和框架，方便开发人员快速搭建和开发的一个框架。

2、Spring Boot有哪些优点？
	方便开发者开发，精简了配置的过程，避免了各种版本冲突
	有助于避免适用XML
	继承了大量常用的第三方库的配置，为这些第三方库提供了开箱用的能力

3、Spring Boot自动配置的原理是什么？
	启动类上的核心注解SpringBootApplication注解主配置类，有了这个在主配置类启动时			SpringBoot就会注解自动配置

4、介绍下Spring Boot配置的加载顺序。
	1、开发者工具 `Devtools` 全局配置参数；

	2、单元测试上的 `@TestPropertySource` 注解指定的参数；

	3、单元测试上的 `@SpringBootTest` 注解指定的参数；

	4、命令行指定的参数，如 `java -jar springboot.jar --name="Java技术栈"`；

	5、命令行中的 `SPRING_APPLICATION_JSONJSON` 指定参数, 如 `java -	Dspring.application.json='{"name":"Java技术栈"}' -jar springboot.jar`

	6、`ServletConfig` 初始化参数；

	7、`ServletContext` 初始化参数；

	8、JNDI参数（如 `java:comp/env/spring.application.json`）；

	9、Java系统参数（来源：`System.getProperties()`）；

	10、操作系统环境变量参数；

	11、`RandomValuePropertySource` 随机数，仅匹配：`ramdom.*`；

	12、JAR包外面的配置文件参数（`application-{profile}.properties（YAML）`）

	13、JAR包里面的配置文件参数（`application-{profile}.properties（YAML）`）

	14、JAR包外面的配置文件参数（`application.properties（YAML）`）

	15、JAR包里面的配置文件参数（`application.properties（YAML）`）

	16、`@Configuration`配置文件上 `@PropertySource` 注解加载的参数；

	17、默认参数（通过 `SpringApplication.setDefaultProperties` 指定）；

5、YAML配置比起Properties配置的优势在哪里？
	代码可视化程度高，有注释提示
	配置简单

6、如何实现Spring Boot的热部署
	使用springloaded配置pom.xml文件，使用mvn spring-boot:run启动
	使用springloaded本地加载启动，配置jvm参数-javaagent:<jar包地址> -noverify
	使用devtool工具包，操作简单，但是每次需要重新部署

7、Spring Boot的核心注解有哪些？
	@SpringBootApplication
	@SpringBootConfiguration
	@SpringBootTest

8、介绍下Spring Boot中的监视器。
	是Spring Boot启动框架中的重要功能之一。
	监视器可帮助访问生产环境中正在运行的应用程序的当前状态。

9、什么是 Spring Boot Stater ？
	starter会把所有用到的依赖都给包含进来，避免了开发者自己去引入依赖所带来的麻烦。

10、Spring 和 Spring Boot 有什么不同？
	Spring Boot是Spring框架的扩展，解决了Spring需要做一系列配置的操作。