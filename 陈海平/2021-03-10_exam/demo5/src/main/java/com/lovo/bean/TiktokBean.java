package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("tiktok")
public class TiktokBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "tiktok_id",type = IdType.NONE)
    private Long tiktokId;
    @TableField("tiktok_speak")
    private Long tiktokSpeak;
    @TableField("tiktok_user")
    private Long tiktokUser;
}
