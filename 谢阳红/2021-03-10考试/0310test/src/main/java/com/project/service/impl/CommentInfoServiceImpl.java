package com.project.service.impl;

import com.project.bean.CommentInfoBean;
import com.project.dao.ICommentInfoDAO;
import com.project.service.ICommentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class CommentInfoServiceImpl extends ServiceImpl<ICommentInfoDAO, CommentInfoBean> implements ICommentInfoService {
    @Override
    public List<CommentInfoBean> findAll() {
        return this.baseMapper.findAll();
    }

    @Override
    public void updateZan(int userId ,int commentId) {
        Integer info =  this.baseMapper.findById(userId, commentId);
        if(info >= 1){
            this.baseMapper.delUserCommentInfo(userId, commentId);
        } else {
            this.baseMapper.addUserCommentInfo(userId, commentId);
        }
    }
}
