package com.lovo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lovo.dao")
public class KaoshiApplication {

    public static void main(String[] args) {
        SpringApplication.run(KaoshiApplication.class, args);
    }

}
