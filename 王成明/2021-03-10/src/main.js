import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import router from './router'
import store from './store'
import ajax from "./service/ajax.js"
import VueCookies from 'vue-cookies'

Vue.use(VueCookies);
Vue.use(ElementUI);
Vue.prototype.ajax = ajax;

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
