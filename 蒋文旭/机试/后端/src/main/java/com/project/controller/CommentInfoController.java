package com.project.controller;


import com.project.result.UserValid;
import com.project.service.ICommentInfoService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/comment")
public class CommentInfoController {
    @Resource
    private ICommentInfoService commentInfoService;

    @ApiOperation(value = "显示所有评论")
    @GetMapping
    @UserValid
    public Object showAll(){
        return commentInfoService.selectAll();
    }

}

