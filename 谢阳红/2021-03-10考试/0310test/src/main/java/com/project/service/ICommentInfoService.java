package com.project.service;

import com.project.bean.CommentInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentInfoService extends IService<CommentInfoBean> {
    List<CommentInfoBean> findAll();

    void updateZan(int userId,int commentId);
}
