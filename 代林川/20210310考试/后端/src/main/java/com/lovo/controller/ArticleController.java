package com.lovo.controller;


import com.lovo.bean.ArticleBean;
import com.lovo.service.IArticleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/article")
public class ArticleController {
    @Resource
    private IArticleService articleService;

    @ApiOperation("显示所有评论方法")
    @GetMapping("/showAll")
    public Object showAll(Integer page,Integer size){
        return articleService.selectAll(page, size);
    }

}

