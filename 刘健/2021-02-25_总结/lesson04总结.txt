1、什么是 YAML？
 	一般YAML文件扩展名为.yaml，或者yml.
	YAML是一种对人友好的对各种程序语言的数据序列化标准
	YAML是配置文件

2、什么是JPA？
	JPA是java持久化的规范，简化sql操作


3、阐述Spring Data JPA的概念
	Spring Data JPA会根据DAO调用方法，自动生成SQL语句，进行表数据的增删改查。
	开发效率高，灵活

4、描述MyBatis和Hibernate框架的区别
	Hibernate是全自动MyBatis是半自动，Hibernate是手自一体变速箱，可以换手动模式，MyBatis算手动挡
	MyBatis需要手动写映射，sql语句，Hibernate一些简单的增删改查则不需要，
	复杂一些的sql还是MyBatis更实用，Hibernate是方便灵活“小巧”

5、什么是JPQL，它和SQL有什么区别
	JPQL是面向对象查询，SQL是数据库查询
	JPQL页可以使用原生sql语句

6、阐述Jpa、Hibernate、Spring Data Jpa三者之间的关系。
	JPA是一套规范，内部是有接口和抽象类组成的。
	hibernate是一套成熟的ORM框架，而且Hibernate实现了JPA规范，所以也可以称hibernate为JPA的一种实现方式，
	Spring Data JPA是Spring提供的一套对JPA操作更加高级的封装

	
7、Spring Data提供了哪些接口来实现持久化操作？请详细描述它们。
	在mapper（DAO）层使用JpaRepository接口，

8、描述实体对象在持久化容器中的状态。
	对象没有被使用的时候是临时状态，
	方法里的对象在方法执行时持久状态
	方法结束后对象处于游离状态。
	没有被使用的对象在程序结束之后被回收

9、自定义持久层接口的方法有哪些？
	使用spring + Hibernate 
	Spring Data JPA


10、在Spring Data JPA中，ID生成策略有哪些？如何实现？

	 @Id

	主键自增长 @GeneratedValue(strategy = GenerationType.IDENTITY)

	最大ID+1  	@GenericGenerator(name = "hbincrement",strategy = "increment")
		        //   @GeneratedValue(generator = "hbincrement")
	
	生成无意义的随机数，一大串
	 @GenericGenerator(name = "idGenerator",strategy = "uuid")
   	 @GeneratedValue(generator = "idGenerator")

	@Column(name = "pk_id")
  	  private String id;