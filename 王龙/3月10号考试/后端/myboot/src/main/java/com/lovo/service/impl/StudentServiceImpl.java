package com.lovo.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.StudentBean;
import com.lovo.dao.IStudentDao;
import com.lovo.service.IStudentService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImpl extends ServiceImpl<IStudentDao, StudentBean> implements IStudentService {

}
