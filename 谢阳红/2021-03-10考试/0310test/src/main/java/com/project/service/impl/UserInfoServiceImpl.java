package com.project.service.impl;

import com.project.bean.UserInfoBean;
import com.project.dao.IUserInfoDAO;
import com.project.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
@Transactional
public class UserInfoServiceImpl extends ServiceImpl<IUserInfoDAO, UserInfoBean> implements IUserInfoService {
    @Resource
    private IUserInfoDAO userInfoDAO;
    @Override
    public UserInfoBean findByName(String userName) {
        return userInfoDAO.findByName(userName);
    }
}
