import Vue from "vue"
import Vuex from 'vuex'
import user from "./user.js"
import list from "./list.js"

Vue.use(Vuex)

export default new Vuex.Store({
    //导入子模块
    modules:{
        user, list,
    }
})