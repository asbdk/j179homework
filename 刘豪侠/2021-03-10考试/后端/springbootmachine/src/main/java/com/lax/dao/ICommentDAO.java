package com.lax.dao;


import com.lax.bean.CommentBean;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentDAO extends BaseMapper<CommentBean> {

    List<CommentBean> findAll();
    Integer findById(@Param("idAdmin") int idAdmin,@Param("idComment") int idComment);

    void addAdminCommentInfo(@Param("idAdmin") int idAdmin,@Param("idComment") int idComment);

    void delAdminCommentInfo(@Param("idAdmin") int idAdmin,@Param("idComment") int idComment);

}
