import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../pages/Login'
import Manage from '../pages/Manage'


Vue.use(VueRouter)

const routes = [
    {path:'/login',component:Login},
  
    {path:'/manage',component:Manage}
]

export default new VueRouter({
    mode:'history',
    routes
})