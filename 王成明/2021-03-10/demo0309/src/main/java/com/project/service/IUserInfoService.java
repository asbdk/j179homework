package com.project.service;

import com.project.bean.UserInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Wangcm
 * @since 2021-03-10
 */
public interface IUserInfoService extends IService<UserInfoBean> {


    /**
     * 点赞，中间表中加入数据
     * @param userId 用户id
     * @param commentId 评论id
     */
    void addLikes(int userId,int commentId);

    /**
     * 取消点赞
     * @param userId 用户id
     */
    void cancelLikes(int userId,int commentId);

    /**
     * 判断当前登录用户是否对该评论点赞
     * @param userId 用户id
     * @param comId 评论id
     * @return 信息
     */
    Integer checkLikes(Integer userId,Integer comId);

}
