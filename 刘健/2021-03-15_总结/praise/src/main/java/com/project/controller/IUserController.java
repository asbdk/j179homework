package com.project.controller;

import com.project.service.IUserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("users")
public class IUserController {

    @Resource
    private IUserService service;

    @RequestMapping("")
    public String login(String userName, String userPwd){
     service.login(userName,userPwd);
        return "ok";
    }
}
