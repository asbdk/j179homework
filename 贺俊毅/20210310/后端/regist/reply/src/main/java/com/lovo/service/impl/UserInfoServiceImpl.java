package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.ReplyInfoBean;
import com.lovo.bean.UserInfoBean;
import com.lovo.dao.IReplyInfoDAO;
import com.lovo.dao.IUserInfoDAO;
import com.lovo.service.IUserInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<IUserInfoDAO, UserInfoBean> implements IUserInfoService {
    @Resource
    private IUserInfoDAO userInfoDAO;
    @Resource
    private IReplyInfoDAO replyInfoDAO;
    @Override
    public UserInfoBean login(String userName, String pwd) {
        return userInfoDAO.login(userName, pwd);
    }

    @Override
        public void like(Long userId, Long replyId) {
        if (userId == null) {
            return;
        } else {
            ReplyInfoBean replyInfoBean = replyInfoDAO.selectLike(replyId, userId);
            if (replyInfoBean == null) {
                userInfoDAO.like(userId, replyId);
            } else {
                userInfoDAO.cancelLike(userId, replyId);
            }

        }
    }


}
