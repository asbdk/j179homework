package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_comment")
public class CommentBean {
    @TableId(value = "comment_id",type = IdType.AUTO)
    private Integer id;
    @TableField("comment_content")
    private String content;
    @TableField(exist = false)
    private List<UserCommentBean> userCommentBeanList;

    private Integer number;
}
