package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.UserInfoBean;
import com.project.dao.IUserInfoDao;
import com.project.service.IUserInfoService;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl extends ServiceImpl<IUserInfoDao, UserInfoBean> implements IUserInfoService {

}
