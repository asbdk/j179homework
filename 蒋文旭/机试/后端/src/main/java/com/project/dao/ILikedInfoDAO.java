package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.CommentInfoBean;
import com.project.bean.LikedInfoBean;

public interface ILikedInfoDAO extends BaseMapper<CommentInfoBean> {
    /**
     * 点赞
     * @param liked 点赞对象
     * @return
     */
    boolean addLiked(LikedInfoBean liked);

    /**
     * 修改点赞的状态。1标识已赞，0标识没有赞
     * @param liked
     * @return
     */
    void updateLiked(LikedInfoBean liked);

    /**
     * 取消点赞
     * @param likedId
     * @return
     */
    boolean removeLiked(Long likedId);

}
