package com.project.shiro;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.project.bean.UserInfoBean;
import com.project.service.IUserInfoService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.List;

public class CustomRealm extends AuthorizingRealm {
    @Resource
    private IUserInfoService userInfoService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 身份认证的操作
        //1.获取用户输入的账号
        String username = (String) authenticationToken.getPrincipal();
        //2.通过username从数据库中查找到user实体
//        UserInfoBean userInfoBean = userInfoService.selectByUserName(username);
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("user_name", username);
        UserInfoBean userInfoBean = userInfoService.getOne(wrapper);
        if (userInfoBean == null) {
            return null;
        }
        //3.通过SimpleAuthenticationInfo做身份处理
        SimpleAuthenticationInfo simpleAuthenticationInfo =
                new SimpleAuthenticationInfo(userInfoBean, userInfoBean.getUserPwd(), getName());
        //4.返回身份处理对象
        return simpleAuthenticationInfo;

    }


}
