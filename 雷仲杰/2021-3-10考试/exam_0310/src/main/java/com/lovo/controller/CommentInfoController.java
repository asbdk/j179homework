package com.lovo.controller;


import com.lovo.result.ResponseResult;
import com.lovo.service.ICommentInfoService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/comments")
@ResponseResult
@Api("评论的控制器")
public class CommentInfoController {
    @Resource
    private ICommentInfoService service;

    @GetMapping("/showAll")
    public Object showAll(){
        return service.showAll();
    }

    @PostMapping("/updateAdd")
    public Object updateAdd(Long userId,Long comId){
        service.updateAdd(userId, comId);
        return "ok";
    }

    @PostMapping("/updateCut")
    public Object updateCut(Long userId,Long comId){
        service.updateCut(userId, comId);
        return "ok";
    }
}

