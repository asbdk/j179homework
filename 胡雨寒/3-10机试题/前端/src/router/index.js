import Vue from 'vue'

import VueRouter from 'vue-router'
import vuexuserList from '../page/platfromUserAccountList.vue'
import vuexLogin from '../login/login.vue'
import vuexAppOne from '../page/AppOne.vue'
Vue.use(VueRouter)

const routes = [    
    {path:'/vuexLogin',component:vuexLogin},
    {path:'/login',component:vuexLogin},
    {path:'/AppOne',component:vuexAppOne,
        children:[
            {path:'/AppOne/vuexSaleCont',component:vuexuserList},
        ]
    
    }
       

] 

export default new VueRouter({
    mode:'history',
    routes
})
const originalPush = VueRouter.prototype.push
   VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}