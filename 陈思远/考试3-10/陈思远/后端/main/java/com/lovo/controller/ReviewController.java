package com.lovo.controller;

import com.lovo.result.ResponseResult;
import com.lovo.result.UserValid;
import com.lovo.service.IReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/review")
@ResponseResult
@Api("评论控制器")
public class ReviewController {
    @Resource
    private IReviewService service;

    @ApiOperation(value = "显示所有评论")
    @GetMapping
    @UserValid
    public Object showAll(){
        return service.selectAll();
    }

    @PostMapping("up")
    @ApiOperation(value = "点赞")
        public Object updateReview(int reviewId, int userId){
        service.updateScore(reviewId,1);
        service.addUserReview(userId,reviewId);
        return "ok";
    }

    @PostMapping("down")
    @ApiOperation(value = "取消点赞")
    public Object downReview(int reviewId, int userId){
        service.updateScore(reviewId,-1);
        service.delUserReview(userId,reviewId);
        return "ok";
    }

}
