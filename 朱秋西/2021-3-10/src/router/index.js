import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../pages/login'
import Manage from '../pages/manage'

Vue.use(VueRouter)

const routes = [
    {path:'/login',component:Login},
    {path:'/manage',component:Manage}
]

export default new VueRouter({
    mode:'history',
    routes
})