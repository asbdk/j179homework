package com.lovo.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.UserCommentBean;
import com.lovo.dao.IUserCommentsDao;
import com.lovo.service.IUserCommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserCommentServiceImpl extends ServiceImpl<IUserCommentsDao,UserCommentBean> implements IUserCommentService {
    @Resource
    private IUserCommentsDao DAO;
    @Override
    public void add(UserCommentBean userCommentBean) {
        DAO.insert(userCommentBean);
    }

    @Override
    public void del(Long userCommentId) {
        DAO.deleteById(userCommentId);
    }

    @Override
    public UserCommentBean find(Long userId, Long commentsId) {
        return DAO.find(userId,commentsId);
    }
}
