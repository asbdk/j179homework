package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.CommentsBean;
import com.lovo.bean.UserBean;
import com.lovo.dao.ICommentsDAO;
import com.lovo.dao.IUserDAO;
import com.lovo.service.ICommentsService;
import com.lovo.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class CommentsServiceImpl extends ServiceImpl<ICommentsDAO, CommentsBean> implements ICommentsService {

    @Resource
    private ICommentsDAO dao;
    @Resource
    private IUserDAO userDao;
    @Override
    public void changeStar(Long userId,Long cId, Integer num) {
        dao.changeStar(cId,num);
        UserBean userBean = userDao.selectById(userId);
        System.out.println("   用户"+userBean);
        List<Long> list = userBean.getCommentsList();
        System.out.println(userBean.getCommentsList());
        list.add(cId);
    }

}
