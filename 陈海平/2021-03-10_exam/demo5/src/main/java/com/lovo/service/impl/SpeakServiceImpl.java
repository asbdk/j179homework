package com.lovo.service.impl;

import com.lovo.bean.SpeakBean;
import com.lovo.dao.ISpeakDAO;
import com.lovo.service.ISpeakService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class SpeakServiceImpl extends ServiceImpl<ISpeakDAO, SpeakBean> implements ISpeakService {
    @Resource
    private ISpeakDAO dao;

    @Override
    public void add(SpeakBean speak) {
        dao.insert(speak);
    }

    @Override
    public List<SpeakBean> findAll(Long user) {

        return dao.findAll(user);
    }

}
