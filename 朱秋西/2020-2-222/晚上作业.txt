1. 描述下Servlet的生命周期
    1，容器加载并实例化Servlet
    2，调用init方法完成初始化
    3，当请求到达，调用service方法，处理请求，产生响应
    4，销毁阶段，调用destroy（）方法，完成资源清理
2. 描述Cookie和Session的作用，区别和各自的应用范围
    Cookie工作流程：客户端请求服务器，服务器发送cookie信息给客户端。在产生响应时，
会产生Set-cookie响应头，再该响应头中，以键值对方式描述cookie信息。响应信息到达客
户端以后，会将cookie信息存储在客户端。当客户端再次发送请求时，会将之前服务器发送
给客户端的cookie信息，在以cookie请求头方式发送给服务器。服务器得到的信息和发送的
信息是一致的，就认为是同一用户。
    工作流程：客户端访问服务器，服务器为了跟踪该用户的信息，为该用户产生一个session
对象，同时为该session对象产生一个唯一标识sessionId。为了管理众多用户的session信息，
以sessionId为键，以session对象为值，将session信息存放在Map集合中。在产生响应时，
服务器将sessionId以Set-cookie响应头的方式发送给客户端。客户端再请求服务器时，会
将sessionId再以Cookie请求头的方式发送给服务器。服务器得到sessionId后，从map集合
中，取出session对象，从而跟踪状态。
3. 依赖注入的方式有几种，各是什么？
    	接口注入
    	设值注入
    	构造器注入
4. Spring 由哪些模块组成?
  	Spring Core
  	Spring Context
  	Spring Context
  	Spring ORM
  	Spring AOP
 	Spring Web
    	Spring Web MVC

5. IOC 的优点是什么？
    	资源集中管理，实现资源的可配置和易管理。
  	降低了使用资源双方的依赖程度，也就是我们说的耦合度。

6. ApplicationContext 通常的实现是什么?
    	ApplicationContext是BeanFactory的子接口，从容器中取出的组件，为立即加载方式。提
供配置类、注解等多种方式，实例化Spring容器中管理的组件，并协作对象间的关联关系。

7. Spring 有几种配置方式？
    XML文件
    注解
   java代码

8.  Spring 框架中都用到了哪些设计模式？
    工厂设计模式 : Spring使用工厂模式通过 BeanFactory、ApplicationContext 创建 bean 对象。
    代理设计模式 : Spring AOP 功能的实现。
    单例设计模式 : Spring 中的 Bean 默认都是单例的。
    模板方法模式 : Spring 中 jdbcTemplate、hibernateTemplate 等以 Template 结尾的对数据库操作的类，它们就使用到了模板模式。
    包装器设计模式 : 我们的项目需要连接多个数据库，而且不同的客户在每次访问中根据需要会去访问不同的数据库。这种模式让我们可以根据客户的需求能够动态切换不同的数据源。
    观察者模式: Spring 事件驱动模型就是观察者模式很经典的一个应用。
    适配器模式 :Spring AOP 的增强或通知(Advice)使用到了适配器模式、spring MVC 中也是用到了适配器模式适配Controller。

9. BeanFactory 接口和 ApplicationContext 接口有什么区别 ？
    BeanFactory从容器中取出的组件，为懒加载方式。
    ApplicationContext是beanFactory的子接口，从容器中取出的组件，为立即加载方式。
提供配置类、注解等多种方式，实例化Spring容器中管理的组件，并协作对象间的关联关系。

10. BeanFactory和FactoryBean的区别？
    BeanFactory是个Factory，也就是IOC容器或对象工厂，FactoryBean是个Bean。在Spring中，
所有的Bean都是由BeanFactory(也就是IOC容器)来进行管理的。但对FactoryBean而言，这个
Bean不是简单的Bean，而是一个能生产或者修饰对象生成的工厂Bean,它的实现与设计模式中的
工厂模式和修饰器模式类似 。
    BeanFactory：在Spring中，BeanFactory是IOC容器的核心接口，它的职责包括：实例化、定位、配置应用程序
中的对象及建立这些对象间的依赖。BeanFactory只是个接口，并不是IOC容器的具体实现，但是
Spring容器给出了很多种实现，如 DefaultListableBeanFactory、XmlBeanFactory、
ApplicationContext等，其中XmlBeanFactory就是常用的一个，该实现将以XML方式描述组成应
用的对象及对象间的依赖关系。
    FactoryBean：一般情况下，Spring通过反射机制利用<bean>的class属性指定实现类实例化Bean，在某些情
况下，实例化Bean过程比较复杂，如果按照传统的方式，则需要在<bean>中提供大量的配置信息
。配置方式的灵活性是受限的，这时采用编码的方式可能会得到一个简单的方案。Spring为此提供
了一个org.springframework.bean.factory.FactoryBean的工厂类接口，用户可以通过实现该接口
定制实例化Bean的逻辑。FactoryBean接口对于Spring框架来说占用重要的地位，Spring自身就提
供了70多个FactoryBean的实现。