package com.lovo.demo02.controller;
import com.lovo.demo02.service.IStudentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/test")
public class ControllerTest {
    @Resource
    private IStudentService service;
    @RequestMapping("/t01")
    public String t01(){
        int a = 10/0;
        return "你好！李焕英";
    }
    @RequestMapping("/t02")
    public String t02(){
        service.eat();
        return "完成了调用业务方法！";
    }
}
