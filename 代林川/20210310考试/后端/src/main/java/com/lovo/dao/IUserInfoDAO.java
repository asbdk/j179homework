package com.lovo.dao;

import com.lovo.bean.UserInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    UserInfoBean findByUidAndAid(Integer userId,Integer articleId);
    UserInfoBean selectByUserName(String username);
    void deleteGreat(Integer userId,Integer articleId);
    void addGreat(Integer userId,Integer articleId);
}
