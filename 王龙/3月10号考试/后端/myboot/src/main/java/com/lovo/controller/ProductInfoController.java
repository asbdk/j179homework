package com.lovo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wanglong
 * @since 2021-02-24
 */
@RestController
@RequestMapping("/product-info-bean")
public class ProductInfoController {

}

