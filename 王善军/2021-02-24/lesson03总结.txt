1、Spring Boot是什么？
	Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。
	该框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。
2、Spring Boot有哪些优点？
	为Spring的开发提供了更快的入门体验；
	零配置（帮开发人员将大量常用的配置提前设置好）；
	集成了大量常用的第三方库的配置，为这些第三方库提供了开箱即用的能力；
	提供了一系列大型项目常用的非功能性特征，如嵌入式服务器、安全性、度量、运行状况检查、外部化配置等；
3、Spring Boot自动配置的原理是什么？
	主要是Spring Boot的启动类上的核心注解SpringBootApplication注解主配置类，有了这个主配置类启动时就会为SpringBoot开启一个@EnableAutoConfiguration注解自动配置功能。
	有了这个EnableAutoConfiguration的话就会：
	1，从配置文件METAINF/Spring.factories加载可能用到的自动配置类
	2、去重，并将exclude和excludeName属性携带的类排除
	3、过滤，将满足条件（@Conditional）的自动配置类返回
4、介绍下Spring Boot配置的加载顺序。
	首先是根目录下的config文件下的配置文件；
	然后是根目录下的其他文件；
	再是resources下的config文件下的配置文件；
	再是resources根目录下的其他文件；
	
5、YAML配置比起Properties配置的优势在哪里？
	配置层次分明，能够很好的阅读。
	.yml拥有天然的树状结构；
	在properties文件中是以”.”进行分割的， 在.yml中是用”:”进行分割；
	.yml的数据格式是K-V格式（和json很像），并且通过”:”进行赋值；
	在.yml中缩进一定不能使用TAB，否则会报很奇怪的错误；
	每个k的冒号后面一定都要加一个空格；
	.yml比.properties对中文对支持更友好。
6、如何实现Spring Boot的热部署
		
	1. 使用springloaded配置pom.xml文件，使用mvn spring-boot:run启动
	2. 使用springloaded本地加载启动，配置jvm参数
	3. 使用devtool工具包，操作简单，但是每次需要重新部署
7、Spring Boot的核心注解有哪些？
	@SpringBootApplication
	@EnableAutoConfiguration
	@SpringBootConfiguration
	@Configuration
	@ComponentScan
	@Conditional
	@ConditionalOnBean
	@ConditionalOnMissingBean
	@ConditionalOnClass
	@ConditionalOnMissingClass
	@ConditionalOnWebApplication
	@ConfigurationProperties
8、介绍下Spring Boot中的监视器。
	Spring boot actuator 是 spring 启动框架中的重要功能之一。Spring boot 监视器可帮助您访问生产环境中正在运行的应用程序的当前状态
	。有几个指标必须在生产环境中进行检查和监控。即使一些外部应用程序可能正在使用这些服务来向相关人员触发警报消息。监视器模块公开了一组可
	直接作为 HTTP URL 访问的REST 端点来检查状态。
9、什么是 Spring Boot Stater ？
	starter可以理解成pom配置了一堆jar组合的空maven项目，用来简化maven依赖配置，
	starter可以继承也可以依赖于别的starter。
10、Spring 和 Spring Boot 有什么不同？
	Spring需要配置各种文件，而spring Boot消除了大部分的配置，简化的程序员的环境搭建操作，避免了编写大量的样板代码，注释和XML配置。