package com.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.project.dao")
public class Exam210310Application {

    public static void main(String[] args) {
        SpringApplication.run(Exam210310Application.class, args);
    }

}
