package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.message.AsynchronouslyFormattable;

import java.io.Serializable;
@NoArgsConstructor

@Data
@TableName(value = "student_info0223")
public class StudentBean implements Serializable {
    @TableId(value = "stu_id",type = IdType.AUTO)
    private Integer id;
    @TableField(value = "stu_age")
    private Integer age;
    @TableField(value = "stu_name")
    private String name;
    @TableField("stu_sex")
    private String sex;

    public StudentBean(Integer id, Integer age, String name, String sex) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.sex = sex;
    }
}
