1. 描述下Servlet的生命周期
servlet生命周期
	1.容器加载并实例化Servlet
	2.调用init（）完成初始化
	3.当请求到达，调用service（）方法，处理请求，产生响应
	4.销毁阶段，容器调用destroy（）方法完成资源清理
	在整个生命周期中，1，2，4都只进行一次，只有第3步，每次都会执行。而servlet实现类是一个但实例，多线程的一个类

2. 描述Cookie和Session的作用，区别和各自的应用范围

作用：
服务器可以利用Cookies或Session包含信息的任意du性来筛选并经常性维护这些信息，以判断在HTTP传输中的状态。
它们dao最典型的应用是判定注册用户是否已经登录网站，用户可能会得到提示，是否在下一次进入此网站时保留用户信息以便简化登录手续。
另一个重要应用场合是“购物车”之类处理。用户可能会在一段时间内在同一家网站的不同页面中选择不同的商品，
这些信息都会写入Cookies或Session，以便在最后付款时提取信息。总而言之，cookies和session就是能够记录顾客状态的技术，
尽管二者属于不同的技术，但只要cookies能做到的，session也能做到

cookie和session的区别：
1，session存放在服务器端，cookie存放在客户端。

2，session会随着会话的结束而关闭，cookie则存放在客户端浏览器上长期有效。

3，session保存的是对象，cookie保存的是字符串

4，存放在cookie里的信息容易泄露，通常只保存不重要的信息，重要的信息放在session中

应用范围：cookie是在新用户的的时候，而session是在选哟记录信息的时候，安全的要求较高的时候

3. 依赖注入的方式有几种，各是什么？
    3种，属性注入，构造器注入，set注入
4. Spring 由哪些模块组成?
   7个模块组成
Spring-Core：
Core包是框架的最基础部分，并提供依赖注入（Dependency Injection）管理Bean容器功能。这里的基础概念是BeanFactory，它提供对Factory模式的经典实现来消除对程序性单例模式的需要，并真正地允许你从程序逻辑中分离出依赖关系和配置。

Spring-Context：(Spring核心容器<上下文模块>)
核心模块的BeanFactory使Spring成为一个容器，而上下文模块使它成为一个框架。这个模块扩展了BeanFactory的概念，增加了消息、事件传播以及验证的支持。另外，这个模块提供了许多企业服务，例如电子邮件、JNDI访问、EJB集成、远程以及时序调度（scheduling）服务。也包括了对模版框架例如Velocity和FreeMarker集成的支持。

Spring-Aop：
Spring在它的AOP模块中提供了对面向切面编程的丰富支持。例如方法拦截器（servletListener ,controller....）和切点，可以有效的防止代码上功能的耦合，这个模块是在Spring应用中实现切面编程的基础。Spring的AOP模块也将元数据编程引入了Spring。使用Spring的元数据支持，你可以为你的源代码增加注释，指示Spring在何处以及如何应用切面函数。

Spring-Dao:
使用JDBC经常导致大量的重复代码，取得连接、创建语句、处理结果集，然后关闭连接、旧代码中迁移自定义工具类JDBCUtil 也让开发变得繁琐。Spring的Dao模块对传统的JDBC进行了抽象，还提供了一种比编程性更好的声明性事务管理方法。

Spring-Web:
Web上下文模块建立于应用上下文模块之上，提供了WEB开发的基础集成特性，例如文件上传。另外，这个模块还提供了一些面向服务支持。利用Servlet listeners进行IOC容器初始化和针对Web的applicationcontext。

Spring Web MVC:
(Model-View-Controller)Spring为构建Web应用提供了一个功能全面的MVC框架。它提供了一种清晰的分离模型，在领域模型代码和web form之间。并且，还可以借助Spring框架的其他特性。

Spring-ORM:
关系映射模块，ORM包为流行的“关系/对象”映射APIs提供了集成层，包括JDO，Hibernate和iBatis（MyBatis）。通过ORM包，可以混合使用所有Spring提供的特性进行“对象/关系”映射，方便开发时小组内整合代码。
	
5. IOC 的优点是什么？
	Spring IoC（Inversion of Control 控制反转）/DI（Dependecy Injection 依赖注入），作用是，协调各组件间相互的依赖关系，同时大大提高了组件的可移植性。

6. ApplicationContext 通常的实现是什么?
	实现类
	ClassPathXmlApplicationContext：spring使用XML配置文件注册组件，XML文件必须放在类路径下
	FileSystemXmlApplicationContext：spirng使用XML配置文件注册组件，可以指定相对路径和绝对路径，查看XML文件
	XmlWebApplicationContext：spring使用XML配置文件注册组件，根据WEB应用程序的部署路径查找XML文件
	AnnotationConfigApplicationContext：spring使用配置类和注解注册组件
	
7. Spring 有几种配置方式？
	基于xml
	基于注解配置
	基于Java
	
8.  Spring 框架中都用到了哪些设计模式？
	工厂设计模式 : Spring使用工厂模式通过 BeanFactory、ApplicationContext 创建 bean 对象。
	代理设计模式 : Spring AOP 功能的实现。
	单例设计模式 : Spring 中的 Bean 默认都是单例的
9. BeanFactory 接口和 ApplicationContext 接口有什么区别 ？
 ApplicationContext是BeanFactory的子接口，从容器中取出的组件，为立即加载方式。提供配置类、注解等多种方式，实例化Spring容器中管理的组件，并协作对象间的关联关系。

10. BeanFactory和FactoryBean的区别？
eanFactory是一个factory，是spring的IOC的工场，而FactoryBean是个bean，它们两个只是名字很相似。

BeanFactory是一个IOC工场，用于管理和创建Bean，它是IOC最基本的接口，为其他的IOC工场提供规范，很多其他的spring容器都实现了它，如ApplicationContext、XMLBeanFactory等。它提供了通过bean的名字获取实例、判断bean是否在工场中、判断是否为单例等方法。

FactoryBean是一个bean，也是一个接口。该接口中定义了三个方法，getObject()，返回实现了FactoryBean接口的类创建的bean实例，getObjectType返回该实例的类型，isSingleton判断该实例是否为单例。当一个类实现了FactoryBean后，如ABean实现了FactoryBean，通过容器使用getBean(String beanName),即applicationContext.getBean(“aBean”)返回的不是aBean对象，而是aBean实现getObject中生成的实例。