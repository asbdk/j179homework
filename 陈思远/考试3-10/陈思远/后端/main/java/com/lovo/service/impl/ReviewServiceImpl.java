package com.lovo.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.ReviewBean;
import com.lovo.dao.IReviewDao;
import com.lovo.service.IReviewService;
import com.lovo.util.RedisUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ReviewServiceImpl extends ServiceImpl<IReviewDao, ReviewBean> implements IReviewService {
    @Resource
    private IReviewDao reviewDao;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public List selectAll() {
        return reviewDao.selectAll();
    }

    @Override
    public void updateScore(int id, int num) {
        ReviewBean reviewBean = reviewDao.selectById(id);
        reviewDao.updateScore(id,reviewBean.getReviewScore()+num);
    }

    @Override
    public void addUserReview(int userId, int reviewId) {
        reviewDao.addUserReview(userId,reviewId);
    }

    @Override
    public void delUserReview(int userId, int reviewId) {
        reviewDao.delUserReview(userId,reviewId);
    }

}
