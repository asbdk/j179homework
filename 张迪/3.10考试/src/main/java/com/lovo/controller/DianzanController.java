package com.lovo.controller;

import com.lovo.result.ResponseResult;
import com.lovo.result.UserValid;
import com.lovo.service.IDianzanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/dianzan")
@ResponseResult
@Api("点评控制器")
public class DianzanController {
    @Resource
    private IDianzanService service;

    @ApiOperation(value = "查看所有评论")
    @GetMapping
    @UserValid
    public Object showAll(){
        return service.selectAll();
    }

    @PostMapping("up")
    @ApiOperation(value = "点赞")
    public Object updateDian(int dianzanId, int userId){
        service.updateScore(dianzanId,1);
        service.addUserReview(userId,dianzanId);
        return "ok";
    }
    @PostMapping("down")
    @ApiOperation(value = "取消点赞")
    public Object downDian(int dianzanId, int userId){
        service.updateScore(dianzanId,-1);
        service.delUserReview(userId,dianzanId);
        return "ok";
    }

}
