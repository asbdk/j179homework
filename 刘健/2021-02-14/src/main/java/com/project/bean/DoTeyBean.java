package com.project.bean;
/**电子宠物实体类*/
public class DoTeyBean {
    /**宠物id*/
    private int doId;
    /**宠物昵称*/
    private String doName;
    /**宠物性别*/
    private String doSex;
    /**宠物力量*/
    private int doStrength;
    /**宠物智力*/
    private  int doCute;
    /**宠物爱心值*/
    private int doLove;
    /**宠物介绍*/
    private String doIntro;
    /**宠物图片*/
    private String doPic;
    /**宠物类型*/
    private String doType;
    /**宠物主外键*/
    private int userId;

    public DoTeyBean() {
    }

    public DoTeyBean(String doName, String doSex, int doStrength, int doCute,
                     int doLove, String doIntro, String doPic, String doType) {
        this.doName = doName;
        this.doSex = doSex;
        this.doStrength = doStrength;
        this.doCute = doCute;
        this.doLove = doLove;
        this.doIntro = doIntro;
        this.doPic = doPic;
        this.doType = doType;
    }

    public int getDoId() {
        return doId;
    }

    public void setDoId(int doId) {
        this.doId = doId;
    }

    public String getDoName() {
        return doName;
    }

    public void setDoName(String doName) {
        this.doName = doName;
    }

    public String getDoSex() {
        return doSex;
    }

    public void setDoSex(String doSex) {
        this.doSex = doSex;
    }

    public int getDoStrength() {
        return doStrength;
    }

    public void setDoStrength(int doStrength) {
        this.doStrength = doStrength;
    }

    public int getDoCute() {
        return doCute;
    }

    public void setDoCute(int doCute) {
        this.doCute = doCute;
    }

    public int getDoLove() {
        return doLove;
    }

    public void setDoLove(int doLove) {
        this.doLove = doLove;
    }

    public String getDoIntro() {
        return doIntro;
    }

    public void setDoIntro(String doIntro) {
        this.doIntro = doIntro;
    }

    public String getDoPic() {
        return doPic;
    }

    public void setDoPic(String doPic) {
        this.doPic = doPic;
    }

    public String getDoType() {
        return doType;
    }

    public void setDoType(String doType) {
        this.doType = doType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "DoTeyBean{" +
                "doId=" + doId +
                ", doName='" + doName + '\'' +
                ", doSex='" + doSex + '\'' +
                ", doStrength=" + doStrength +
                ", doCute=" + doCute +
                ", doLove=" + doLove +
                ", doIntro='" + doIntro + '\'' +
                ", doPic='" + doPic + '\'' +
                ", doType='" + doType + '\'' +
                '}';
    }
}
