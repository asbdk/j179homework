package com.lax.dao;

import com.lax.bean.AdminBean;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IAdminDAO extends BaseMapper<AdminBean> {
    AdminBean findByName(@Param("name") String name);

    List<AdminBean> findCommentZan(@Param("id") int id);

}
