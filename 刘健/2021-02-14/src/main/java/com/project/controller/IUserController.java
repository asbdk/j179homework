package com.project.controller;

import com.project.bean.UserBean;
import com.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class IUserController {
    @Autowired
    private UserService service;

    @RequestMapping("login")
    public UserBean login(String name,String pwd){
        return service.login(name, pwd);
    }

    @RequestMapping("add")
    public void add(UserBean user){
        service.add(user);
    }

}
