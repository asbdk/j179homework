package com.project.controller;

import com.github.pagehelper.PageInfo;
import com.project.bean.CommendBean;
import com.project.bean.UserBean;
import com.project.service.ICommendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/commend")
@Api("评价业务接口")
public class CommendController {
    @Resource
    private ICommendService commendService;

    @GetMapping("/getAll")
    public PageInfo<CommendBean> getCommend(Integer pageNO,Integer size){
       return commendService.findAll(pageNO, size);
    }

    @ApiOperation(value = "判断是否点赞",notes = "判断当前用户是否对某评价点赞")
    @GetMapping("/checkIsZan")
    public Object checkIsZan(Long commendId){
        UserBean userBean = (UserBean) SecurityUtils.getSubject().getPrincipal();
        Long userId = userBean.getId();
        String flag = "no";
        try{
            if(commendService.checkIsZan(userId,commendId)){
                flag = "ok";
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "ok";
    }

    @GetMapping("/getTotal")
    public List<CommendBean> getTotal(){
        return commendService.findTotal();
    }
}
