import axios from 'axios'
import qs from 'qs'
export const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}

export const login = async (userName,userPass) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/project/users/login",
        data:qs.stringify({
            userName,userPass
        })
    })
    return response.data;
}
export const showAll = async (page=1,size=5) => {
    console.log(page,size)
    let response = await axios({
        method:"get",
        url:"/project/users",
        params:{page,size}
    })
    return response.data;
}
export const getUser = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/project/users/token"
    })
    return response.data;
}
export const refreshToken = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/project/users/refreshToken"
    })
    return response.data;
}

export const getGreat = async (tokuserId, articleId) => {
    
    let response = await axios({
        headers,
        method:"post",
        url:"/project/users/getGreat",
        data:{
            tokuserId, articleId
        }
    })
    return response.data;
}
