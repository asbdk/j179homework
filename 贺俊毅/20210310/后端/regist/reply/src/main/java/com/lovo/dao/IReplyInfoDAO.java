package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.ReplyInfoBean;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
public interface IReplyInfoDAO extends BaseMapper<ReplyInfoBean> {
    @Select("SELECT `reply_info`.*,`user_info`.`user_name`,COUNT(`userAndReply_info`.`userAndReply_id`) replyNum FROM reply_info LEFT JOIN userAndReply_info  ON `userAndReply_info`.`fk_replyid` = `reply_info`.`reply_id`" +
            "JOIN user_info ON `user_info`.`user_id`=`reply_info`.`fk_userid`" +
            " GROUP BY `reply_info`.`reply_id`")
    @ResultMap("replyMap")
    List<ReplyInfoBean> selectAll();
    void add(ReplyInfoBean replyInfoBean);

    ReplyInfoBean selectLike(@Param("replyId") Long replyId,@Param("userId") Long userId);
}
