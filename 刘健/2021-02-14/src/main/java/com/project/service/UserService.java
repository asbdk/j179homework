package com.project.service;

import com.project.bean.UserBean;

/**
 * 宠物主业务接口
 */
public interface UserService {

    /**
     * 用户登录方法
     * @param name 用户id
     * @param pwd 用户密码
     * @return 返回登录用户对象
     */
    public UserBean login(String name,String pwd);

    /**
     * 注册添加用户
     * @param user 用户对象
     */
    public void add(UserBean user);



}
