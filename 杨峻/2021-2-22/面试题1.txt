java基础
1、请用文字描述super()和this()的区别
This指当前对象，this()表示在一个构造方法中调用本类另一个构造方法
Super指父类对象，super()表示子类构造方法中调用父类构造方法，产生父类对象

2、什么是java序列化，如何实现java序列化。
在进行对象数据传输时，由于对象数据庞大，无法直接传输。在传输前，需要将对象打散成字节序列，以利于传输，这个过程称为序列化过程。
到达目的地后，又需要将字节序列还原成对象，这个过程称为反序列化过程。
实现序列化接口Serializable

3、Overload(重载)和Override（重写）的区别，Overloaded的方法是否可以改变返回值的类型？
重载发生在同一个类中，两个方法的方法名相同，参数列表不同（参数类型、个数、顺序）和返回类型无关。调用时根据实参来决定调用哪个重载方法。
重写发生在父类和子类之间，子类方法的方法名和父类相同，参数列表相同，返回类型也相同，调用方法时，优先调用子类重写的方法。


4、请指出下列代码中的问题
package com.aplus.interview;
public abstract class InterViewClass1{
	public final abstract int thMethod1(int para1);
}
final和abstract不能同时使用，因为修饰方法时final不允许重写，abstract允许重写



5、有字符串为1234567890，请用最简短的代码将此字符串倒序输出到控制台。
String str = "1234567890";
String str1 = "";
for (int i = str.length() - 1; i >= 0; i--) {
       str1 = str1 + str.charAt(i);
}
System.out.println(str1);

6、请问这个程序会有下面哪种结果？
public class Cygnus{
	static int value = 9;
	public static void main(String[] args) throws Exception{
		new Cygnus().printValue();
	}
	private void printValue(){
		int value = 69;
		System.out.println(this.value);
	}
}
A、编译错误    B、打印9    C、打印69  D、运行时抛出异常
B

7、请问NullPointerException，RuntimeException各是什么异常，有什么异同？
NullPointerException是空指针异常，当对null值进行属性或方法调用时抛出
RuntimeException是运行时异常，NullPointerException就是RuntimeException中的一种



8、请问&和&&的区别
&是位运算符，在判断语句中，两边都会运算
&&是逻辑运算符，在判断语句中，如果左侧为false，右侧则不会运算


9、在old.txt中，乱序排列着abcdefg等字符，请用代码描述使用文件读写流，把old.txt中的内容写入到new.txt中。
old.txt        
bcdafge

new.txt
abcdefg

Reader r = null;
        BufferedReader br = null;
        Writer w = null;
        BufferedWriter wr = null;

        try {
            r = new FileReader("old.txt");
            br = new BufferedReader(r);

            w = new FileWriter("new.txt");
            wr = new BufferedWriter(w);

            String str = "";
            String str1 = "";
            while ((str = br.readLine()) != null){
                str1 += str;
            }
            char[] arrayCh = str1 .toCharArray();
            Arrays.sort(arrayCh);
            String sortedStr=new String(arrayCh);
            System.out.println(sortedStr);
            wr.write(sortedStr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
                r.close();
                wr.close();
                w.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }




10、请用简短语言描述用jdbc连接mysql数据库，查询某表A的所有数据，并打印第一列的数据到控制台
1、加载驱动
2、建立连接
3、执行SQL语句
4、关闭连接

SELECT * FROM A LIMIT 1
在映射中只映射第一列的值




11、war包是什么，ear包是什么，他们之间是什么关系，请大体指出war包的目录结构
war包是Sun提出的一种web应用程序格式，与jar类似，是很多文件的压缩包。
war
——index.html
——WEB-INF
————web.xml
————classes
	……
————lib
	……

ear包括war


12、ArrayList和LinkedList的区别。
ArrayList：底层采用数组实现，遍历元素、追加元素速度快，中间插入和删除元素速度慢，线程不安全。
LinkedList：采用双向链表实现，元素空间不连续。每个元素除存放数据外，还要存放上一个和下一个元素的引用。遍历元素速度慢，中间插入和删除元素速度快。


java web
1、实现一个servlet从编码到部署，需要做哪些工作，请简要描述。
编写配置文件，编写前后端文件，启动服务器


2、sendRedirect()和forward()有什么不同
1、重定向客户端向服务器发出两次请求，请求转发向客户端发出一次请求。因此，请求转发可以共享request中绑定的共享数据，而重定向不行。
2、请求转发只能转发服务器本地的资源，而重定向可以在第二次请求时访问别的服务器
3、请求转发由于客户端只发出一次请求，浏览器地址不发生变化。而重定向会将浏览器地址栏中的内容更新为第二次请求的路径时


3、简述spring的加载机制
applicationContext
Spring先根据配置文件进行初始化的处理
扫描包、类、组件
	
4、简要描述spring中声明事务配置过程。
1、配置事务管理器
2、配置事务的通知
3、配置AOP通用切入点表达式
4、建立事务通知和切入点表达式的对应关系
5、配置事务的属性

5、使用javascript向网页输出<h1>hello</h1>，以下代码可行的是（）
A、<script>
	document.write(<h1>hello</h1>);
</script>
B、<script>
	document.write("<h1>hello</h1>");
</script>
C、<script>
<h1>hello</h1>
</script>
D、<script>
	document.write("hello");
</script>
B

6、描述servlet生命周期。
1、容器加载并实例化Servlet
2、调用init方法完成初始化
3、当请求到达，调用service处理请求，产生响应
4、销毁阶段，调用destroy()，完成资源清理

7、servlet、filter、listener加载顺序
listener->Filter->servlet


8、为一个图片设置背景图像后，设置背景图像在纵向和横向上平铺，下面哪个是实现该功能的？
A、no-repeat   B、repeat    C、repeat-x    D、repeat-y
A

数据库
1、表名t_User
userName   tel   	content   birthday
张三  13333663366   大专毕业  2006-10-11
张三  13612312331   本科毕业  2006-10-15
张四  021-55665566  中专毕业  2006-10-15
a)有一新记录（小王 13254748547  高中毕业  2007-05-06）请用SQL语句新增至表中
INSERT INTO t_User(userName,tel,content,birthday) VALUES (小王,13254748547,高中毕业,2007-05-06)


b)请用sql语句把张三的手机号更新13709092345
UPDATE t_User SET tel = '13709092345' WHERE userName = '张三'

c)请写出删除名为张四的全部记录的SQL
DELETE FROM t_User WHERE userName = '张四'


2、请写出下列要求的SQL
表名Sates
id		district		bookName		amount
1		西南			词典1			0
2		西北			书籍1			12
3		西南			杂志2			23
请书写降序排列各地区销售情况，结果要求展现district字段
SELECT district FROM States ORDER BY amount DESC


3、什么是主键，什么是外键，什么是索引，各有什么作用？
主键是数据库表中，每条记录的唯一标识，用于区分不同的实体，不能为空，不能重复。
外键是用来表达表和表之间关联关系的列。
索引是对数据库表中一个或多个列的值进行排序的结构,使用索引可快速访问数据库表中的特定信息。


4、递归考核：
数据库中，有一个员工关联关系表：
id		pid		name
1		0		刘备
2		0		曹操
3		1		关羽
4		2		张辽
5		3		关平
……
分别表示：员工编号、上级领导编号、姓名。
把它从数据库查询出来，递归成如下树结构：（使用java查询数据库递归实现）
const list = [{
	id:1,
	name:'刘备',
	children:[
		{
			id:3,
			name:'关羽'
			childern:[……]
		},
		……
	]
},……]