package com.project.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.project.bean.AdminInfoBean;
import com.project.dao.IAdminInfoDAO;
import com.project.service.IAdminInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.util.RedisUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class AdminInfoServiceImpl extends ServiceImpl<IAdminInfoDAO, AdminInfoBean> implements IAdminInfoService {
    @Resource
    IAdminInfoDAO dao;
    @Resource
    private RedisUtil redisUtil;
    @Override
    public AdminInfoBean findByName(String name) {
        return dao.findByName(name);
    }

    @Override
    public Object selectAll(Integer page, Integer size) {
        long start = System.currentTimeMillis();
        // 从缓存中获取数据
        Object obj = redisUtil.get("selectAllUser_"+page+"_"+size);
        if(obj == null){
            // 如果缓存中没有数据，从数据库获取，并加入到缓存
            IPage pageObject = new Page(page,size);
            obj = dao.selectPage(pageObject,null);
            redisUtil.set("selectAllUser_"+page+"_"+size,obj,60 * 60);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        return obj;
    }

}
