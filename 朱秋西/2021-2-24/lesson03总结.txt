1、Spring Boot是什么？
是一个全新的框架，一种全新的编程规则，其设计目的是用来简化新spring应用初始搭建以及开发过程。该框架使用了特定的方式来配置从经行配置，从而使开发人员不在需要定义样板配置，spring Boot 其实就是很多插件的组件（框架），内嵌了使用工具（比如内嵌Tomcat，Jetty等），方便开发人员快速和开发的一个框架
2、Spring Boot有哪些优点？
搭建项目速度快，测试变得简单
3、Spring Boot自动配置的原理是什么？
主要是springboot启动类上的核心springBootApplication注释主配置类，有了这个主配置类启动时就会为SPringBoot开启一个
1.从配置文件metainf/spring.factores加载可能用到的自动配置类
2.去重，并将exclude和excludeName属性携带的类排除
3.过滤，将满足条件（@conditional）的自动配置类返回

4、介绍下Spring Boot配置的加载顺序。
1、使用 @Value 注解直接注入对应的值，这能获取到 Spring 中 Environment 的值；
2、使用 @ConfigurationProperties 注解把对应的值绑定到一个对象；
3、直接获取注入 Environment 进行获取；

配置属性的方式很多，Spring boot使用了一种独有的 PropertySource 可以很方便的覆盖属性的值。
5、YAML配置比起Properties配置的优势在哪里？
配置有序。
支持数组，数组中的元素可以是基本数据类型也可以是对象。
相比properties配置文件，YAML还有一个缺点，就是不支持@PropertySource注解导入自定义的YAML配置。
6、如何实现Spring Boot的热部署
1.使用springloaded配置pom.xml文件，使用mvn spring-boot：run启动
2.使用springloaded本地加载启动，配置jvm参数-javaagent：<jar包地址>- noverify
3.使用devtool工具包，操作简单，但是每次需要重新部署

7、Spring Boot的核心注解有哪些？
1.@springBootConfiguration:它组合了configuration注解文件的功能
2.@enableAutoconfiguration：打开自动配置功能，也可以关闭某个指定的自动配置选项如关闭数据源自动配置功能：@springBootApplication（exclude={DataSourceAutoConfiguration.class}）。
3.@ComponentScan：spring扫描组件。

8、介绍下Spring Boot中的监视器
 	spring boot actuator是 spring 启动框架中的重要功能之一。spring boot 监视器可帮助你访问生产环境中正在运行的应用程序的当前状态。有几个指标必须在生产中进行检查和监控。即使一些外部应用程序可能正在使用这些服务来向相关人员触发警报信息。监视器模块公布了一组可以直接作为HTTP URL 访问的PEST 端点来检查状态。

9、什么是 Spring Boot Stater ？
	starte可以理解成pom配置了一堆jar组合的空movaen项目，用来简化mavaen依赖配置，starter可以来继承也可以依赖starter。
10、Spring 和 Spring Boot 有什么不同？
	spring框架为开发Java应用程序提供了全面的基础架构支持
	spring boot基本是spring框架的扩展它消除了设置spring应用程序所需的复杂例行配置。将各种配置和库进行整合，很大程度上减少了各种配置