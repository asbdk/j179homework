import { showAll } from "../service/ajax";
// import ajax  from "../service/ajax.js"
export default{
    namespaced:true, //允许使用命名空间

    state:{ //状态 （单一数据源）
       commentObj:{},
       pagination:{},
    },

    mutations:{ // mutations更改状态只能采用同步（规范）
        setCommentObj(state,obj) {
            state.commentObj = obj;
        },
        setPagination(state,pagination){
            state.pagination = pagination;
        }
    },

    actions:{
        async setComment({commit},obj={}){   
            let data = await showAll(obj.pageNO,obj.size);

            commit("setCommentObj",data.data.records);
            commit("setPagination",data.data);
        }
    }

}