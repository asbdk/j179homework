package com.project.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;

/**
 * 上传文件
 */
public class UploadUtil {
    public static String upload(MultipartFile mf, String dirPath){
        // 得到上传文件名
        String fileName = mf.getOriginalFilename();
        try {
            fileName = System.currentTimeMillis() + fileName.substring(fileName.lastIndexOf("."));
            //得到上传文件存放目录的真实路径
            URL url = Thread.currentThread().getContextClassLoader()
                    .getResources(dirPath).nextElement();
            String filePath = URLDecoder.decode(url.getFile(),"utf-8");
            //将上传文件的二进制数据，写入指定的文件
            mf.transferTo(new File(filePath +"/"+fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }
}