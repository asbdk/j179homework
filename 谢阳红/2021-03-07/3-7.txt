1、认证 (Authentication) 和授权 (Authorization)的区别是什么？
	认证是关于验证的凭据，用户名，用户密码，以验证用户身份
	授权发生在系统成功验证用户身份后，根据用户拥有权限终会授予用户访问的资源

2、什么是Cookie ? Cookie的作⽤是什么?如何在服务端使⽤ Cookie ?
	Cookie 是个存储在浏览器目录的文本文件，用来记录用户的一些信息，可以在发送请求时从中取出信息作为状态跟踪传给服务器，
	服务器用sid于服务器中对比，判断用户的状态

3、如果没有Cookie的话Session还能⽤吗？
	服务器存储 session 的 sessionid 是通过 cookie 存到浏览器里。
	如果浏览器禁用了 cookie，浏览器请求服务器无法携带 sessionid，服务器无法识别请求中的用户身份，session失效。
	但是可以通过其他方法在禁用 cookie 的情况下，可以继续使用session。
	通过url重写，把 sessionid 作为参数追加的原 url 中，后续的浏览器与服务器交互中携带 sessionid 参数。
	
4、为什么Cookie ⽆法防⽌CSRF攻击，⽽token可以？
	CSRF攻击的全称是跨站请求伪造。
	在服务器中储存着session对象的表，sid和对象是以键值对的方式一个储存在服务器，一个储存在浏览器的cookies中，
	在恶意网站的HTML或者JS代码中会让你去访问其它网站，你的浏览器在访问网站的时候会自己携带该网站的Cookie去请求，然而网站对于用户的认证信息都在Cookie里，所以容易被伪造挟制
	而token，在信任网站的HTML或js中，会向服务器传递参数token，不是通过Cookie传递的，若恶意网站要伪造用户的请求，也必须伪造这个token，否则用户身份验证不通过
	但是，同源策略限制了恶意网站不能拿到信任网站的Cookie内容，只能使用，所以就算是token是存放在Cookie中的，恶意网站也无法提取出Cookie中的token数据进行伪造

5、什么是OAuth 2.0？
	OAuth是一个关于授权（authorization）的开放网络标准，
	OAuth在客户端与服务提供商之间，设置了一个授权层（authorization layer）
	客户端不能直接登录服务提供商，只能登录授权层
	客户端登录授权层以后，"服务提供商"根据令牌的权限范围和有效期，向"客户端"开放用户储存的资料。

6、什么是SSO？
	SSO是一种统一认证和授权机制，指访问同一服务器不同应用中的受保护资源的同一用户，只需要登录一次，
	即通过一个应用中的安全验证后，再访问其他应用中的受保护资源时，不再需要重新登录验证。

7、描述Spring Security 和 Shiro 各自的优缺点 ?
	Spring Security是一个能够为基于Spring的企业应用系统提供声明式的安全访问控制解决方案的安全框架。
	Shiro是一个强大且易用的Java安全框架，能够非常清晰的处理身份验证、授权、管理会话以及密码加密。

8、解释下粗粒度权限控制和细粒度权限控制。
	粗粒度权限管理，对资源类型的权限管理。资源类型比如：菜单、url连接、用户添加页面、用户信息、类方法、页面中按钮
	细粒度权限管理，对资源实例的权限管理。资源实例就资源类型的具体化，比如：用户id为001的修改连接，1110班的用户信息、行政部的员工。
	

9、Shiro的核心模块有哪些？
	Subject（主体）与当前应用交互的任何东西都是Subject，与Subject的所有交互都会委托给SecurityManager来执行
	SecurityManager（安全管理器）所有与安全有关的操作都会与SecurityManager进行交互，并且SecurityManager管理者所有的Subject
	Realm：Shiro从Realm获取安全数据（用户、角色、权限），就是说SecurityManager要验证用户身份，那么它需要从Realm获取相应的用户进行比较以确定用户身份是否合法，也需要从Realm获取用户的角色\权限来判断用户是否能进行一系列操作

10、Shiro内置的过滤器有哪些？分别描述下它们的用法。
	认证过滤器：anon(不认证也可以访问)，authcBasic, authc(必须认证后才可访问),user
	授权过滤器：perms（指定资源需要哪些权限才可以访问），Roles, ssl,rest, port

