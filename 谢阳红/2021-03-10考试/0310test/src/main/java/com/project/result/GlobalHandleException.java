package com.project.result;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
@NoArgsConstructor
@Data
public class GlobalHandleException extends Throwable {
    private com.project.result.ResultCode resultCode;

    public GlobalHandleException(com.project.result.ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public com.project.result.Result handleMethodArgumentNotValid(MethodArgumentNotValidException exception) {
        com.project.result.Result resultVO = com.project.result.Result.fail(com.project.result.ResultCode.PARAM_IS_INVALID);
        return resultVO;
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public com.project.result.Result handleConstraintViolation(ConstraintViolationException exception) {
        com.project.result.Result resultVO = com.project.result.Result.fail(com.project.result.ResultCode.PARAM_IS_INVALID);
        return resultVO;
    }
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(UnauthorizedException.class)
    public com.project.result.Result handleUnauthorized(UnauthorizedException exception) {
        com.project.result.Result resultVO = Result.fail(ResultCode.USER_AUTHORIZATION_ERROR);
        return resultVO;
    }

}

