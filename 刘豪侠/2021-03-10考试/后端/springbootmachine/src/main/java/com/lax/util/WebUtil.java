package com.lax.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lax.bean.AdminBean;
import com.lax.result.GlobalHandleException;
import com.lax.result.Result;
import com.lax.service.IAdminService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

import javax.servlet.ServletResponse;
import java.io.IOException;

public class WebUtil {
    public static void writeValueAsString(ServletResponse response, Result result) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().print(mapper.writeValueAsString(result));
    }

    /**
     * 验证token的有效性
     * @param token
     * @param userInfoService
     * @return token对应的用户对象，如果为null表示验证失败
     * @throws IOException
     */
    public static AdminBean verifyToken(String token, IAdminService userInfoService) throws GlobalHandleException {
        if(token == null){
            return null;
        }
        String userName = null;
        try {
            userName = JWTUtil.getAudience(token);
        } catch (GlobalHandleException e) {
            return null;
        }
        AdminBean userInfoBean = userInfoService.findByName(userName);
        if(userInfoBean == null){
            return null;
        }
        try {
            JWTUtil.verifyToken(token,userInfoBean.getAdminPwd());
        } catch (GlobalHandleException e) {
            return null;
        }

        return userInfoBean;
    }

    /**
     * 封装请求信息对象
     * @return
     */
    public static HttpEntity httpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
        headers.add("accept","application/json");
        HttpEntity<HttpHeaders> request = new HttpEntity<>(headers);
        return request;
    }


}
