import Vue from "vue";
import Vuex from "vuex"; // 导入Vuex
import user from "./user.js"
import discuss from "./discuss.js"



Vue.use(Vuex); //应用Vuex


export default new Vuex.Store({
    //引入子模块
    modules:{
        user,discuss
    },
    //定义共享数据，主要用于显示
    state:{
        num:0
    },
    getters:{   //定义计算属性
        computNum(state){
            return "$"+state.num;
        }
    },
    mutations:{
        addNum(state){
            state.num ++;
        }
    }
});