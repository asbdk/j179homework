package com.lovo.controller;


import com.lovo.result.ResponseResult;
import com.lovo.service.ICommentsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/comments")
@ResponseResult
public class CommentsController {
    @Resource
    private ICommentsService service;

    @GetMapping
    public Object findAll(){
        System.out.println(service.findAll());
        return service.findAll();
    }
}

