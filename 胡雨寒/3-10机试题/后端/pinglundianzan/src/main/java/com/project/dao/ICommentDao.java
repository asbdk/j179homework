package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.CommentBean;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ICommentDao extends BaseMapper<CommentBean> {
    public List<CommentBean> selectAll();
    void updateAdd(@Param("uuId") Long id, @Param("coId") Long num);
    void updateDel(@Param("uuId") Long id,@Param("coId") Long num);
    Object selectByIdNum(@Param("uuId") Long id,@Param("coId") Long num);
}
