package com.project.service.impl;

import com.project.bean.UserCommentBean;
import com.project.dao.IUserCommentDAO;
import com.project.service.IUserCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
@Service
public class UserCommentServiceImpl extends ServiceImpl<IUserCommentDAO, UserCommentBean> implements IUserCommentService {

}
