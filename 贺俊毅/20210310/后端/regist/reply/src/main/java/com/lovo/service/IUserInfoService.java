package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.UserInfoBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
public interface IUserInfoService extends IService<UserInfoBean> {
    UserInfoBean login(String userName,String pwd);
    void like(Long userId,Long replyId);
}
