package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.UserCommentBean;

public interface IUserCommentService extends IService<UserCommentBean> {
    void addUserComment(UserCommentBean userCommentBean);

    void delUserComment(Integer id);
}
