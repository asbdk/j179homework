import Vue from "vue"
import VueRouter from "vue-router"
import login from "../replyPages/login.vue"
import reply from "../replyPages/reply.vue"

Vue.use(VueRouter)

const routes = [
    {path:'/login',component:login},
    {path:'/reply',component:reply},


]
export default new VueRouter({
    mode:'history',
    routes
})