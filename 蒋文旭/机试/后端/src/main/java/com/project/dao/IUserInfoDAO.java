package com.project.dao;

import com.project.bean.UserInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    UserInfoBean selectByUserName(String userName);
}
