package com.project.util;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.io.Reader;
import java.time.LocalDate;

@Component
public class DateChange implements Converter<String, LocalDate> {

    @Override
    public LocalDate convert(String s) {
        if (s != null && s.matches("\\d{4}-\\d{2}-\\d{2}")){
            return LocalDate.parse(s);
        }
       return null;
    }

}
