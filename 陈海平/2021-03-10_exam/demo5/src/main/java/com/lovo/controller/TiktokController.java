package com.lovo.controller;


import com.lovo.bean.TiktokBean;
import com.lovo.service.ITiktokService;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/tiktok-bean")
public class TiktokController {
    @Resource
    private ITiktokService service;
    public String add(TiktokBean tiktokBean){
        service.save(tiktokBean);
        return "ok";
    }
    public String del(TiktokBean tiktokBean){
        service.removeById(tiktokBean);
        return "ok";
    }
}

