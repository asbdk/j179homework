package com.lovo.controller;


import com.lovo.bean.CommentsBean;
import com.lovo.bean.UserBean;
import com.lovo.dao.IUserDAO;
import com.lovo.result.ResponseResult;
import com.lovo.service.ICommentsService;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/comment")
@ResponseResult
public class CommentsController {
    @Resource
    private ICommentsService service;
//    @Resource
//    private UserController user;

    @RequestMapping("star")
    public Object changeStar(Long userId,Long cId, Integer num){
        service.changeStar(userId,cId,num);
        return "ok";
    }

    @RequestMapping("list")
    public Object list(){
        List<CommentsBean> cList =service.list();
        UserBean userBean =(UserBean) SecurityUtils.getSubject().getPrincipal();
        List<Long> userList = userBean.getCommentsList();
        for (CommentsBean commentsBean:cList) {
            if(userList.contains(commentsBean.getCId())){
                commentsBean.setState("yes");
            }
            commentsBean.setState("no");
        }
        return cList;
    }
    @RequestMapping("smallList")
    public Object smallList(){
        List<CommentsBean> cList =service.list();
        return cList;
    }

}

