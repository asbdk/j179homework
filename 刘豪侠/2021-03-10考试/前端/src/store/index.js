import Vue from "vue";
import Vuex from "vuex"; // 导入Vuex
import user from "../store/users.js"

Vue.use(Vuex); //应用Vuex
export default new Vuex.Store({

    //导入子模块
    modules:{
      user
    },
    
});