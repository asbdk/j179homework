java基础
1、请用文字描述super()和this()的区别
　　　答：相同点：两者都是调用构造方法，创建实体bean。
　　　不同：super（）是在子类继承父类时，在子类实例化前调用super（）先实例化一		个父类的对象。在子类的构造方法中默认第一句执行的代码就是super（），且		只能在第一句。
　　　This（）是在当前类中，带参构造方法中可以加上this（），表示优先实例化	一个无参的对象。
2、什么是java序列化，如何实现java序列化。
　　　答：java序列化是指将java对象转为二进制字节序列的过程，因为java对象较大，而数据之间交互都是以二进制字节的形式传输的，所以在对对象进行传输时就需要转化为二进制字节序列。只需要在定义类时implement Serializable就可以了。
ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("D:\\objectFile.obj"));

ObjectInputStream in = new ObjectInputStream(new FileInputStream("D:\\objectFile.obj"));

　　　

3、Overload(重载)和Override（重写）的区别，Overloaded的方法是否可以改变返回值的类型？
　　　答：重载是发生在一个类中，为了便于使用者调用方法是可以传递不同的值，所以重载是指一个类中方法名相同、参数列表不同的方法，参数不同体现在类型、个数、顺序。方法重载对返回值类型、访问修饰符、抛出异常不作要求，就是可以返回不同类型的数据。
　　　重写是发生在子类继承父类的情况下。子类对从父类继承的方法进行修改重写。方法名、及参数列表必须相同，返回值类型不能大于父类的方法的返回值类型，如果继承的方法有抛出异常，子类抛出的异常不能大于父类。权限修饰符不能小于父类
　　　
4、请指出下列代码中的问题
package com.aplus.interview;
public abstract class InterViewClass1{
	public final abstract int thMethod1(int para1);
}
答：final 和 abstract不能共用，删除final修饰符

5、有字符串为1234567890，请用最简短的代码将此字符串倒序输出到控制台。
(1)new StringBuilder(1234567890).reverse().toString();StringBuilder中专门倒序的方法，而string中没有

6、请问这个程序会有下面哪种结果？
public class Cygnus{
	static int value = 9;
	public static void main(String[] args) throws Exception{
		new Cygnus().printValue();
	}
	private void printValue(){
		int value = 69;
		System.out.println(this.value);
	}
}
A、编译错误    B、打印9    C、打印69  D、运行时抛出异常

7、请问NullPointerException，RuntimeException各是什么异常，有什么异同？
　　　答：NullPointException是空指针异常，RUNTimeException是运行时异常，前者是后者的子类

8、请问&和&&的区别
　　　答：两者都可以表示逻辑运算符“与”，&表示普通与，运算符两端的表达式都要执行，&&表示短路与，如果运算符左边表达式结果为false，就可以直接确定返回结果，而不必要在计算右边的表达式。
　　　&还可以表示位运算符，表示两个数按位作相应的计算。
9、在old.txt中，乱序排列着abcdefg等字符，请用代码描述使用文件读写流，把old.txt中的内容写入到new.txt中。
old.txt        
bcdafge

new.txt
Abcdefg

答：FileInPutStream in = new FileInPutStream(“old.txt”);
　　　FileOutPutStream out = new FileOutPutStream(“new.txt”,true)
　　　byte[] bytes = new byte[];
　　　Int len=0;
　　　While((len = in.read(bytes)) != -1){
　　　out.write(bytes,0,len);
　　　}

10、请用简短语言描述用jdbc连接mysql数据库，查询某表A的所有数据，并打印第一列的数据到控制台
答：jdbc连接数据库步骤大致是：①导入并加载驱动（class.forName(***)），②创建连接,(DriverManager.getConnection(***),如要提供数据库的端口号、数据库的名称、用户名、密码)，③接下来就是用流操作数据了，一般用prepareStatement,可以防止sql注入。

11、war包是什么，ear包是什么，他们之间是什么关系，请大体指出war包的目录结构
　　　JAR包：打成JAR包的代码，一般作为工具类，在项目中，会应用到N多JAR工具包；

	WAR包：JAVA WEB工程，都是打成WAR包，进行发布，如果我们的服务器选择TOMCAT等轻量级服务器，一般就打出WAR包进行发布；

	EAR包：这针对企业级项目的，实际上EAR包中包含WAR包和几个企业级项目的配置文件而已，一般服务器选择WebSphere等，都会使用EAR包。
　　　War包的目录结构是：①index.jsp（可有可无），②METAINF(maven自动生成的目录)，③WEB-INF:*存放class类文件，*lib目录：存放jar文件，*web.xml是对war文件的描述
12、ArrayList和LinkedList的区别。
(1)两者都实现了List接口，区别在于底层存储数据的形式不同
(2)Arraylistd底层是利用动态数组进行存储的，定义一个原始长度为10的数组，当存放的数据超过容量时，默认扩容到1.5倍。
(3)LinkedList底层是通过node节点链表实现的，有单项连表、双向连表。双向连表中每个链的节点都有三个属性，其中两个分别存放前后节点的地址，另外一个是存放需要存储的数据。
(4)各自优缺点：arrayList遍历比较快，因为数组有地址连续的特点。LinkedList适合插入、删除。不适合遍历，但删除和添加节点比较快。

java web
1、实现一个servlet从编码到部署，需要做哪些工作，请简要描述。
　　　答：servlet是web服务器和 开发者业务组件通信的标准，是由sun公司定义的一套接口。
　　　①实现接口。我们一般用的是httpServlet接口（HTTPServlet是根据http协议继承的GenenicServlet类），只需要实现具体的doget或doPost方法就好了。实现其中的service方法后，
　　　②注册servlet。需要将servlet注册到web服务器中，tomcat中有一个addServlet方法。 
2、sendRedirect()和forward()有什么不同
　　　答：sendRedirect是指发送请求到服务器后，服务器反馈后来一个新的URL地址，客户端再次发送请求去访问第二个URL地址，网页地址栏可以看到地址的变化。缺点：是多次请求、效率比较低。优点：可以转化到任何界面。
　　　Forward是指只发送一次请求。服务器内部可以转化到不同的界面。最终将结果响应给客户端，网页地址栏没有地址的变化。缺点：只能跳转到服务器内部的界面。优点：只有一次请求执行效率比较高。
3、简述spring的加载机制
   
	
4、简要描述spring中声明事务配置过程。
	答：在主配置类上要加上一个注解@EnableTransactionManagerment,表示注册的组件是可以配置事务管理的。在具体定义组件时，要增加注解@Transactional,该注解默认的事务传播行为是propagation=propagation.REQUIRED(有事务则加入，没有则新建事务)默认的事务隔离行为是isolation=isolation.READ_COMMITED(读取已经提交的数据),可以防止脏读的放生。

5、使用javascript向网页输出<h1>hello</h1>，以下代码可行的是（）
A、<script>
	document.write(<h1>hello</h1>);
</script>
B、<script>
	document.write("<h1>hello</h1>");
</script>
C、<script>
<h1>hello</h1>
</script>
D、<script>
	document.write("hello");
</script>

6、描述servlet生命周期。
①容器的加载并实例化Servlet
②调用init方法完成初始化
③当请求到达后，调用service方法，处理请求产生响应
④销毁阶段，调用destroy完成数据的清理，其中一个周期中124只执行一次，3可以多次执行。

7、servlet、filter、listener加载顺序

8、为一个图片设置背景图像后，设置背景图像在纵向和横向上平铺，下面哪个是实现该功能的？B
A、no-repeat   B、repeat    C、repeat-x    D、repeat-y

数据库
1、表名t_User
userName   tel   	content   birthday
张三  13333663366   大专毕业  2006-10-11
张三  13612312331   本科毕业  2006-10-15
张四  021-55665566  中专毕业  2006-10-15
a)有一新记录（小王 13254748547  高中毕业  2007-05-06）请用SQL语句新增至表中

Insert into t_User(userName,tel,content,birthday) values(‘小王’，13254748547,’高中毕业’,2007-05-06)

b)请用sql语句把张三的手机号更新13709092345
　　　Update t_User set tel=’13709092545’ where userName = ‘张三’
c)请写出删除名为张四的全部记录的SQL
　　　Delete from t_User where userName = ‘张四’


2、请写出下列要求的SQL
表名Sates
id		district		bookName		amount
1		西南			词典1			0
2		西北			书籍1			12
3		西南			杂志2			23
请书写降序排列各地区销售情况，结果要求展现district字段
Select district from Sates order by amount DESC


3、什么是主键，什么是外键，什么是索引，各有什么作用？
答：主键是一个表中每条记录的唯一标识，这样的标识组成的列就是主键列，作用是让每行数据都可区别。
　　　外键是一个表中关联另外一个表的标识，通常都是关联另外一个表的主键，作用是建立多个表之间的关联
　　　

4、递归考核：
数据库中，有一个员工关联关系表：
id		pid		name
1		0		刘备
2		0		曹操
3		1		关羽
4		2		张辽
5		3		关平
……
分别表示：员工编号、上级领导编号、姓名。
把它从数据库查询出来，递归成如下树结构：（使用java查询数据库递归实现）
const list = [{
	id:1,
	name:'刘备',
	children:[
		{
			id:3,
			name:'关羽'
			childern:[……]
		},
		……
	]
},……]