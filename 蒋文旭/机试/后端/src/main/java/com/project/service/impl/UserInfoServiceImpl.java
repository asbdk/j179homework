package com.project.service.impl;

import com.project.bean.UserInfoBean;
import com.project.dao.IUserInfoDAO;
import com.project.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<IUserInfoDAO, UserInfoBean> implements IUserInfoService {

    @Resource
    private IUserInfoDAO userInfoDAO;
    @Override
    public UserInfoBean selectByUserName(String userName) {
        return userInfoDAO.selectByUserName(userName);
    }
}
