package com.lax.service;

import com.lax.bean.AdminBean;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IAdminService extends IService<AdminBean> {
    AdminBean findByName(String name);

    List<Integer> findCommentZan(int id);
}
