package com.lovo.controller;


import com.lovo.bean.CommentBean;
import com.lovo.bean.UserBean;
import com.lovo.result.ResponseResult;
import com.lovo.result.UserValid;
import com.lovo.service.ICommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zack
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/comments")
@ResponseResult
@Api("评论控制器")
public class CommentController {
    @Resource
    private ICommentService commentService;

    @ApiOperation("查询所有评论")
    @GetMapping
    public Object findAll(){
        return commentService.findAll();
    }

    @ApiOperation("点赞")
    @RequestMapping("/findByCount")
    public Object findByCount(Integer commentId){
        UserBean userBean = (UserBean) SecurityUtils.getSubject().getPrincipal();
        Integer userId = userBean.getUserId();
        if(commentService.findByCount(userId, commentId)==0){
            commentService.addCount(userId, commentId);
            return "addOk";
        }else {
            commentService.deleteCount(userId, commentId);
            return "delOk";
        }
    }
}

