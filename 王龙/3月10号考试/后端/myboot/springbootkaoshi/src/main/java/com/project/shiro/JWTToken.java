package com.project.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * 自定义token对象，目的是将token字符串转化为token对象。
 */
public class JWTToken implements AuthenticationToken {
    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }
}
