import ajax from "../service/ajax.js"
export default{
    namespaced:true,    //允许使用命名空间
    state:{
        pageObj:{},
        params:{pageNO:1}
    },
    mutations:{
        setPageObj(state,obj){
            state.pageObj = obj;
        },
        setParams(state,pageNumber){
            state.params = {pageNO:pageNumber}
        }
    },
    actions:{
        async findAll(context){
            var obj =await ajax.getSubmit("/project/user/findAll",context.state.params);
            context.commit("setPageObj",obj);
        }
    },
}