package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@TableName("t_user")
public class UserBean implements Serializable {
    @TableId(value = "user_id",type = IdType.ASSIGN_ID)
    private Long id;
    @TableField("user_name")
    private String name;
    @TableField("user_pwd")
    private String pwd;
    @TableField(exist = false)
    private List<GradeBean> gradeBeanList;
    @TableField(exist = false)
    private List<RoleBean> roleBeanList;
    public UserBean() {
    }

    public UserBean(String name, String pwd) {
        this.name = name;
        this.pwd = pwd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public List<GradeBean> getGradeBeanList() {
        return gradeBeanList;
    }

    public void setGradeBeanList(List<GradeBean> gradeBeanList) {
        this.gradeBeanList = gradeBeanList;
    }

    public List<RoleBean> getRoleBeanList() {
        return roleBeanList;
    }

    public void setRoleBeanList(List<RoleBean> roleBeanList) {
        this.roleBeanList = roleBeanList;
    }
}
