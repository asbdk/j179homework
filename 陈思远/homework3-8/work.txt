1、为什么说 Mybatis 是半⾃动 ORM 映射⼯具？它与全⾃动的区别在哪⾥？
	ORM 框架的本质是简化编程中操作数据库的编码，MyBatis 是⼀款标准的 ORM 框架，MyBatis 对动态 SQL ⽀持⾮常友好，可以在
	XML ⽂件中复⽤代码⾼效编写动态 SQL 。
	全自动的Hibernate，可以不⽤写⼀句 SQL ，但灵活度没有半自动的支持广泛

2、简述 Mybatis 的 Xml 映射⽂件和 Mybatis 内部数据结构之间的映射关系？
	Mybatis将所有Xml配置信息都封装到All-In-One重量级对象Configuration内部。
     	在Xml映射文件中，<parameterMap>标签会被解析为ParameterMap对象，其每个子元素会被解析为ParameterMapping对象。
	<resultMap>标签会被解析为ResultMap对象，其每个子元素会被解析为ResultMapping对象。
	每一个<select>、<insert>、<update>、<delete>标签均会被解析为MappedStatement对象，标签内的sql会被解析为BoundSql对象。

3、MyBatis中的#{}和${}的区别是什么？
	  #{}是预编译处理，${}是字符串替换。
	（1）mybatis在处理#{}时，会将sql中的#{}替换为?号，调用PreparedStatement的set方法来赋值。
	（2）mybatis在处理${}时，就是把${}替换成变量的值。
	（3）使用#{}可以有效的防止SQL注入，提高系统安全性。

4、Mybatis 动态 sql 是做什么的？都有哪些动态 sql？能简述⼀下动态 sql 的执⾏原理不？
	动态sql就是（在进行sql操作的时候）动态的根据属性值（所匹配的条件）来拼接数据库执行的sql语句，根据传入的属性值不同，动态拼接出不同的可执行sql。
	<if><where><choose><when><otherwise><foreach><include><set>
	第一部分：在启动加载解析xml配置文件的时候进行解析，根据关键标签封装成对应的handler处理对象，封装成sqlSource对象存在mappedStatement。
	第二部分：在执行过程中获取sqlSource中获取bondSql对象时，执行相应的标签handler

5、Mybatis 是如何将 sql 执⾏结果封装为⽬标对象并返回的？都有哪些映射形式？
	1.通过resultMap标签，将列名与对象属性名之间一一对应。
	2.通过sql的列别名功能，使列别名与对象属性名之间对应，比如：T—NAME AS name，列别名通常不区分大小写，属性名通常小写，mybatis通常会忽略大小写智能的对应。

6、Mybatis 能执⾏⼀对⼀、⼀对多的关联查询吗？都有哪些实现⽅式，以及它们之间的区别
	

7、Mybatis 是否⽀持延迟加载？如果⽀持，它的实现原理是什么？
	1.mybatis仅支持关联对象association和关联集合对象collection的延迟加载，association是一对一，
	collection指的是一对多查询，在mybatis配置文件中可以配置lazyloadingEnable=true/false.
	2.原理：使用CGLIB为目标对象建立代理对象，当调用目标对象的方法时进入拦截器方法。
	比如调用a.getb().getName(),拦截器方法invoke（）发现a.getb()为null值，会单独发送事先保存好的查询关联b对象的sql语句，
	把b查询上来然后调用a.setB(b),于是a的对象的属性b就有值了，然后接着调用a.getb().getName(),这就是延迟加载的原理。

8、ConcurrentHashMap 和 Hashtable 的区别
	hashtable(同一把锁):使用synchronized来保证线程安全，但效率非常低下。当一个线程访问同步方法时，其他线程也访问同步方法
	concurrenthashmap(分段锁):(锁分段技术)每一把锁只锁容器其中一部分数据，多线程访问容器里不同数据段的数据，就不会存在锁竞争，提高并发访问率。

9、描述Java类的加载过程
	加载->链接（验证+准备+解析）->初始化（使用前的准备）->使用->卸载

10、SQL优化手段有哪些
	1、在表中建立索引，优先考虑where、group by使用到的字段。
	2、尽量避免是用select *，返回无用的字段会降低查询效率。
	3、尽量避免使用 in 和 not in ，会导致数据库引擎放弃索引进行全表扫描。如果是连续数值，可以用between代替，如果是子查询，可以用exists代替
	4、尽量避免使用or，会导致数据库引擎放弃索引进行全表扫描。
	5、尽量避免在字段开头模糊查询，会导致数据库引擎放弃索引进行全表扫描。	
	用代码拼装sql时进行判断

