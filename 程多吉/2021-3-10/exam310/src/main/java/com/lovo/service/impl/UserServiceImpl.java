package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.CommentBean;
import com.lovo.bean.UserBean;
import com.lovo.dao.IUserDao;
import com.lovo.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<IUserDao, UserBean> implements IUserService {
    @Resource
    IUserDao dao;

    @Override
    public void addUser(UserBean userBean) {
        dao.insert(userBean);
    }

    @Override
    public UserBean findByName(String userName) {
        return dao.findByName(userName);
    }

    @Override
    public UserBean findById(Integer id) {
        return dao.findById(id);
    }

    @Override
    public List<CommentBean> findByUserName(String userName) {
        return dao.findByUserName(userName);
    }


}
