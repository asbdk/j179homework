package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo0309Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo0309Application.class, args);
    }

}
