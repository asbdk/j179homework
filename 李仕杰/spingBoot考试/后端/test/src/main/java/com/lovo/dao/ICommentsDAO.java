package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.CommentsBean;
import com.lovo.bean.UserBean;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentsDAO extends BaseMapper<CommentsBean> {

    public void changeStar(@Param("id") Long id, @Param("num")Integer num);
}
