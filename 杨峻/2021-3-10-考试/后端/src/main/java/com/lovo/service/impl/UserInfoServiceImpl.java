package com.lovo.service.impl;

import com.lovo.bean.DiscussBean;
import com.lovo.bean.UserInfoBean;
import com.lovo.dao.IUserInfoDAO;
import com.lovo.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<IUserInfoDAO, UserInfoBean> implements IUserInfoService {

    @Resource
    private IUserInfoDAO userInfoDAO;

    @Override
    public boolean checkFavor(Integer userId, Integer discussId) {
        if (userInfoDAO.checkFavor(userId, discussId) == 1){
            return true;
        }
        return false;
    }

    @Override
    public UserInfoBean selectByCode(String code) {
        return userInfoDAO.selectByCode(code);
    }
}
