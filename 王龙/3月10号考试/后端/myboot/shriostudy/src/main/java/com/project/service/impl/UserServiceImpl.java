package com.project.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.UserBean;
import com.project.dao.IUserDao;
import com.project.service.IUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<IUserDao, UserBean> implements IUserService {
    @Resource
    private IUserDao userDao;
    @Override
    public void add(UserBean userBean) {
        System.out.println("添加操作");
        userDao.insert(userBean);

    }

    @Override
    public UserBean findByName(String name) {
        return userDao.findByName(name);
    }

    @Override
    public UserBean getOne(String name, String pwd) {
        QueryWrapper<UserBean> wrapper = new QueryWrapper<UserBean>();
        wrapper.eq("user_name",name);
        wrapper.eq("user_pwd",pwd);
        return userDao.selectOne(wrapper);
    }

}
