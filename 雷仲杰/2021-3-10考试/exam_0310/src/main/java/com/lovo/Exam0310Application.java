package com.lovo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lovo.dao")
public class Exam0310Application {

	public static void main(String[] args) {
		SpringApplication.run(Exam0310Application.class, args);
	}

}
