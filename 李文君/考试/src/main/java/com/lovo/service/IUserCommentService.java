package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.UserCommentBean;

public interface IUserCommentService extends IService<UserCommentBean> {
    void add(UserCommentBean userCommentBean);
    void del(Long userCommentId);
    UserCommentBean find(Long userId,Long commentsId);

}
