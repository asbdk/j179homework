package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("t_review")
public class ReviewBean {
    private static final long serialVersionUID = 1L;
    @TableId(value = "review_id",type = IdType.NONE)
    private Long reviewId;
    @TableField("review_name")
    private String reviewName;
    @TableField("review_content")
    private String reviewContent;
    @TableField("review_score")
    private int reviewScore;

}
