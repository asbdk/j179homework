package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("liked_info")
public class LikedInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "liked_id", type = IdType.AUTO)
    private Long likedId;

    /**点赞状态1标识已赞，0标识没有赞*/
    @TableField("like_status")
    private String likeStatus = "1";

    /**所赞用户*/
    @TableField(exist = false)
    private UserInfoBean userInfoBean = new UserInfoBean();

    /**所赞评论*/
    @TableField(exist = false)
    private CommentInfoBean commentInfoBean = new CommentInfoBean();

    public LikedInfoBean(UserInfoBean userInfoBean, CommentInfoBean commentInfoBean) {
        this.userInfoBean = userInfoBean;
        this.commentInfoBean = commentInfoBean;
    }
}
