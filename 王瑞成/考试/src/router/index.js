import Vue from  "vue"
import VueRouter from "vue-router"
import login from "../pages/Login.vue"
import commentInfo from "../pages/commentInfo.vue"




Vue.use(VueRouter)
const originalPush = VueRouter.prototype.push
  VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const routes = [ 
  {path:'/commentInfo',component:commentInfo},
  {path:'/login',component:login}, 

]

export default new VueRouter({
    mode:'history',
    routes  // routes:routes    可以简写成  routes
})
