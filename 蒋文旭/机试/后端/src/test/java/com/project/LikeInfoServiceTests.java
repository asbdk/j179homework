package com.project;

import com.project.bean.CommentInfoBean;
import com.project.bean.LikedInfoBean;
import com.project.bean.UserInfoBean;
import com.project.service.ILikedInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LikeInfoServiceTests {

    @Resource
    private ILikedInfoService likedInfoService;

    @Test
    public void TestSelect(){
        UserInfoBean userInfoBean = new UserInfoBean(1L,"jwx", "123" );
        CommentInfoBean commentInfoBean = new CommentInfoBean(1L,"挺好的挺好的！");
        likedInfoService.updateLiked(new LikedInfoBean(null, "0", userInfoBean, commentInfoBean));

    }

    @Test
    public void TestAdd(){
        UserInfoBean userInfoBean = new UserInfoBean(2L,"jwx", "123" );
        CommentInfoBean commentInfoBean = new CommentInfoBean(3L,"挺好的挺好的！");
        likedInfoService.addLiked(new LikedInfoBean(userInfoBean, commentInfoBean));
    }
}
