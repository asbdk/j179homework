package com.project;

import com.project.dao.ICommendDAO;
import com.project.service.ICommendService;
import com.project.service.IUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MyTest {
    @Resource
    private IUserService userService;
    @Resource
    private ICommendService commendService;
    @Resource
    private ICommendDAO dao;
    @Test
    public void test01(){
//        System.out.println(userService.login("张三", "123"));
//        System.out.println(commendService.findAll(1, 2));
//        System.out.println(dao.findByPage());
        System.out.println(commendService.checkIsZan(1l, 1l));
    }
}
