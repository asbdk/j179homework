package com.project.shiro;

import com.project.bean.CommentInfoBean;
import com.project.bean.UserInfoBean;
import com.project.result.GlobalHandleException;
import com.project.service.IUserInfoService;
import com.project.util.WebUtil;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.List;

public class JWTRealm extends AuthorizingRealm {
    @Resource
    private IUserInfoService userInfoService;


    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取当前登录的用户
        UserInfoBean userInfoBean = (UserInfoBean) principalCollection.getPrimaryPrincipal();
        //通过SimpleAuthenticationInfo做授权
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        List<CommentInfoBean> commentInfoBeans = userInfoBean.getCommentInfoBeans();
        for (CommentInfoBean comment : commentInfoBeans) {
            //添加角色
            simpleAuthorizationInfo.addRole(comment.getCommentContent());
        }

        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String token = (String) authenticationToken.getPrincipal();
        if(token == null){
            return new SimpleAuthenticationInfo(null,"",getName());
        }
        UserInfoBean userInfoBean = null;
        try {
            userInfoBean = WebUtil.verifyToken(token,userInfoService);
        } catch (GlobalHandleException e) {}
        System.out.println("-------------------------userInfoBean:"+userInfoBean);
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(userInfoBean,token,getName());
        return simpleAuthenticationInfo;
    }
}
