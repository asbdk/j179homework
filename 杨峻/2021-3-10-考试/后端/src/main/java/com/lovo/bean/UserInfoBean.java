package com.lovo.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("userInfo")
public class UserInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "userInfo_id",type = IdType.NONE)
    private Integer userInfoId;
    @TableField("code")
    private String code;
    @TableField("pwd")
    private String pwd;
    @TableField(exist = false)
    private List<DiscussBean> discussBeans;
}
