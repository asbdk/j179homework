package com.lovo.service;

import com.lovo.bean.SpeakBean;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ISpeakService extends IService<SpeakBean> {
    public void add(SpeakBean speak);
    public List<SpeakBean> findAll(Long user);



}
