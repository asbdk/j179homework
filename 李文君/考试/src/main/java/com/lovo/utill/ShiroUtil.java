package com.lovo.utill;


import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.servlet.AbstractShiroFilter;

import java.util.LinkedHashMap;
import java.util.Map;

public class ShiroUtil {
    public static Map loadFilterChainMap(){
        //配置拦截链 使用LinkedHashMap,因为LinkedHashMap是有序的，shiro会根据添加的顺序进行拦截
        // Map<K,V> K指的是拦截的url V值的是该url是否拦截
        Map<String, String> filterChainMap = new LinkedHashMap<String, String>(16);
        //authc:所有url都必须认证通过才可以访问; anon:所有url都都可以匿名访问,先配置anon再配置authc。
        filterChainMap.put("/comments/**","anon");
        filterChainMap.put("/users/login", "anon");
        filterChainMap.put("/oauth/process", "anon");
        filterChainMap.put("/swagger-ui.html", "anon");
        filterChainMap.put("/webjars/**", "anon");
        filterChainMap.put("/swagger-resources/**", "anon");
        filterChainMap.put("/v2/api-docs", "anon");


//        for (GradeBean function:functionInfoBeans) {
//            if (function.getGradeName() != null && !function.getGradeName().equals("")) {
//                String url = function.getGradeUrl();
//                if(function.getFuncMethod() != null && !function.getFuncMethod().equals("")){
//                    url += "==" + function.getFuncMethod();
//                }
//                filterChainMap.put(url,"perms["+function.getGradeName()+"]");
//                System.out.println("过滤器   "+url );
//            }
//
//        }
        filterChainMap.put("/**", "authc");
        return  filterChainMap;
    }

    /**
     * 刷新权限的方法
     * @param shiroFilterFactoryBean
     * @param filterChainMap
     * @throws Exception
     */
    public static void updatePermission(ShiroFilterFactoryBean shiroFilterFactoryBean, Map filterChainMap) throws Exception {
        synchronized (shiroFilterFactoryBean) {
            AbstractShiroFilter shiroFilter;
            try {
                shiroFilter = (AbstractShiroFilter) shiroFilterFactoryBean.getObject();
            } catch (Exception e) {
                throw new Exception("get ShiroFilter from shiroFilterFactoryBean error!");
            }
            PathMatchingFilterChainResolver filterChainResolver = (PathMatchingFilterChainResolver) shiroFilter.getFilterChainResolver();
            DefaultFilterChainManager manager = (DefaultFilterChainManager) filterChainResolver.getFilterChainManager();

            // 清空拦截管理器中的存储
            manager.getFilterChains().clear();
            // 清空拦截工厂中的存储,如果不清空这里,还会把之前的带进去
            //            ps:如果仅仅是更新的话,可以根据这里的 map 遍历数据修改,重新整理好权限再一起添加
            shiroFilterFactoryBean.getFilterChainDefinitionMap().clear();
            // 动态查询数据库中所有权限
            shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainMap);
            // 重新构建生成拦截
            Map<String, String> chains = shiroFilterFactoryBean.getFilterChainDefinitionMap();
            for (Map.Entry<String, String> entry : chains.entrySet()) {
                manager.createChain(entry.getKey(), entry.getValue());
            }

        }
    }
}
