import axios from 'axios';
import qs from 'qs'; // 将对象转化为 a=1&b=2格式字符串
const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
export default {
    async getSubmit (serverURL,paramObj){
    let response = await axios({
        method:"get",  // 请求方式
        url:serverURL,  // 请求URL
        params:paramObj  // 请求参数
    })
    return response.data;  // 返回响应消息体内容    
},
    async getSubmitWithToken (serverURL,token,paramObj){
    let response = await axios({
        headers:{
            'token':token    // 请求头
        },         
        method:"get",  // 请求方式
        url:serverURL,  // 请求URL
        params:paramObj  // 请求参数
    })
    return response.data;  // 返回响应消息体内容    
},
   async postSubmit(serverURL,paramObj)  {
    let response = await axios({
        headers,
        method:"post",
        url:serverURL,
        data:qs.stringify(paramObj)
    })
    return response.data;
},
    async uploadSubmit (submitURL,param) {
        let formData = new FormData();

        for(var fieldName in param){
            formData.append(fieldName, param[fieldName]);
        }
        let config = {
            headers: {
            'Content-Type': 'multipart/form-data'
            }
        }
        var response = await axios.post(submitURL, formData, config);
        return response.data;
  
    }
}