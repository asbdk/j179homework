package com.project.service;

import com.project.bean.AdminInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IAdminInfoService extends IService<AdminInfoBean> {
    AdminInfoBean findByName(String name);
    Object selectAll(Integer page,Integer size);
}
