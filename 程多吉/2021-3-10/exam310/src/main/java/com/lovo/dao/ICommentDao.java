package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.CommentBean;
import com.lovo.bean.UserBean;

import java.util.List;

public interface ICommentDao extends BaseMapper<CommentBean> {

    List<CommentBean> findAllByOrder();

    List<CommentBean> checkCommentById(Integer id);


}
