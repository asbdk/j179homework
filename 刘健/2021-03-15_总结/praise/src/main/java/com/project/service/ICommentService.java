package com.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.CommentBean;
import com.project.bean.UserBean;

public interface ICommentService extends IService<CommentBean> {
    void add(Long userId,String info);

}
