package com.project.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.List;

public class CustomRealm extends AuthorizingRealm {
    @Resource
    private IUserInfoService userInfoService;
    @Resource
    private IFunctionInfoService functionInfoService;
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //获取当前登录的用户
        UserInfoBean userInfoBean = (UserInfoBean) principalCollection.getPrimaryPrincipal();
        //通过SimpleAuthenticationInfo做授权
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        // 得到当前用户包含的所有角色
        List<RoleInfoBean> roleInfoBeans = userInfoBean.getRoleInfoBeans();
        List<FunctionInfoBean> functionInfoBeans = functionInfoService.selectByRoleId(roleInfoBeans);
        for (RoleInfoBean role : roleInfoBeans) {
            //添加角色
            simpleAuthorizationInfo.addRole(role.getRoleName());
        }
        for (FunctionInfoBean function : functionInfoBeans) {
            //添加权限
            simpleAuthorizationInfo.addStringPermission(function.getFuncName());
        }
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 身份认证的操作
        //1.获取用户输入的账号
        String username = (String) authenticationToken.getPrincipal();
        //2.通过username从数据库中查找到user实体
        UserInfoBean userInfoBean = userInfoService.selectByUserName(username);
        if (userInfoBean == null) {
            return null;

        }
        //3.通过SimpleAuthenticationInfo做身份处理
        SimpleAuthenticationInfo simpleAuthenticationInfo =
                new SimpleAuthenticationInfo(userInfoBean, userInfoBean.getUserPass(), getName());
        //4.返回身份处理对象
        return simpleAuthenticationInfo;

    }
}
