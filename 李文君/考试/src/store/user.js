import {showAll} from '../service/user'
export default {
    namespaced:true,
    state:{
        token:"",
        users:[],
        pagination:{
        }
    },
    mutations:{
        setToken(state,token){
            state.token = token
        },
        setUsers(state,users){
            state.users = users;
        },
        setPagination(state,pagination){
            state.pagination = pagination;
        }
    },
    actions:{
        // async setUsers({commit},obj={}){
        //     let data = await showAll();
        //     commit("setUsers",data.data.records);
        //     commit("setPagination",data.data);
        // }
    }
}