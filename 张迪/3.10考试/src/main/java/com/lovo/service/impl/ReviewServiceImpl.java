package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.DianzanBean;
import com.lovo.dao.IDianzanDao;
import com.lovo.dao.IReviewDao;
import com.lovo.service.IDianzanService;
import com.lovo.util.RedisUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DianzanServiceImpl extends ServiceImpl<IDianzanDao, DianzanBean> implements IDianzanService {
    @Resource
    private IDianzanDao dianzanDao;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public List selectAll() {
        return dianzanDao.selectAll();
    }

    @Override
    public void updateScore(int id, int num) {
        DianzanBean reviewBean = dianzanDao.selectById(id);
        dianzanDao.updateScore(id, DianzanBean.getDianScore()+num);
    }

    @Override
    public void addUserDian(int userId, int dianzanId) {
        dianzanDao.addUserDian(userId,dianzanId);
    }

    @Override
    public void delUserDian(int userId, int dianzanId) {
        dianzanDao.delUserDian(userId,dianzanId);
    }

}
