package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.ReplyInfoBean;
import com.lovo.dao.IReplyInfoDAO;
import com.lovo.service.IReplyInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
@Service
public class ReplyInfoServiceImpl extends ServiceImpl<IReplyInfoDAO, ReplyInfoBean> implements IReplyInfoService {
    @Resource
    private IReplyInfoDAO replyInfoDAO;
    @Override
    public List<ReplyInfoBean> selectAll() {
        return replyInfoDAO.selectAll();
    }

    @Override
    public void add(ReplyInfoBean replyInfoBean) {
//        ReplyInfoBean replyInfoBean1 = new ReplyInfoBean();
//        replyInfoBean1.setReplyNum(replyInfoBean.getReplyNum()+1);
//        replyInfoDAO.updateById(replyInfoBean1);
        replyInfoDAO.add(replyInfoBean);

    }
}
