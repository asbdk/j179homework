package com.lax.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Date;

public class UploadFile {

    public static String upload(MultipartFile file){
        String path = "E:/server/upload";
        // 获取上传图片的文件名
        String fileName = file.getOriginalFilename();
        // 截取后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        // 生成新的文件名（时间 + 随机数 + 后缀名）
        String newFileName = new Date().getTime() + "_" + String.valueOf(Math.random()).substring(2) + suffixName;
        File targetFile = new File(path, newFileName);
        if (!targetFile.exists()) {
            targetFile.mkdirs();
        }
        // 保存
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newFileName;
    }
}
