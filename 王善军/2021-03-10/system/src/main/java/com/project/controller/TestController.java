package com.project.controller;

import com.project.util.UploadUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

@RestController
@RequestMapping("test")
public class TestController {

    @RequestMapping("info")
    public String test(){
        return "hello word!";
    }
    @RequestMapping("add")
    public String add(LocalDate startDate, LocalDate endDate, Integer startPrice, Integer endPrice,
                      @RequestParam("face")MultipartFile mf){
        UploadUtil.upload(mf, "META-INF/resources/upload");
        return startDate+"  "+endDate+"     "+startPrice+"      "+endPrice;
    }
}
