package com.lovo.service;

import com.lovo.bean.UserBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUserService extends IService<UserBean> {
    public UserBean selectByUserName(String userName);

}
