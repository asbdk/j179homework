package com.project.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.project.bean.UserBean;
import com.project.result.GlobalHandleException;
import com.project.result.ResponseResult;
import com.project.result.ResultCode;
import com.project.result.UserValid;
import com.project.service.IUserService;
import com.project.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

//import org.apache.shiro.SecurityUtils;
//import org.apache.shiro.authc.AuthenticationException;
//import org.apache.shiro.authc.IncorrectCredentialsException;
//import org.apache.shiro.authc.UnknownAccountException;
//import org.apache.shiro.authc.UsernamePasswordToken;
//import org.apache.shiro.authz.AuthorizationException;
//import org.apache.shiro.subject.Subject;

@RestController
@RequestMapping("/users")
@ResponseResult
@Api("用户系统api")
public class UserController {
    @Resource
    private IUserService service;

    @PostMapping("/register")
//    @ApiOperation(value = "注册请求")
    public Object register(@RequestBody UserBean userBean){
        // 使用SHA256加密
//        userBean.setPwd(DigestUtils.sha256Hex(userBean.getPwd()));
        System.out.println("------------"+userBean);
        service.add(userBean);
        return null;
    }

//    @ApiOperation(value = "显示所有用户")
//    @GetMapping
//    @UserValid
//    public Object showAll(Integer page,Integer size) throws GlobalHandleException {
////        Subject currentUser = SecurityUtils.getSubject();
////        if(currentUser.hasRole("系统管理员") || currentUser.isPermitted("查看用户")){
////            return userInfoService.selectAll(page,size);
////        }else{
////            throw new GlobalHandleException(ResultCode.USER_AUTHORIZATION_ERROR);
////        }
//        return service.(page,size);
//    }

    @ApiOperation("登录方法")
    @PostMapping("/login")
    public Object login(String userName, String userPass) throws GlobalHandleException {
        QueryWrapper wrapper = new QueryWrapper();
//        wrapper.eq("user_name",userName);
//        wrapper.eq("user_pass", DigestUtils.sha256Hex(userPass));
        UserBean userInfoBean = service.getOne(userName,userPass);
        if(userInfoBean == null){
            throw new GlobalHandleException(ResultCode.USER_LOGIN_ERROR);
        }

//        session.setAttribute("user",userInfoBean);
        String token = JWTUtil.createToken(userInfoBean.getId().toString(),userInfoBean.getPwd(), Calendar.SECOND,10);
        String refreshToken = JWTUtil.createToken(userInfoBean.getId().toString(),userInfoBean.getPwd(), Calendar.DATE,7);
        Map map = new HashMap();
        map.put("token",token);
        map.put("refreshToken",refreshToken);
        return map;
        // 把用户输入的账号和密码封装到shiro框架提供的token对象中
//        UsernamePasswordToken token = new UsernamePasswordToken(userName, DigestUtils.sha256Hex(userPass));
//        UsernamePasswordToken token = new UsernamePasswordToken(userName, userPass);
//        Subject currentUser = SecurityUtils.getSubject();
//        try{
//            //主体提交登录请求到SecurityManager
//            currentUser.login(token);
//        }catch(IncorrectCredentialsException ice){
//            throw new GlobalHandleException(ResultCode.USER_PASS_ERROR);
//        }catch (UnknownAccountException uae){
//            throw new GlobalHandleException(ResultCode.USER_NOT_EXIST);
//        }catch (AuthenticationException ae){
//            throw new GlobalHandleException(ResultCode.USER_AUTHENTICATION_ERROR);
//        }catch (AuthorizationException ae){
//            throw new GlobalHandleException(ResultCode.USER_AUTHORIZATION_ERROR);
//        }
//        return currentUser.getPrincipal();

    }

    @GetMapping("/removeSession")
    public Object removeSession(HttpSession session){
        session.setAttribute("user",null);
        return null;
    }

    /**
     * 从session中获取用户对象
     * @return
     */
    @GetMapping("/session")
    @UserValid
    public Object getUserWithSession(HttpSession session){
        return session.getAttribute("user");
    }
    /**
     * 从token中获取用户对象
     * @return
     */
    @GetMapping("/token")
    @UserValid
    public Object getUserWithToken(HttpServletRequest request){
        return request.getAttribute("user");
    }

    @ApiOperation("添加测试")
    @PostMapping("/add")
    @ResponseResult
    public String add(){
        return "ok";
    }
}
