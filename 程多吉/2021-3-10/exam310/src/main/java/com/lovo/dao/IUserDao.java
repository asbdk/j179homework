package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.CommentBean;
import com.lovo.bean.UserBean;

import java.util.List;

public interface IUserDao extends BaseMapper<UserBean> {

    UserBean findByName(String userName);

    UserBean findById(Integer id);

    List<CommentBean> findByUserName(String userName);

}
