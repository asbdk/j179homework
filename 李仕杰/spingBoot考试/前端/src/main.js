import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import ajax from "./service/ajax.js"
import router from "./router/index.js"
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

Vue.config.productionTip = false
Vue.prototype.ajax=ajax
Vue.use(ElementUI);
new Vue({
  router:router,
  render: h => h(App),
}).$mount('#app')
