package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.ReviewBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IReviewDao extends BaseMapper<ReviewBean> {
    List selectAll();
    void updateScore(@Param("id") int id,@Param("num") int num);
    void addUserReview(@Param("userId") int userId,@Param("reviewId") int reviewId);
    void delUserReview(@Param("userId") int userId,@Param("reviewId") int reviewId);
}
