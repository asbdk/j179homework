import ajax from "../service/ajax.js"
export default{
    namespaced:true,    //允许使用命名空间
    state:{
        userObj:{},
        param:{}
    },
    mutations:{
        setUsers(state,user){
            state.userObj = user;
        },
        setParam(state,param){
            state.param = param;
        },
    },
    actions:{
        async getUser(context){
            var userObj = await ajax.getSubmit("/project/users/getUser");
            context.commit("setUsers",userObj);
        },
        async checkFavor(context){
            var flag = await ajax.getSubmit("/project/users/checkFavor",context.state.param);
            context.commit("setDiscussList",flag.data);
        },
    }
}