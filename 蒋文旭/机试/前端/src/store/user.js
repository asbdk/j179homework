import {showAll,showAllComment} from '../service/user'
export default {
    namespaced:true,
    state:{
        token:"",
        users:[],
        comments:[],
        pagination:{
        }
    },
    mutations:{
        setToken(state,token){
            state.token = token
        },
        setUsers(state,users){
            state.users = users;
        },
        setComments(state,comments){
            state.comments = comments;
        },
        setPagination(state,pagination){
            state.pagination = pagination;
        }
    },
    actions:{
        async setUsers({commit},obj={}){
            let data = await showAll(obj.page,obj.size,$cookies.get('token'));
            commit("setUsers",data.data.records);
            commit("setPagination",data.data);
        },

        async setComments({commit},obj={}){
            let data = await showAllComment($cookies.get('token'));
            console.log('data',JSON.stringify(data));
            commit("setComments",data);
        }

    }
}