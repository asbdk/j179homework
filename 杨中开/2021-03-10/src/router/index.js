import Vue from 'vue'
import VueRouter from 'vue-router'

import demoLogin  from '../pages/demo/DemoLogin'
import demoList from '../pages/demo/DemoList'

Vue.use(VueRouter)

const routes = [
    {path:'/demoLogin',component:demoLogin},
    {path:'/demoList',component:demoList},

]

export default new VueRouter({
    mode:'history',
    routes
})