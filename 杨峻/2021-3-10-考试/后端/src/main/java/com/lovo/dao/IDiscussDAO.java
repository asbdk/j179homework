package com.lovo.dao;

import com.lovo.bean.DiscussBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IDiscussDAO extends BaseMapper<DiscussBean> {
    void deleteFavor(Integer userId, Integer discussId);
    void addFavor(Integer userId, Integer discussId);
    List<DiscussBean> findDiscuss();
}
