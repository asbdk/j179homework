import Vue from 'vue'
import VueRouter from 'vue-router'
import Register from '../pages/Register'
import Manage from '../pages/Manage'
import User from '../pages/User'
import Login from '../pages/Login'

import demoLogin  from '../pages/demo/DemoLogin'
import demoList from '../pages/demo/DemoList'

Vue.use(VueRouter)

const routes = [
    {path:'/demoLogin',component:demoLogin},
    {path:'/demoList',component:demoList},


    {path:'/',redirect:"/login"},
    {path:'/login',component:Login},
    {path:'/register',component:Register},
    {path:'/manage',component:Manage,
    children:[
        {path:'/manage/user',component:User}
    ]    
}
]

export default new VueRouter({
    mode:'history',
    routes
})