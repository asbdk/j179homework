package com.project.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@TableName("t_commend")
@Data
public class CommendBean implements Serializable {
    @TableId("comm_id")
    private Long id;
    @TableField("comm_content")
    private String content;
    @TableField(exist = false)
    private UserBean userBean;
    @TableField(exist = false)
    private Integer totalZan;


    public CommendBean() {
    }

    public CommendBean(String content) {
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public Integer getTotalZan() {
        return totalZan;
    }

    public void setTotalZan(Integer totalZan) {
        this.totalZan = totalZan;
    }
}
