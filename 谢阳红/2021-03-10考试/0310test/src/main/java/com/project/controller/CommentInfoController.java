package com.project.controller;


import com.project.result.ResponseResult;
import com.project.service.ICommentInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/comments")
public class CommentInfoController {
    @Resource
    private ICommentInfoService service;


    @GetMapping("/findAll")
    @ResponseResult
    public Object selectAll(){
        return  service.findAll();
    }

    @RequestMapping("/updateInfo")
    public Object update(int userId,int commentId){
        service.updateZan(userId, commentId);
        return "ok";
    }




}

