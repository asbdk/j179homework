package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.CommentBean;
import com.project.bean.UserBean;
import com.project.dao.ICommentDao;
import com.project.service.ICommentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ICommentServiceImpl extends ServiceImpl<ICommentDao, CommentBean> implements ICommentService {

    @Resource
    private ICommentDao dao;
    /**
     * 根据用户添加评论
     * @param userId 用户对象id
     * @param info 评论
     */
    @Override
    public void add(Long userId,String info) {
        dao.add(userId, info);
    }
}
