import Vue from "vue";
import Vuex from "vuex"; // 导入Vuex
import login from "./login.js"
import showAll from "./show.js"
import ajax from "../service/ajax.js"
import cookies from "vue-cookies"
Vue.use(Vuex); //应用Vuex


export default new Vuex.Store({
    //引入子模块
    modules:{
        login,showAll,
    },
    //定义共享数据，主要用于显示
    state:{
        num:0,
        loginUser:{}
    },
    getters:{   //定义计算属性
        computNum(state){
            return "$"+state.num;
        }
    },
    mutations:{
        addNum(state){
            state.num ++;
        },
        setLoginUser(state,obj){
            state.loginUser  = obj
        },
        
    },
    actions:{
        async quit(context){
            await ajax.getSubmit("/project/user/quit");
            context.state.loginUser = {};
        },
        async getUser(context){
            alert("665");
            var tokenParam = cookies.get("token");
            alert("**"+tokenParam);
            var user = await ajax.getSubmitWithToken("/project/user/getUser",{},tokenParam);
            alert(JSON.stringify(user))
            console.log(user);
            context.state.loginUser = user;
        }
    }
});