package com.lovo.controller;

import com.lovo.bean.UserCommentBean;
import com.lovo.service.IUserCommentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/UserComment")
public class UserCommentController {


    @Resource
    IUserCommentService userCommentService;

    @PostMapping("/thumpUp")
    public String  thumbUps(Integer commentId, Integer userId){
        UserCommentBean userCommentBean = new UserCommentBean(null, userId, commentId);
        userCommentService.addUserComment(userCommentBean);
        return "okay";
    }


    @PostMapping("/thumpDown")
    public String  thumbDown(Integer userCommentId){
        userCommentService.delUserComment(userCommentId);
        return "okay";
    }



}
