package com.project.service.impl;

import com.project.bean.CommentInfoBean;
import com.project.dao.ICommentInfoDAO;
import com.project.service.ICommentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class CommentInfoServiceImpl extends ServiceImpl<ICommentInfoDAO, CommentInfoBean> implements ICommentInfoService {
    @Resource
    ICommentInfoDAO dao;
    @Override
    public List<CommentInfoBean> findAll() {
        return dao.findAll();
    }

    @Override
    public void del(Integer adminId, Integer commentId) {
        dao.del(adminId, commentId);
    }

    @Override
    public void add(Integer adminId, Integer commentId) {
        dao.del(adminId, commentId);
    }

    @Override
    public Integer findByCommentId(Integer adminId, Integer commentId) {
        return dao.findByCommentId(adminId,commentId);
    }
}
