package com.lovo.demo02.service.impl;

import com.lovo.demo02.service.IStudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements IStudentService {
    @Override
    public void eat() {
        System.out.println("吃饭！");
    }
}
