package com.lovo.service.impl;

import com.lovo.bean.FunctionInfoBean;
import com.lovo.bean.RoleInfoBean;
import com.lovo.dao.IFunctionInfoDAO;
import com.lovo.service.IFunctionInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.util.ShiroUtil;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-05
 */
@Service
public class FunctionInfoServiceImpl extends ServiceImpl<IFunctionInfoDAO, FunctionInfoBean> implements IFunctionInfoService {
    @Resource
    private IFunctionInfoDAO functionInfoDAO;
    @Resource
    @Lazy
    private ShiroFilterFactoryBean shiroFilterFactoryBean;
    @Override
    public List selectByRoleId(List<RoleInfoBean> roleInfoBeans) {
        List ids = new ArrayList();
        for(RoleInfoBean roleInfoBean : roleInfoBeans){
            ids.add(roleInfoBean.getRoleId());
        }
        return functionInfoDAO.selectByRoleIds(ids);
    }

    @Override
    public void add(FunctionInfoBean functionInfoBean) throws Exception {
        functionInfoDAO.insert(functionInfoBean);
        List functionInfoBeans = functionInfoDAO.selectList(null);
        Map map = ShiroUtil.loadFilterChainMap(functionInfoBeans);
        ShiroUtil.updatePermission(shiroFilterFactoryBean,map);
    }

    @Override
    public void delete(Long id) throws Exception {
        functionInfoDAO.deleteById(id);
        List functionInfoBeans = functionInfoDAO.selectList(null);
        Map map = ShiroUtil.loadFilterChainMap(functionInfoBeans);
        ShiroUtil.updatePermission(shiroFilterFactoryBean,map);
    }
}
