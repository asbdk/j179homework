程序题
1、假定C盘下没有1.txt文件。在执行下面代码后会发生什么情况？
	   Writer w = null;
		BufferedWriter bw = null;
		try {
			w = new FileWriter("c:/1.txt");
			bw = new BufferedWriter(w);
			
			bw.write(“abcde”);
			bw.newLine();
			bw.write(“123”);
		} catch (IOException e) { 
			e.printStackTrace();
		}
结果是
FileNotFoundException
2、
public static void main(String[] args) {
		String s = "abcd";
		StringBuffer sb = new StringBuffer("abcd");
		test(s,sb);
		System.out.println(s+"   "+sb);
	}
	
	public static void test(String s,StringBuffer sb){
		s = s + "&&&";
		sb.append("***");
	}
}
结果是
abcd   abcd***

3、		try{
			JFrame j = null;
			j.setSize(400,300);
		}
		catch(NullPointerException e){
			System.out.println("空指针异常");
		}
		catch(Exception e){
			System.out.println("发生异常");
		}
		finally{
			System.out.println("finally代码");
		}
		
		System.out.println("异常外代码");
结果是
空指针异常
发生异常
finally代码

4、interface  A{ 
  int x = 0;
}
class B{ 
  int x =1;
}
class C extends B implements A { 
  public void pX(){ 
     System.out.println(x);

  }
  public static void main(String[] args) { 
     new C().pX();
  }
}
结果是
对x的引用不明确

5、class A{ 
   public void speak(int a){ 
    System.out.println("ok"); 
    } 
    } 
     class B extends A{ 
  public void speak(int a, int c){ 
       System.out.println("yes");  
      } 
      public static void main(String[] args){ 
        B b=new B(); 
        b.speak(0); 
      } 
    }   
结果是
ok

*6、interface Playable { 
   void play();
}
interface Bounceable { 
   void play();
}
interface Rollable extends Playable, Bounceable { 
   Ball ball = new Ball("PingPang");
}
class Ball implements Rollable { 
   private String name;
   public String getName() { 
       return name;
    }
   public Ball(String name) { 
       this.name = name;        
    }
  public void play() {
       ball = new Ball("Football");
       System.out.println(ball.getName());
    }

   public static void main(String[] args){
        Ball a=new Ball("abc");
        a.play();
   }
}
结果是
无法为最终变量ball分配值

选择题：
1.下面的main方法执行到A时，str的值是？（　A　）
void main () {
   string str=＂BEA＂;
   this.modify (str);
   //A
}
void modify (Sting str) {
   str.replace (＇A ＇, ＇E ＇);
   str.toLowerCase ();
   str+=＂B＂;
 }
A.＂BEA＂
B.＂BEE＂
C.＂bee＂
D.＂beeB＂

2.下面有关表和视图叙述中错误的是（　B　）
A.视图的数据可以来自多个表
B.对视图的数据修改最终传递到基表
C.基表不存在，不能创建视图
D.删除视图不会影响基表的数据


3.下面对数组的操作代码哪个是正确的？（ABDE　）（选择所有正确答案）
    char [ ] a=＂hello world＂.toCharArray ();
A.Class cc=a.getClass();
B.int len=a.length;
C.char c=a[999];
D.char c=a[0];
E.a.equals (new Object());


4、MyObject哪个方法会影响到这段代码的正确性以及效率？（　AC　）（选择所有正确答案）
   Set set=new java.util.HashSet ();
     set.add (new MyObject (＂A＂));
     if (set.contains (new MyObject (＂A＂))){ ．．．}
A.toString
B.hashCode
C.equals
D.clone

5、下面代码中对result描述正确的是？（　ACD　）（选择所有正确答案）
InputStream in=．．．;
  byte[ ] buff=new byte[1024];
  int result=in.read (buff,0,256);
A.result可能等于-1
B.result可能等于０
C.result可能等于256
D.result可能等于1024

6、下面哪个SQL属于DML？（　AB）（选择所有正确答案）
A.INSERT INTO table_name (column1,column2)VALUES (value1,value2);
B.drop table;
C.commit;
D.Select column from table;

7、SpringFramework提供了哪种IoC实现？（　ABC　）（选择所有正确答案）
A.基于接口的IoC实现（Interface Injection）
B.基于Setter的IoC实现（setter-based Injection）
C.基于构造函数的IoC实现（Constructor-based Injection）
D.基于静态方法的IoC实现（Static-Method Injection）

8、下列数据结构中，按先进后出原则组织数据的是（B）
A、线性链表
B、栈
C、循环链表
D、顺序表

9、根据以下代码，哪些类可以访问并改变变量name的值？（A）
package test;
class Target{
	public String name = "hello";
}
A、任何类
B、仅限Target类
C、任何test包中的类
D、任何继承Target的类

10、关于异常，下面哪个代码是合法的？（　AE）（选择所有正确答案）
A.try{}finally{}
B.try{}catch (Exception e){}catch (ArithmeticException a){}
C.try{}catch (Throwable th){}finally{}
D.try{}catch (Throwable th){}
E.try{}catch (NullpointerException ex){}catch (Exception ex){}




问答题
1、Java中字符流和字节流的区别及使用场景，各自对应的Java类是什么？
一个是按字节读写，一个是按字符
纯文本用字符流，传输对象、图片等用字节流
InputStream/OutputStream
Reader/Writer

2、以下程序创建了几个对象？分别是什么？
String A,B,C;
A="a";
B="b";
A=A+B;
StringBuffer D = new StringBuffer("abc");
D = D.append("567");

3、数据库中left join,right join,join有什么区别？
join等价于inner join内连接，bai是返回两个表中du都有值的符合条件的行。
left join左连dao接，是返回左表中所zhuan有shu的行及右表中符合条件的行。
right join右连接，是返回右表中所有的行及左表中符合条件的行。

4、线程有几种状态？并尽可能描述引起状态变化的原因。
新建（创建Thread对象）——就绪（调用start()）启动线程——运行（执行run()）——死亡（run()执行完毕）
运行期间有四个状态可以暂时停止运行线程
	休眠（调用sleep()）——等待（调用Object的wait()）——挂起（调用yield()，让出CPU控制权）——阻塞（等待IO输入）

5、删除数据库表中的重复记录。
delete from users where id not in (
		select t.max_id from 
		(select max(id) as max_id from users group by identity_id,name) as t
		);

6、简述socket通信原理
创建服务端serversocket，指定端口号和ip地址；

调用accept()方法开始监听；
调用getInputStream()方法 获取输入流，读取客户端信息；
调用getOutputStream()方法获取输出流。响应客户端的请求；

创建客户端Socket，指定服务器端口号和地址；
调用getOutputStream()方法获取输出流，向服务器端发送信息；
调用getInputStream()方法获取输入流，并读取服务器端的响应信息；

7、描述一下public,protected,private,final关键字在Java中的用法?
public 本类和非本类
protected 本包和不同包子类
private 本类

如果是被final修饰过的类，他就不可以有子类
用final修饰过的方法，不能被子类重写
值不可以改变

8、Java中创建一个对象有哪几种方法？
通过new产生对象
通过反射产生对象
通过clone产生对象
通过反序列化产生对象

9、描述一下Java中的异常机制,什么是Checked Exception, Unchecked Exception?
受检异常：必须要捕获；
非受检异常：可捕获也可以不捕获；

10、描述三层架构
三层架构分为表示层、业务逻辑层、数据访问层（持久层）。
数据访问层：完成内存和数据库之间的数据交互。采用DAO模式，建立实体类和数据库表进行映射，也就是哪个类映射哪个表，哪个属性（ORM）映射哪个列。数据访问层的目的，就是完成对象数据和关系数据的转换。
业务逻辑层：完成内存数据的业务处理操作。业务逻辑层采用事务脚本模式。将一个业务中所有的操作封装成一个方法。保证一个业务方法中，所有的数据库更新操作同时成功，或同时失败。不允许出现部分成功，部分失败，这样引起数据混乱的操作。
表示层：完成数据的展示，并提供界面供用户进行数据的录入。表示层采用MVC模式。
M：模型。也就是实体类，负责数据的封装和数据的传输。
V：视图。也就是GUI组件，负责提供界面和数据进行数据交互，以及数据的展示。
C：控制。也就是事件处理，负责业务流程的控制。

11、描述TCP和UDP的区别
TCP协议
	TCP协议是一种可靠的网络协议，提供三次握手机制。提供消息确认、错误检测和错误恢复等服务。如果数据在传输过程中有损耗，有意识，会要求发送方重新发送。从而保证数据的完整性。
UDP协议
	比较不可靠。如果数据在传输中有损耗，不会要求重发，优点是速度快。

12、什么是同步，什么是异步，什么是阻塞，什么是非阻塞。
同步是指：发送方发出数据后，等接收方发回响应以后才发下一个数据包的通讯方式。
异步是指：发送方发出数据后，不等接收方发回响应，接着发送下个数据包的通讯方式。

阻塞：线程持续等待资源中数据准备完成，直到返回响应结果。
非阻塞：线程直接返回结果，不会持续等待资源准备数据结束后才响应结果。

13、写clone()时，通常都有一行代码，是什么？
super.clone()

14、springMVC中如何实现文件上传


15、springMVC的拦截器如何实现？
第一种方式是要定义的Interceptor类要实现了Spring的HandlerInterceptor 接口
第二种方式是继承实现了HandlerInterceptor接口的类，比如Spring已经提供的实现了HandlerInterceptor接口的抽象类HandlerInterceptorAdapter

16、spring组件从spring容器中取出时默认为单例模式，如何实现多实例？
使用 @Scope("prototype") 注解来使对象成为多例模式
通过 ApplicationContext.getBean(C.class); 获取的实例是多例的

17、AOP常见的通知类型有哪些？
前置通知Before：目标方法执行前调用
后通知After：目标方法执行后调用
返回后通知：目标方法正确执行后调用
环绕通知Around：目标方法执行前，执行后都需要调用
抛出异常通知Throws：目标方法抛出异常时调用

18、谈谈mybatis的延迟加载。
延迟加载的条件：resultMap可以实现高级映射（使用association、collection实现一对一及一对多映射），association、collection具备延迟加载功能。
延迟加载的好处：
先从单表查询、需要时再从关联表去关联查询，大大提高 数据库性能，因为查询单表要比关联查询多张表速度要快。
延迟加载的实例：
如果查询订单并且关联查询用户信息。如果先查询订单信息即可满足要求，当我们需要查询用户信息时再查询用户信息。把对用户信息的按需去查询就是延迟加载。


编程题

1、写两个线程，一个线程打印1到52，另外一个线程打印字母A到Z，打印顺序为12A34B56C....5152Z。要求用线程间的通信。
static final Object object=new Object();


    public static void main(String[] args) {
        //线程1打印1
        new Thread(new Runnable(){
            @Override
            public void run() {
                for (int i = 1; i <= 52 ; i++) {
                    synchronized (object){
                        System.out.println(i);
                        if (i % 2 ==  0){
                            object.notify();//唤醒线程2
                            try{
                                object.wait();//线程1进入等待
                            }catch(InterruptedException e){
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }).start();

        //线程2
        new Thread(new Runnable(){
            @Override
            public void run() {
                for (char i = 'A'; i <= 'Z'; i++) {
                    synchronized (object) {
                        System.out.println(i);
                        object.notify();//唤醒线程1
                        try {
                            object.wait();//线程2进入等待
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

2、请用SQL语句查询学生成绩表（name，gender，score），获得男生和女生中成绩最高的前3名。
SELECT * FROM t_score  ORDER  BY score DESC LIMIT 0,3
 
3、写一个工具函数，去除字符串中重复的字母，并保持其他顺序不变。
public static String distinct(String str){
        if (str == null){
            return str;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            int firstIndex = str.indexOf(c);
            int endIndex = str.lastIndexOf(c);
            if (firstIndex ==  endIndex || firstIndex == i){
                sb.append(c);
            }
        }
        return sb.toString();
    }


4、某超市为服装类和生鲜类产品搞活动，服装类产品一律9折，生鲜类一律8折，其余商品原价。要求书写一个方法，输入商品类别和原价，返回该商品的销售价。
System.out.println("请输入类型");
        Scanner sc1 = new Scanner(System.in);
        String type = sc1.next();
        System.out.println("请输入价格");
        Scanner sc2 = new Scanner(System.in);
        String price = sc1.next();
        double salePrice = Double.parseDouble(price);
        if (type.equals("服装类")){
            System.out.println(salePrice * 0.9);
            return;
        }
        if (type.equals("生鲜类")){
            System.out.println(salePrice * 0.8);
            return;
        }
        System.out.println(salePrice);

