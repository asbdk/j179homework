package com.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.PraiseBean;

import java.util.List;

public interface IPraiseService extends IService<PraiseBean> {
    PraiseBean findByPraise(Long commentId,Long userId);
    boolean addPraise(Long userId,Long commentId);
    void deletePraise(Long userId,Long commentId);
}
