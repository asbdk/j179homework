package com.project.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author 一期一会
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentBean {
    private Long id;
    private String name;
    @NotNull
    @Min(15)
    @Max(50)
    private Integer age;
    private String gender;

    public StudentBean(String name, Integer age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
}
