package com.project.mapper;

import com.project.bean.DoTeyDiaryBean;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface IDoTeyDiaryMapper {
    /**
     *按宠物id分页查询宠物日志信息
     * @param doTeyDiaryId 宠物id
     * @return 宠物日志分页对象
     */
    public List<DoTeyDiaryBean> findByItem(int doTeyDiaryId);

    /**
     * 按id添加宠物日志信息
     * @param doTeyId  宠物id
     * @param doTeyDiary 日志对象
     */
    public void addDiary(@Param("doTeyId") int doTeyId,@Param("doTeyDiary")DoTeyDiaryBean doTeyDiary);

    /**
     * 按日志id查看日志对象
     * @param diaryId 日志id
     * @return 返回日志对象
     */
    public DoTeyDiaryBean findById(@Param("diaryId") int diaryId);

    /**
     * 按宠物日志id删除宠物日志
     * @param diaryId 日志id
     */
    public void delDiary(@Param("diaryId") int diaryId);

}
