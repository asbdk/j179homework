package com.project.dao;

import com.project.bean.CommentInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentInfoDAO extends BaseMapper<CommentInfoBean> {
    List<CommentInfoBean> findAll();
    Integer findById(@Param("userId") int userId, @Param("commentId") int commentId);
    void addUserCommentInfo(@Param("userId") int userId,@Param("commentId") int commentId);
    void delUserCommentInfo(@Param("userId") int userId,@Param("commentId") int commentId);

}
