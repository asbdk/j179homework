1. 描述下Servlet的生命周期
①容器加载并且实例化servlet
②调用init方法完成初始化
③当请求到达的时候调用servlet方法处理请求产生响应
④调用destroy()方法，清理资源

2. 描述Cookie和Session的作用，区别和各自的应用范围
Cookie：客户端请求服务器，服务器发送cookie信息给客户端。在产生响应时，会产生Set-cookie响应头，在该响应头中，以键值对方式描述cookie信息。响应信息到达客户端以后，会将cookie信息存储在客户端。当客户端再次发出请求，会将之前服务器发送给客户端的cookie信息，再以cookie请求头方式发送给服务器，服务器得到信息和发送的信息是一致的，就认为是同一用户。

Session：客户端访问服务器，服务器为了跟踪该用户的信息，为该用户产生一个session对象，同时为该session对象产生一个唯一标识sessionId。为了管理众多用户的session信息，以sessionId为键，以session对象为值，将session信息存放在Map集合中。

3. 依赖注入的方式有几种，各是什么？
三种
①构造器注入
②setter方法注入
③接口注入

4. Spring 由哪些模块组成?
①Spring-Core
②Spring-Context
③Spring-Aop
④Spring-Dao
⑤Spring-Web
⑥Spring Web MVC
⑦Spring-ORM

5. IOC 的优点是什么？
自动对代码进行初始化；创建实例的时候不需要了解其中的细节；降低耦合度，提高程序的灵活性和可维护性

6. ApplicationContext 通常的实现是什么?
①ClassPathXmlApplicationContext
②FileSystemXmlApplicationContext
③XmlWebApplicationContext
④AnnotationConfigApplicationContext

7. Spring 有几种配置方式？
①基于XML的配置
②基于注解的配置
③基于Java的配置

8.  Spring 框架中都用到了哪些设计模式？
①工厂设计模式
②单例设计模式
③代理设计模式
④模板方法模式
⑤包装器设计模式 
⑥观察者模式
⑦适配器模式

9. BeanFactory 接口和 ApplicationContext 接口有什么区别 ？
BeanFactory接口是Spring IoC 容器的核心接口。 BeanFactory 包含了种bean的定义，以便在接收到客户端请求时将对应的bean实例化。
Spring核心工厂是BeanFactory ,BeanFactory 采取延迟加载，第一次getBean时才会初始化Bean , ApplicationContext 是会在加载配置文件时初始化Bean；ApplicationContext是对BeanFactory扩展

10. BeanFactory和FactoryBean的区别？
BeanFactory： 以Factory结尾，表示它是一个工厂类，是用于管理Bean的一个工厂。 在Spring中，所有的Bean都是由BeanFactory(也就是IOC容器)来进行管理的。
FactoryBean：对FactoryBean而言，这个Bean不是简单的Bean，而是一个能生产或者修饰对象生成的工厂Bean，它的实现与设计模式中的工厂模式和修饰器模式类似。
