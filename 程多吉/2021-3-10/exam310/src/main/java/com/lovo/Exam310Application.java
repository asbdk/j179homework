package com.lovo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Exam310Application {

    public static void main(String[] args) {
        SpringApplication.run(Exam310Application.class, args);
    }

}
