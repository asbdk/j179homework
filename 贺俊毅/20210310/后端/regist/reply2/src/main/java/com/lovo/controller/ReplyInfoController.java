package com.lovo.controller;


import com.lovo.service.IReplyInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hjy
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/replys")
public class ReplyInfoController {
    @Resource
    private IReplyInfoService replyInfoService;
    @GetMapping("/showAll")
    public Object showAll(){
        return replyInfoService.selectAll();
    }
}

