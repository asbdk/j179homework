package com.project.service;

import com.project.bean.GradeBean;
import com.project.bean.RoleBean;

import java.util.List;

public interface IGradeService {
    List<GradeBean> findByRoleList(List<RoleBean> roleBeanList);
    List<GradeBean> findAll();
    public void add(GradeBean gradeBean) throws Exception;
    public void delete(Long id) throws Exception;
}
