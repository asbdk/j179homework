package com.project.dao;

import com.project.bean.CommentInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wangcm
 * @since 2021-03-10
 */
public interface ICommentInfoDAO extends BaseMapper<CommentInfoBean> {

    /**
     * 查询所有评论信息
     * @return
     */
    List<CommentInfoBean> findAll();
    /**
     * 根据评论id统计点赞次数
     * @param comId
     * @return
     */
    Integer countLikes(Integer comId);




}
