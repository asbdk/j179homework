package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.UserBean;
import com.lovo.bean.UserCommentBean;
import com.lovo.dao.IUserCommentDao;
import com.lovo.service.IUserCommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
@Service
@Transactional
public class UserCommentServiceImpl extends ServiceImpl<IUserCommentDao, UserCommentBean> implements IUserCommentService {

    @Resource
    private IUserCommentDao dao;

    @Override
    public void addUserComment(UserCommentBean userCommentBean) {
        dao.insert(userCommentBean);
    }

    @Override
    public void delUserComment(Integer id) {
        dao.delById(id);
    }
}
