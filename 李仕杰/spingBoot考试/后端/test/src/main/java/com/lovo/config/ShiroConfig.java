package com.lovo.config;


//import com.lovo.service.IGradeService;
import com.lovo.shiro.*;
import com.lovo.utill.ShiroUtil;
import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.servlet.Filter;
import java.util.List;
import java.util.Map;

@Configuration
public class ShiroConfig {
//    @Resource
//    private IGradeService functionInfoService;
    /**
     * 配置Shiro核心 安全管理器 SecurityManager
     * SecurityManager安全管理器：所有与安全有关的操作都会与SecurityManager交互；且它管理着所有Subject；负责与后边介绍的其他组件进行交互。（类似于SpringMVC中的DispatcherServlet控制器）
     */
    @Bean
    public SecurityManager securityManager() {
//        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
//        //将自定义的realm交给SecurityManager管理
//        securityManager.setRealm(customRealm());
//        return securityManager;

        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        //将自定义的realm交给SecurityManager管理
        securityManager.setRealm(jwtRealm());
        // 关闭 ShiroDAO 功能
        DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
        DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
        // 不需要将 Shiro Session 中的东西存到任何地方（包括 Http Session 中）
        defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
        subjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
        securityManager.setSubjectDAO(subjectDAO);
        //禁止Subject的getSession方法
        securityManager.setSubjectFactory(subjectFactory());
        return securityManager;
    }
    /**
     * 配置Shiro的Web过滤器，拦截浏览器请求并交给SecurityManager处理
     *
     * @return
     */
    @Bean
    public ShiroFilterFactoryBean webFilter(SecurityManager securityManager) {
//        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
//        shiroFilterFactoryBean.setSecurityManager(securityManager);
//        //配置拦截链 使用LinkedHashMap,因为LinkedHashMap是有序的，shiro会根据添加的顺序进行拦截
//        // Map<K,V> K指的是拦截的url V值的是该url是否拦截
//        Map<String, String> filterChainMap = new LinkedHashMap<String, String>(16);
//        // 配置不需要进行身份认证的路径，其中anon表示不需要认证
//        filterChainMap.put("/user/login", "anon");
//        filterChainMap.put("/swagger-ui.html", "anon");
//        filterChainMap.put("/webjars/**", "anon");
//        filterChainMap.put("/swagger-resources/**", "anon");
//        filterChainMap.put("/v2/api-docs", "anon");
////        filterChainMap.put("/user", "anon");
//
//
//        List<GradeBean> functionInfoBeans = functionInfoService.list();
//        for(GradeBean functionInfoBean : functionInfoBeans){
//            if(functionInfoBean.getGradeUrl() != null && !functionInfoBean.equals("")){
//                filterChainMap.put(functionInfoBean.getGradeUrl(),"perms["+functionInfoBean.getGradeName()+"]");
//            }
//        }
//
//        // 配置需要进行身份认证的路径。/**表示所有路径
//        filterChainMap.put("/**", "authc");
//        // 当认证失败后跳转的路径
//        shiroFilterFactoryBean.setLoginUrl("/authentication_fail");
//        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainMap);

        // 用自定义的FactoryBean来设置过滤器
        ShiroFilterFactoryBean shiroFilterFactoryBean = new RestShiroFilterFactoryBean();

        shiroFilterFactoryBean.setSecurityManager(securityManager);
        Map<String, Filter> filters = shiroFilterFactoryBean.getFilters();
        filters.put("perms", new RestAuthorizationFilter());
        filters.put("authc", new ShiroFormAuthenticationFilter());
//        List functionInfoBeans = functionInfoService.list();
        Map filterChainMap = ShiroUtil.loadFilterChainMap();
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainMap);


        return shiroFilterFactoryBean;

    }


    @Bean
    public CustomRealm customRealm(){
        return new CustomRealm();
    }
    @Bean
    public JWTRealm jwtRealm(){
        return new JWTRealm();
    }
    @Bean
    public JWTDefaultSubjectFactory subjectFactory(){
        return new JWTDefaultSubjectFactory();
    }

    /**
     * 开启aop注解支持
     * 即在controller中使用 @RequiresPermissions("user/userList")
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor attributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        //设置安全管理器
        attributeSourceAdvisor.setSecurityManager(securityManager);
        return attributeSourceAdvisor;
    }

    @Bean
    @ConditionalOnMissingBean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator defaultAAP = new DefaultAdvisorAutoProxyCreator();
        defaultAAP.setProxyTargetClass(true);
        return defaultAAP;
    }
}
