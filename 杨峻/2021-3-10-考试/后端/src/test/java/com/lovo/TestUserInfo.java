package com.lovo;

import com.lovo.service.IUserInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestUserInfo {
    @Resource
    private IUserInfoService userInfoService;

    @Test
    public void testFind(){
        System.out.println(userInfoService.selectByCode("code111"));
    }
}
