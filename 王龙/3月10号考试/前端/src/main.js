
import Vue from 'vue'
import App from './App.vue'
import router from "./router/router.js"
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import ajax from "./service/ajax.js"
import store from "./store/index.js"
import echarts from 'echarts'
import cookies from "vue-cookies"

Vue.prototype.$echarts = echarts;

Vue.use(ElementUI);
Vue.use(cookies)
Vue.prototype.ajax=ajax;

Vue.config.productionTip = false

new Vue({
  store,
  router:router,
  render: h => h(App),
}).$mount('#app')
