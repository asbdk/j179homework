package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.UserInfoBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-02
 */
public interface IUserInfoService extends IService<UserInfoBean> {
    Object selectAll(Integer page, Integer size);
    void add(UserInfoBean userInfoBean);
    UserInfoBean selectByUserName(String userName);
}
