import Vue from "vue"
import VueRouter from "vue-router"
import index from "../pages/index"
import login from "../pages/login.vue"


Vue.use(VueRouter)
const routes = [
    {path:"/index",component:index},
    {path:"/login",component:login},
 
    
]
export default new VueRouter({
    
    mode:'history',
    routes
})