package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.CommentsBean;
import com.lovo.bean.UserBean;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentsDAO extends BaseMapper<UserBean> {

    public List<CommentsBean> findAll();
}
