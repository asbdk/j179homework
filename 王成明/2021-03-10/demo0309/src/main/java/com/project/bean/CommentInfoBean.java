package com.project.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author Wangcm
 * @since 2021-03-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("comment_info")
@NoArgsConstructor
@AllArgsConstructor
public class CommentInfoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "com_id",type = IdType.NONE)
    private Integer comId;
    /**
     * 评论内容
     */
    @TableField(value = "com_content")
    private String comContent;
    /**
     * 评论发布人对象
     */
    @TableField(exist = false)
    private UserInfoBean user;
    /**
     * 评论点赞次数
     */
    @TableField(exist = false)
    private Integer count = 0;

    @TableField(exist = false)
    private List<UserInfoBean> userInfoBeans;


}
