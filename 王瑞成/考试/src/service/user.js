import axios from 'axios'
import qs from 'qs'
export const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
export const register = async (adminName,adminPass) => {
    let response = await axios({
        
        method:"post",
        url:"/api/admin",
        data:{
            adminName,adminPass
        }
    })
    return response.data;
}
export const login = async (adminName,adminPass) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/api/admin/login",
        data:qs.stringify({
            adminName,adminPass
        })
    })
    return response.data;
}
export const showAll = async (page=1,size=5,token) => {
    
    console.log(page,size)
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/admin",
        params:{page,size}
    })
    return response.data;
}
export const getUser = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/admin/token"
    })
    return response.data;
}
export const logout = async () => {

    let response = await axios({
        method:"get",
        url:"/api/admin/removeSession"
    })
    return response.data;
}