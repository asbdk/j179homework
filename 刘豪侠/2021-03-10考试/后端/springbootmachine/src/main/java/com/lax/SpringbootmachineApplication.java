package com.lax;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@MapperScan("com.lax.dao")
@EnableAspectJAutoProxy
public class SpringbootmachineApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootmachineApplication.class, args);
    }

}
