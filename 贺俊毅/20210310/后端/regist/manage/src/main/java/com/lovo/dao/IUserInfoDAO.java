package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.UserInfoBean;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-02
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    UserInfoBean selectByUserName(String userName);
}
