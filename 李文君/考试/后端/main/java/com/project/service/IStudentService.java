package com.project.service;


import com.project.bean.StudentBean;

import java.util.List;

public interface IStudentService {
    void add(StudentBean studentBean);
    StudentBean findById(int id);
    void del(int id);
    void update(int id,String age);
    List<StudentBean> findAll();
}
