package com.lovo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.bean.CommentBean;
import com.lovo.dao.ICommentDao;
import com.lovo.dao.IUserDao;
import com.lovo.service.ICommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.security.Provider;
import java.util.List;

@Service
@Transactional
public class CommentServiceImpl extends ServiceImpl<ICommentDao, CommentBean> implements ICommentService {

    @Resource
    private ICommentDao dao;

    @Override
    public void addComment(CommentBean commentBean) {
        dao.insert(commentBean);
    }

    @Override
    public List<CommentBean> findAllByOrder() {
        return dao.findAllByOrder();
    }

    @Override
    public List<CommentBean> checkCommentById(Integer id) {
        return dao.checkCommentById(id);
    }


}
