package test;

import com.project.MainServer;
import com.project.bean.ManBean;
import com.project.service.IManService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MainServer.class)
public class ManTest {
    @Autowired
    private IManService service;
    @Test
    public void test(){
        service.add(new ManBean("张一", LocalDate.parse("1993-03-19")));
    }
}
