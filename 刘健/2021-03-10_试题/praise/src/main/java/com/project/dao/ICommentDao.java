package com.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.project.bean.CommentBean;
import com.project.bean.UserBean;
import org.apache.ibatis.annotations.Param;

public interface ICommentDao extends BaseMapper<CommentBean> {
    void add(@Param("user") Long userId, @Param("info") String info);
}
