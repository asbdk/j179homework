package com.project.service;

import com.project.bean.UserInfoBean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
public interface IUserInfoService extends IService<UserInfoBean> {
    UserInfoBean selectByUserName(String userName);

}
