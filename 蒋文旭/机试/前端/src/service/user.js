import axios from 'axios'
import qs from 'qs'
export const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
export const register = async (userName,userPass,userHeader) => {
    let response = await axios({
        
        method:"post",
        url:"/api/users",
        data:{
            userName,userPass,userHeader
        }
    })
    return response.data;
}
export const login = async (userName,userPass) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/api/users/login",
        data:qs.stringify({
            userName,userPass
        })
    })
    return response.data;
}
export const showAll = async (page=1,size=5,token) => {
    console.log(page,size)
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/users",
        params:{page,size}
    })
    return response.data;
}
export const getUser = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/users/token"
    })
    return response.data;
}
export const refreshToken = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/users/refreshToken"
    })
    return response.data;
}
export const logout = async () => {

    let response = await axios({
        method:"get",
        url:"/api/users/removeSession"
    })
    return response.data;
}

export const showAllComment = async (token) => {
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/comment",
    })
    return response.data;
}

