package com.project.dao;

import com.project.bean.UserInfoBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wangcm
 * @since 2021-03-10
 */
public interface IUserInfoDAO extends BaseMapper<UserInfoBean> {
    /**
     * 点赞，中间表中加入数据
     * @param userId 用户id
     */
    void addLikes(@Param("userId") int userId, @Param("commentId")int commentId);

    /**
     * 取消点赞，删除中间表信息
     * @param userId
     */
    void cancelLikes(@Param("userId") int userId, @Param("commentId")int commentId);

    /**
     * 判断当前登录用户是否对该评论点赞
     * @param userId 用户id
     * @param comId 评论id
     * @return 信息
     */
    Integer checkLikes(@Param("userId") int userId, @Param("comId")int comId);

}
