package com.lovo.controller;

import com.lovo.bean.UserCommentBean;
import com.lovo.result.ResponseResult;
import com.lovo.service.IUserCommentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/userComments")
@ResponseResult
public class UserCommentsController {
    @Resource
    private IUserCommentService service;

    @PostMapping
    public Object add(UserCommentBean userCommentBean){
        service.add(userCommentBean);
        return "ok";
    }
    @DeleteMapping("/{id}")
    public Object del(@PathVariable("id")Long userCommentId) {
        service.del(userCommentId);
        return "ok";
    }

    /**
     * 判断是否点过赞
     * @param userId 登录用户id
     * @param commentsId 点赞记录id
     * @return 中间表记录
     */
    @GetMapping
    public Object find(Long userId,Long commentsId ){
        return service.find(userId,commentsId);
    }
}
