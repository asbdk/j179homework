import ajax from "../service/ajax"

export default{
    namespaced:true, //允许使用命名空间
    state:{
        token:"",
        reviewList:{},
        param:{page:1},

    },
    mutations:{
        setToken(state,info){
            state.token = info
        },
        setReviewList(state,list){
            state.reviewList = list
        },
        setParam(state,info){
            state.param.page = info
        }
    },
    actions:{
        async selectAll(context){
            var info = await ajax.getSubmit("/project/review");
            console.log(JSON.stringify(info))
            context.commit("setReviewList",info);
        }
    }
}