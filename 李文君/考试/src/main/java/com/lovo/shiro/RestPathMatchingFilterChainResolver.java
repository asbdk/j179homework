package com.lovo.shiro;

import org.apache.shiro.web.filter.mgt.FilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 路径匹配的过滤器链处理器
 */
public class RestPathMatchingFilterChainResolver extends PathMatchingFilterChainResolver {
//    private static final Logger log = LoggerFactory.getLogger(RestPathMatchingFilterChainResolver.class);
    @Override
    public FilterChain getChain(ServletRequest request, ServletResponse response, FilterChain originalChain) {
        FilterChainManager filterChainManager = this.getFilterChainManager();
        if (!filterChainManager.hasChains()) {
            return null;
        } else {
            String requestURI = this.getPathWithinApplication(request);
            if (requestURI != null && !"/".equals(requestURI) && requestURI.endsWith("/")) {
                requestURI = requestURI.substring(0, requestURI.length() - 1);
            }

            //the 'chain names' in this implementation are actually path patterns defined by the user.  We just use them
            //as the chain name for the FilterChainManager's requirements
            for (String pathPattern : filterChainManager.getChainNames()) {
                String[] pathPatternArray = pathPattern.split("==");
                // 只用过滤器链的 URL 部分与请求的 URL 进行匹配
                if (pathMatches(pathPatternArray[0], requestURI)) {

//                    if (log.isTraceEnabled()) {
//                        log.trace("Matched path pattern [" + pathPattern + "] for requestURI [" + requestURI + "].  " +
//                                "Utilizing corresponding filter chain...");
//                    }

                    String httpMethod = WebUtils.toHttp(request).getMethod().toUpperCase();

                    if (pathPatternArray.length > 1 && !httpMethod.equals(pathPatternArray[1].toUpperCase())) {
                        System.out.println("判断请求"+ httpMethod + "  "+ pathPatternArray[1]);
                        continue;
                    }
                    return filterChainManager.proxy(originalChain, pathPattern);
                }
            }
            return null;
        }

    }
}
