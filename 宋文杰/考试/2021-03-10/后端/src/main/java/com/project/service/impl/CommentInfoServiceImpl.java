package com.project.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.CommentInfoBean;
import com.project.dao.ICommentInfoDao;
import com.project.service.ICommentInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CommentInfoServiceImpl extends ServiceImpl<ICommentInfoDao, CommentInfoBean> implements ICommentInfoService {

    @Resource
    private ICommentInfoDao commentInfoDao;

    @Override
    public Object findAll(Integer pageNO, Integer size) {
        IPage pageObject = new Page(pageNO,size);

        return commentInfoDao.selectPage(pageObject, null);
    }

    @Override
    public void addUC(int userId, int commentId) {
        commentInfoDao.addUC(userId, commentId);
    }

    @Override
    public void delUC(int commentId, int userId) {
        commentInfoDao.delUC(commentId, userId);
    }

    @Override
    public void updateAddLikeNum(int commentId) {
        CommentInfoBean commentInfoBean = commentInfoDao.selectById(commentId);
        int likeNum = commentInfoBean.getLikeNum() + 1;
        commentInfoBean.setLikeNum(likeNum);
        commentInfoDao.updateById(commentInfoBean);
    }

    @Override
    public void updateReduceLikeNum(int commentId) {
        CommentInfoBean commentInfoBean = commentInfoDao.selectById(commentId);
        int likeNum = commentInfoBean.getLikeNum() - 1;
        commentInfoBean.setLikeNum(likeNum);
        commentInfoDao.updateById(commentInfoBean);
    }

}
