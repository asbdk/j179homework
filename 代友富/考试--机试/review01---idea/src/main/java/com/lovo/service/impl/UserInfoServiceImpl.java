package com.lovo.service.impl;

import com.lovo.bean.UserInfoBean;
import com.lovo.dao.IUserInfoDAO;
import com.lovo.service.IUserInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<IUserInfoDAO, UserInfoBean> implements IUserInfoService {
    @Resource
    private IUserInfoDAO userInfoDAO;

    @Override
    public UserInfoBean login(String userName, String userPwd) {
        return userInfoDAO.login(userName,userPwd);
    }

    @Override
    public UserInfoBean selectByUserName(String userName) {
        return userInfoDAO.selectByUserName(userName);
    }
}
