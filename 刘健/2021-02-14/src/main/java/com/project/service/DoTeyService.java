package com.project.service;

import com.github.pagehelper.PageInfo;
import com.project.bean.DoTeyBean;

/**
 * 宠物业务接口
 */
public interface DoTeyService {

    public int PAGESIZE=3;
    /**
     * 按用户id添加宠物
     * @param userId 用户id
     * @param doTey 宠物对象
     */
    public void addDoTey(int userId,DoTeyBean doTey);

    /**
     * 按宠物id修改宠物属性
     * 动态修改宠物属性 点击“喂食”--------->力量+5,爱心+1
     * 点击“讲故事”--------->智力+5,爱心+1
     * 点击“玩游戏”--------->力量+1,智力+2,爱心+3
     * @param id
     * @param info info方便判断需要修改哪些数值 ‘喂食’，‘玩游戏’，‘讲故事’

     */
    public void update(int id,String info);

    /**
     * 按昵称、类别 动态条件分页查询，可以选择排序方式
     * @param pageNO 页码
     * @param type 类别
     * @param doTeyName 宠物昵称
     * @param rank 排序方式
     * @return 宠物分页对象
     */
    public PageInfo<DoTeyBean> findByItem(int pageNO,int userId,String type,String doTeyName,String rank);

    /**
     * 按id删除宠物,同时删除该宠物的日志
     */
    public void del(int doTeyId);

}
