package com.lovo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.bean.UserBean;

public interface IUserDao extends BaseMapper<UserBean> {
    UserBean selectByUserName(String userName);

}
