package com.lovo.service.impl;

import com.lovo.bean.UsersBean;
import com.lovo.dao.IUsersDAO;
import com.lovo.service.IUsersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@Service
public class UsersServiceImpl extends ServiceImpl<IUsersDAO, UsersBean> implements IUsersService {
@Resource
private IUsersDAO dao;
    @Override
    public UsersBean selectByUserName(String userNmae) {
        return dao.selectByUserName(userNmae);
    }
}
