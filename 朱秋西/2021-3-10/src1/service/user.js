import axios from 'axios'
import qs from 'qs'
export const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
}
export const login = async (userName,userPwd) => {
    let response = await axios({
        headers,
        method:"post",
        url:"/api/users/login",
        data:qs.stringify({
            userName,userPwd
        })
    })
    return response.data;
}
export const showAll = async (token) => {
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/reviews/selectAllReview"
    })
    return response.data;
}
export const update = async (userId,revId) => {
    let response = await axios({
        method:"post",
        url:"/api/reviews/updateNum",
        data:qs.stringify({
            userId,revId
        })
    })
    return response.data;
}
export const look = async (userId,revId) => {
    let response = await axios({
        method:"get",
        url:"/api/reviews/look",
        params:{userId,revId},
        paramsSerializer: function(params) {
            return qs.stringify(params, {arrayFormat: 'repeat'})
        }
    })
    return response.data;
}
export const getUser = async (token) => {
    
    let response = await axios({
        headers:{
            'token':token
        },
        method:"get",
        url:"/api/users/token"
    })
    return response.data;
}