package com.lovo.result;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
@NoArgsConstructor
@Data
public class GlobalHandleException extends Throwable {
    private ResultCode resultCode;

    public GlobalHandleException(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    //当我们使用这个@ExceptionHandler注解时，我们需要定义一个异常的处理方法，比如上面的handlemethodArgumentNotValid()方法，
    // 给这个方法加上@ExceptionHandler注解，这个方法就会处理类中其他方法（被@RequestMapping注解）抛出的异常。
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result handlemethodArgumentNotValid(MethodArgumentNotValidException exception) {
        Result resultVO = Result.fail(ResultCode.PARAM_IS_INVALID);
        return resultVO;
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public Result handleConstraintViolation(ConstraintViolationException exception) {
        Result resultVO = Result.fail(ResultCode.PARAM_IS_INVALID);
        return resultVO;
    }
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//    @ExceptionHandler(UnauthorizedException.class)
//    public ResultVO handleUnauthorized(UnauthorizedException exception) {
//        ResultVO resultVO = ResultVO.fail(ResultCode.USER_AUTHORIZATION_ERROR);
//        return resultVO;
//    }

}

