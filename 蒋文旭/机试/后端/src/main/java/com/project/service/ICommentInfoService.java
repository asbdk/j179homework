package com.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.project.bean.CommentInfoBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jwx
 * @since 2021-03-10
 */
public interface ICommentInfoService extends IService<CommentInfoBean> {
    /**
     * 获取所有的评论.按点赞数排序
     * @return
     */
    Object selectAll();

    /**
     * 修改评论点赞数量
     * @param count 点赞数量新总额
     */
    void updateCommentCount(Long commentId, int count);


    /**
     * 检测用户是否点赞该评论。
     * @param userId 当前用户
     * @param commentId 当前评论ID
     * @return 返回值为1：已点赞。返回值0：未点赞
     */
    int checkedLike(Long userId,Long commentId);

}
