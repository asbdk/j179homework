package com.project.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.project.bean.DoTeyDiaryBean;
import com.project.mapper.IDoTeyDiaryMapper;
import com.project.service.DoTeyDiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 宠物日志实现类
 */
@Service
@Transactional
public class DoTeyDiaryServiceImpl implements DoTeyDiaryService {
    @Autowired
    private IDoTeyDiaryMapper mapper;

    @Override
    public PageInfo<DoTeyDiaryBean> findByItem(int pageNO,int doTeyDiaryId) {
        PageHelper.startPage(pageNO, PAGESIZE);
        PageInfo<DoTeyDiaryBean> info = new PageInfo<>(mapper.findByItem(doTeyDiaryId));
        return info;
    }

    @Override
    public void addDiary(int doTeyId, DoTeyDiaryBean doTeyDiary) {
        mapper.addDiary(doTeyId, doTeyDiary);
    }

    @Override
    public DoTeyDiaryBean findById(int diaryId) {
        return mapper.findById(diaryId);
    }

    @Override
    public void delDiary(int diaryId) {
        mapper.delDiary(diaryId);
    }

    @Override
    public void batchDel() {

    }
}
