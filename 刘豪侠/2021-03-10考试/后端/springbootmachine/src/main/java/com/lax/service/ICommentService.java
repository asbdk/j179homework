package com.lax.service;

import com.lax.bean.CommentBean;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface ICommentService extends IService<CommentBean> {

    List<CommentBean> findAll();

    void updateZan(int idAdmin,int idComment);

    boolean findById(int idAdmin,int idComment);


}
