package com.lovo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.bean.ReviewBean;
import com.lovo.bean.UserBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IReviewService extends IService<ReviewBean> {
    List selectAll();
    void updateScore( int id, int num);
    void addUserReview(int userId, int reviewId);
    void delUserReview(int userId, int reviewId);
}
