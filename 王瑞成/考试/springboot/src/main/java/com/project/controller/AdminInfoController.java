package com.project.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.project.bean.AdminInfoBean;
import com.project.result.GlobalHandleException;
import com.project.result.ResultCode;
import com.project.result.UserValid;
import com.project.service.IAdminInfoService;
import com.project.util.JWTUtil;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
@RestController
@RequestMapping("/admin")
public class AdminInfoController {
    @Resource
    private IAdminInfoService adminInfoService;

    @ApiOperation(value = "显示所有用户")
    @GetMapping
    @UserValid
    public Object showAll(Integer page,Integer size){
        return adminInfoService.selectAll(page,size);
    }
    @ApiOperation("登录方法")
    @PostMapping("/login")
    public Object login(String adminName, String adminPass) throws GlobalHandleException{
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("admin_name",adminName);
        wrapper.eq("admin_pass",DigestUtils.sha256Hex(adminPass));
        AdminInfoBean adminInfoBean = adminInfoService.getOne(wrapper);
        if(adminInfoBean == null){
            throw new GlobalHandleException(ResultCode.USER_LOGIN_ERROR);
        }
        String token = JWTUtil.createToken(adminInfoBean.getAdminName(),adminInfoBean.getAdminPass(), Calendar.MINUTE,30);
        String refreshToken = JWTUtil.createToken(adminInfoBean.getAdminName(),adminInfoBean.getAdminPass(), Calendar.DATE,7);
        Map map = new HashMap();
        map.put("token",token);
        map.put("refreshToken",refreshToken);
        return map;
    }
    @GetMapping("/removeSession")
    public Object removeSession(HttpSession session){
        session.setAttribute("user",null);
        return null;
    }


    /**
     * 从session中获取用户对象
     * @return
     */
    @GetMapping("/session")
    @UserValid
    public Object getUserWithSession(HttpSession session){
        return session.getAttribute("user");
    }
    /**
     * 从token中获取用户对象
     * @return
     */
    @GetMapping("/token")
    @UserValid
    public Object getUserWithToken(HttpServletRequest request){
        return SecurityUtils.getSubject().getPrincipal();
    }

    @GetMapping("/refreshToken")
    @UserValid
    public Object refreshToken(HttpServletRequest request){
        AdminInfoBean adminInfoBean = (AdminInfoBean) request.getAttribute("user");
        return JWTUtil.createToken(adminInfoBean.getAdminId().toString(),adminInfoBean.getAdminPass(),Calendar.SECOND,10);
    }
}

