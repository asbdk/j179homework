package com.project.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.project.bean.UserBean;
import com.project.dao.IUserDao;
import com.project.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class IUserServiceImpl extends ServiceImpl<IUserDao, UserBean> implements IUserService {
    @Resource
    private IUserDao userDao;
    @Override
    public Object login(String name, String pwd) {
        return userDao.login(name, pwd);
    }

    @Override
    public UserBean selectByUserName(String name) {
        return userDao.selectByUserName(name);
    }
}
