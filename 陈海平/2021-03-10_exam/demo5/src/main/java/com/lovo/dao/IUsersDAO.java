package com.lovo.dao;

import com.lovo.bean.UsersBean;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bdk
 * @since 2021-03-10
 */
public interface IUsersDAO extends BaseMapper<UsersBean> {
    UsersBean selectByUserName(@Param("userName") String userName);

}
