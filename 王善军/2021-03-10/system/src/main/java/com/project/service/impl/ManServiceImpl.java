package com.project.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.project.bean.ManBean;
import com.project.mapper.IManMapper;
import com.project.service.IManService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ManServiceImpl implements IManService {
    @Autowired
    private IManMapper mapper;
    @Override
    public void add(ManBean man) {
        mapper.add(man);
    }

    @Override
    public PageInfo<ManBean> findAll(int pageNO) {
        PageHelper.startPage(pageNO,PAGESIZE);
        PageInfo<ManBean> pageInfo = new PageInfo<ManBean>(mapper.findAll());
        return pageInfo;
    }
}
