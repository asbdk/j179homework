import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '../pages/login.vue'
import mainpage from '../pages/mainpage.vue'

Vue.use(VueRouter)
const routes=[
    {path:"/login", component:login},
    {path:"/mainpage",component:mainpage},
]



export default new VueRouter({
    mode:'history',
    routes
})