package com.lovo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lovo.dao")
public class Reply2Application {

    public static void main(String[] args) {
        SpringApplication.run(Reply2Application.class, args);
    }

}
